package cn.gson.springmvc.controller;

import cn.gson.springmvc.model.pojos.MedicalCardRecordEntity;
import cn.gson.springmvc.model.services.MedicalCardRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MedicalCardRecordController {

    @Autowired
    MedicalCardRecordService mcrs;

    @RequestMapping("sava-mcre")
    @ResponseBody
    public int SavaMcre(@RequestBody MedicalCardRecordEntity mcre){
        return mcrs.AddMcrd(mcre);
    }
}
