package cn.gson.springmvc.controller;


import cn.gson.springmvc.model.services.ZhuyuanApplyServiceLt;
import com.alibaba.fastjson.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class ZhuyuanApplyControllerLt {

    @Autowired
    ZhuyuanApplyServiceLt zhuyuanApplyService;
  /*  住院通知*/
    @RequestMapping("zy-tongzhi")
    @ResponseBody
   public Map<String,Object> zyfindAdd(){
        JSONArray jsonpObject = zhuyuanApplyService.zytongzhiFindAll();
        Map<String,Object> map=new HashMap<>();
        map.put("rows",jsonpObject);
        System.out.println("通知："+map);
        return map;
    }
}

