package cn.gson.springmvc.controller;


import cn.gson.springmvc.model.pojos.HospEntity;
import cn.gson.springmvc.model.services.HospServiceLt;
import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class HospControllerLt {

    @Autowired
    HospServiceLt hospService;

    @RequestMapping("hosp-DJ")
    @ResponseBody
   public Map<String,Object> findAdd(int currPage,int pageSize){
        Page<Object> page = PageHelper.startPage(currPage, pageSize);
        JSONArray jsonpObject = hospService.HospFindAll();
        Map<String,Object> map=new HashMap<>();
        map.put("total",page.getTotal());
        map.put("rows",jsonpObject);
        System.out.println("出现吧："+map);
        return map;
    }

    @RequestMapping("hosp-add")
    public void addHosp(@RequestBody HospEntity HospEntity){
        System.out.println("AAAA");
        hospService.addHosp(HospEntity);
    }

}

