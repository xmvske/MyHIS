package cn.gson.springmvc.controller;

import cn.gson.springmvc.model.Vo.DrugappleZx;
import cn.gson.springmvc.model.dao.DrugapplyDaoZx;
import cn.gson.springmvc.model.dao.DrugapplyTableDaoZx;
import cn.gson.springmvc.model.dao.YaoPinDaoZx;
import cn.gson.springmvc.model.services.DrugapplyServicesZx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class DrugapplyControllerZx {
    @Autowired
    YaoPinDaoZx yaoPinDaoZx;

    @Autowired
    DrugapplyDaoZx drugapplyDaoZx;

    @Autowired
    DrugapplyTableDaoZx drugapplyTableDaoZx;
    @Autowired
    DrugapplyServicesZx drugapplyServicesZx;

    @RequestMapping("/in-drugapply")
    @ResponseBody
    public void fillXz(@RequestBody DrugappleZx drugappleZx){
        System.err.println(drugappleZx);
        drugapplyServicesZx.savePurchase(drugappleZx);
    }
}
