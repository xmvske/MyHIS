package cn.gson.springmvc.controller;


import cn.gson.springmvc.model.pojos.OperationEntity;
import cn.gson.springmvc.model.services.OperationServiceLt;
import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class OperationControllerLt {

    @Autowired
    OperationServiceLt operationServiceLt;


    @RequestMapping("op-all")
    @ResponseBody
    public Map<String,Object> findAdd(int currPage,int pageSize){
        Page<Object> page = PageHelper.startPage(currPage, pageSize);
        JSONArray jsonpObject = operationServiceLt.opAll();
        Map<String,Object> map=new HashMap<>();
        map.put("total",page.getTotal());
        map.put("rows",jsonpObject);
        System.out.println("手术安排ALL：："+map);
        return map;
    }


    /**
     *
     * 新增手术安排
     * @param operationEntity
     */
    @RequestMapping("op-add")
    public void addHosp(@RequestBody OperationEntity operationEntity){
        System.out.println("bbb");
        operationServiceLt.opadd(operationEntity);
    }





}

