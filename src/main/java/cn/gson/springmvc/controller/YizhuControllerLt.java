package cn.gson.springmvc.controller;


import cn.gson.springmvc.model.pojos.YizhuEntity;
import cn.gson.springmvc.model.services.YizhuServiceLt;
import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class YizhuControllerLt {

    @Autowired
    YizhuServiceLt yizhuServiceLt;

    //住院登记打状态
    @RequestMapping("hosp-state-yizhu")
    @ResponseBody
    public Map<String,Object> leixifindAdd(){
        JSONArray jsonpObject =yizhuServiceLt.hospyizhustate();
        Map<String,Object> map=new HashMap<>();
        map.put("rows",jsonpObject);

        return map;
    }
    //查询药房为二打住院药品
    @RequestMapping("yaofan-yizhu")
    @ResponseBody
    public Map<String,Object> ReYao(){
        JSONArray jsonpObject =yizhuServiceLt.ReYao();
        Map<String,Object> map=new HashMap<>();
        map.put("rows",jsonpObject);

        return map;
    }


    @RequestMapping("yizhu-xq")
    @ResponseBody
    public void  YiZhuJiaXianQin(@RequestBody YizhuEntity yizhuEntity){
        yizhuServiceLt.YiZhuJiaXianQin(yizhuEntity);
    }



    @RequestMapping("yizhu-onall")
    @ResponseBody
    public Map<String,Object> yizhufindAll(Integer currPage,Integer pageSize){
        Page<Object> page = PageHelper.startPage(currPage, pageSize);
        JSONArray jsonpObject = yizhuServiceLt.yizhufindAll();
        Map<String,Object> map=new HashMap<>();
        map.put("total",page.getTotal());
        map.put("rows",jsonpObject);
        System.out.println("查询医嘱："+map);
        return map;
    }


    @RequestMapping("yizhuXQ-onall")
    @ResponseBody
    public Map<String,Object> yizhuXQfindAll(Long YizhuIdXQ){
        JSONArray jsonpObject = yizhuServiceLt.yizhuXQfindAll(YizhuIdXQ);
        Map<String,Object> map=new HashMap<>();
        map.put("rows",jsonpObject);
        System.out.println("查询医嘱详情："+map);
        return map;
    }



}

