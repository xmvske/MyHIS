package cn.gson.springmvc.controller;


import cn.gson.springmvc.model.pojos.ChargeEntity;
import cn.gson.springmvc.model.pojos.MedicalCardRecordEntity;
import cn.gson.springmvc.model.pojos.RegistrationEntity;
import cn.gson.springmvc.model.services.ChargeService;
import cn.gson.springmvc.model.services.MedicalCardRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
public class ChargeController {

    @Autowired
    ChargeService chas;

    @Autowired
    MedicalCardRecordService mcrs;

    @RequestMapping("save-cha")
    @ResponseBody
    public int demo(@RequestBody ChargeEntity ce){

        return chas.savaCha(ce);
    }

    @RequestMapping("state-cha")
    @ResponseBody
    public int demo2(@RequestBody ChargeEntity ce){
        if(ce.getMedicalCardRecordsByChargeId()!=null){
            List<MedicalCardRecordEntity> mcr = (ArrayList) ce.getMedicalCardRecordsByChargeId();
            mcrs.AddMcrd(mcr.get(0));
        }
        return chas.stateCha(ce);
    }

    @RequestMapping("byrid-cha")
    @ResponseBody
    public List<ChargeEntity> findByRid(@RequestBody RegistrationEntity re){

        return chas.findByRid(new Long(re.getRegistrationId()));
    }
}
