package cn.gson.springmvc.controller;

import cn.gson.springmvc.model.pojos.MedicalCardEntity;
import cn.gson.springmvc.model.pojos.MedicalCardRecordEntity;
import cn.gson.springmvc.model.services.MedicalCardRecordService;
import cn.gson.springmvc.model.services.MedicalCardService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class MedicalCardController {
    @Autowired
    MedicalCardService meas;

    @Autowired
    MedicalCardRecordService mcrs;

    @RequestMapping("init-meadicalCar")
    @ResponseBody
    public Map<String,Object> FindMeadicalCarByState(Integer currPage,Integer pageSize){
        Page<Object> objects = PageHelper.startPage(currPage, pageSize);

        Map<String,Object> map=new HashMap<>();
        map.put("rows", meas.FindMeadicalCarInit(0));
        map.put("total",objects.getTotal());
        return map;
    }

    @RequestMapping("noPage-meadicalCar")
    @ResponseBody
    public Map<String,Object> FindMeadicalCarByState2(){
        Map<String,Object> map=new HashMap<>();
        map.put("rows", meas.FindMeadicalCar(1));
        return map;
    }

    @RequestMapping("save-meadicalCar")
    @ResponseBody
    public void addMeadicalCar(@RequestBody MedicalCardEntity
                                           me) {
        if(me.getMedicalCardRecordsByMedicalId()!=null){
            List<MedicalCardRecordEntity> mcr = (ArrayList) me.getMedicalCardRecordsByMedicalId();
            mcrs.AddMcrd(mcr.get(0));
        }
        me.setMedicalState(1);
        meas.SaveMeadicalCar(me);
    }

    @RequestMapping("del-meadicalCar")
    @ResponseBody
    public int deleteMeadicalCar(@RequestBody MedicalCardEntity me) {
        me.setMedicalState(0);
        return meas.SaveMeadicalCar(me);
    }

    @RequestMapping("change-meadicalState")
    @ResponseBody
    public int demostate(@RequestBody MedicalCardEntity me) {
        me.setMedicalState(2);
        return meas.SaveMeadicalCar(me);
    }
}
