package cn.gson.springmvc.controller;

import cn.gson.springmvc.model.pojos.MedicalRecordEntity;
import cn.gson.springmvc.model.services.MedicalRecordService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class MedicalRecordController {

    @Autowired
    MedicalRecordService mrs;

    @RequestMapping("save-mrd")
    @ResponseBody
    public int demo(@RequestBody MedicalRecordEntity mre){
        return mrs.SaveMrd(mre);
    }

    @RequestMapping("init-mrd")
    @ResponseBody
    public Map<String,Object> findAllByMid(Integer currPage,Integer pageSize,Long mid){
        Page<Object> objects = PageHelper.startPage(currPage, pageSize);
        Map<String,Object> map=new HashMap<>();
        map.put("rows",mrs.findAllByMid(mid));
        map.put("total",objects.getTotal());
        return map;
    }
}
