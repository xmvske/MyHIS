package cn.gson.springmvc.controller;

import cn.gson.springmvc.model.Vo.YaoPinXzZx;
import cn.gson.springmvc.model.services.YaoPinServicesZx;
import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class YaoPinControllerZx {

    @Autowired
    YaoPinServicesZx yaoPinServicesZx;

    @RequestMapping("/in-yaopinss")
    @ResponseBody
    public JSONArray findAlln(Integer gonyingsId){//根据id查询药品
        JSONArray jsonArray=yaoPinServicesZx.finAlln(gonyingsId);
        return jsonArray;
       /* JSONArray objects= gonyingsServicesZx.finAll(gonyingsid);

        Map<String,Object> map=new HashMap<>();
        map.put("rows",objects);
        return map;*/
    }

    @RequestMapping("/in-xingzeyp")
    @ResponseBody
    public void findXz(@RequestBody YaoPinXzZx y){
        System.err.println(y);
        yaoPinServicesZx.findXz(y);
    }

    @RequestMapping("/in-yaopin")
    @ResponseBody
    public Map<String,Object> findAl(int currPage,int pageSize){
        Page<Object> page=PageHelper.startPage(currPage,pageSize);
        JSONArray objects= yaoPinServicesZx.findAll();

        Map<String,Object> map=new HashMap<>();
        map.put("total",page.getTotal());
        map.put("rows",objects);
        return map;
    }
}
