package cn.gson.springmvc.controller;

import cn.gson.springmvc.model.services.OutpatientServicesZx;
import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class OutpatientControllerZx {
    @Autowired
    OutpatientServicesZx outpatientServicesZx;

    //库存查询
    @RequestMapping("in-kucucx")
    @ResponseBody
    public Map<String,Object> findll(int currPage,int pageSize){
        Page<Object> page=PageHelper.startPage(currPage,pageSize);
        JSONArray jsonArray=outpatientServicesZx.findAll();

        Map<String,Object> map=new HashMap<>();
        map.put("total",page.getTotal());
        map.put("rows",jsonArray);
        return map;
    }
}
