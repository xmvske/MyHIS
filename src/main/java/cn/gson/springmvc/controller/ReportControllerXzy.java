package cn.gson.springmvc.controller;


import cn.gson.springmvc.model.services.HealthServiceXzy;
import cn.gson.springmvc.model.services.ReportServiceXzy;
import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class ReportControllerXzy {
    @Autowired
    ReportServiceXzy ds;

    @RequestMapping("init-demo6")
    @ResponseBody
    public Map<String,Object> findAll(int currPage, int pageSize){
        Page<Object> obj = PageHelper.startPage(currPage, pageSize);
        JSONArray objects = ds.finaAll();

        Map<String,Object> map = new HashMap<>();
        map.put("total",obj.getTotal());
        map.put("rows", objects);
        return map;
    }

}
