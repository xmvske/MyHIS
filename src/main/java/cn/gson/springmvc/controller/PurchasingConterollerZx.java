package cn.gson.springmvc.controller;

import cn.gson.springmvc.model.Vo.PurchaseInfoZx;
import cn.gson.springmvc.model.services.PurchasingServicesZx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class PurchasingConterollerZx {
    @Autowired
    PurchasingServicesZx purchasingServicesZx;

//   @RequestMapping("/save-purchase")
//    @ResponseBody
//    public void savePurchase(@RequestBody PurchaseInfo purchaseInfo){
//        System.err.println(purchaseInfo);
//    }

    @RequestMapping("/save-purchase")
    @ResponseBody
    public void savePurchase(@RequestBody PurchaseInfoZx purchaseInfoZx){
        System.err.println(purchaseInfoZx);
        purchasingServicesZx.savePurchase(purchaseInfoZx);
    }
}
