package cn.gson.springmvc.controller;

import cn.gson.springmvc.model.Vo.RukuZx;
import cn.gson.springmvc.model.services.RukutableServicesZx;
import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class RukutableControllerZx {
    @Autowired
    RukutableServicesZx rukutableServicesZx;

    @RequestMapping("/in-rukushenhe")
    @ResponseBody
    public Map<String,Object> findAll(int currPage,int pageSize){
        Page<Object> page=PageHelper.startPage(currPage,pageSize);
        JSONArray objects=rukutableServicesZx.findAll();

        Map<String,Object> map=new HashMap<>();
        map.put("total",page.getTotal());
        map.put("rows",objects);

        return map;

    }
    //根据采购编号查询采购详情
    @RequestMapping("/in-shenhe")
    @ResponseBody
    public JSONArray findById(Integer purchasingId){
        JSONArray objects=rukutableServicesZx.findAlln(purchasingId);

        return objects;
    }
    //新增
    @RequestMapping("/save-ruku")
    @ResponseBody
    public void savePurchase(@RequestBody RukuZx rukuZx){
        System.err.println(rukuZx);
        rukutableServicesZx.shenhexz(rukuZx);
    }

    //修改
    @RequestMapping("/save-xiugai")
    @ResponseBody
    public void saveXiugai( Long purId){
        System.err.println(purId);
        rukutableServicesZx.shenhexz(purId);
    }
}
