package cn.gson.springmvc.controller;

import cn.gson.springmvc.model.pojos.UserEntity;
import cn.gson.springmvc.model.services.LZLUserService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class LZLUserController {
    @Autowired
    LZLUserService us;

    @RequestMapping("user-list")
    @ResponseBody
    public Map<String,Object> updateStatus(Integer currPage,Integer pageSize){
        Page<Object> page = PageHelper.startPage(currPage, pageSize);
        Map<String,Object> map = new HashMap<>();
        map.put("total",page.getTotal());
        map.put("rows",us.findAll());
        return map;
    }

    @RequestMapping("user-passcs")
    @ResponseBody
    public void xiugai(Integer userid){us.xgmm(userid);}

    @RequestMapping("user-lizhi")
    @ResponseBody
    public void lizhi(Integer userid){
        us.yglizhi(userid);
    }

    @RequestMapping("user-denlu")
    @ResponseBody
    public Map<String,Object> denlu(String userName,String userPass){
        System.out.println(userName+"\t"+userPass);
        Map<String,Object> map = new HashMap<>();
        map.put("rows",us.denlu(userName,userPass));
        return map;
    }
}
