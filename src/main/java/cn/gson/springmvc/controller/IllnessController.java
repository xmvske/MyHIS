package cn.gson.springmvc.controller;

import cn.gson.springmvc.model.services.IllnessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class IllnessController {

    @Autowired
    IllnessService ills;

    @RequestMapping("init-ill")
    @ResponseBody
    public Map<String,Object> findDemo(){
        Map<String,Object> map=new HashMap<>();
        map.put("rows",ills.MfindAll());
        return map;
    }
}
