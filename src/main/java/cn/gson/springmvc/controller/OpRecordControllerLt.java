package cn.gson.springmvc.controller;


import cn.gson.springmvc.model.pojos.OpRecordEntity;
import cn.gson.springmvc.model.services.OpRecordServiceLt;
import com.alibaba.fastjson.JSONArray;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class OpRecordControllerLt {

    @Autowired
    OpRecordServiceLt opRecordServiceLt;

  @RequestMapping("opjl-all")
    @ResponseBody
    public Map<String,Object> opjlAll(int currPage,int pageSize){
        Page<Object> page = PageHelper.startPage(currPage, pageSize);
        JSONArray jsonpObject = opRecordServiceLt.oprecordnAll();
        Map<String,Object> map=new HashMap<>();
        map.put("total",page.getTotal());
        map.put("rows",jsonpObject);
        System.out.println("手术记录ALL：："+map);
        return map;
    }

    @RequestMapping("opjl-add")
    @ResponseBody
    public void opjladd(@RequestBody OpRecordEntity opRecordEntity){
        opRecordServiceLt.opjladd(opRecordEntity);
    }

}

