package cn.gson.springmvc.controller;


import cn.gson.springmvc.model.pojos.RegistrationEntity;
import cn.gson.springmvc.model.services.RegistrationSeivice;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

@Controller
public class RegistrationController {
    @Autowired
    RegistrationSeivice regs;

    @RequestMapping("save-reg")
    @ResponseBody
    public int Savedemo(@RequestBody RegistrationEntity re){
        return regs.saveReg(re);
    }

    @RequestMapping("init-reg")
    @ResponseBody
    public Map<String,Object> initdemo(@RequestParam("currPage") Integer currPage,
                                              @RequestParam("pageSize")Integer pageSize){
        Page<Object> objects = PageHelper.startPage(currPage, pageSize);
        Map<String,Object> map=new HashMap<>();
        map.put("rows", regs.findAll());
        map.put("total",objects.getTotal());

        return map;
    }
    @RequestMapping("jh-reg")
    @ResponseBody
    public Map<String,Object> findRegByDate(@RequestParam("currPage") Integer currPage,
                                             @RequestParam("pageSize")Integer pageSize,
                                             @RequestParam("uid") Long uid){
        Page<Object> objects = PageHelper.startPage(currPage, pageSize);
        Map<String,Object> map=new HashMap<>();
        map.put("rows", regs.findRegByDate(new Date(new java.util.Date().getTime()),uid));
        map.put("total",objects.getTotal());

        return map;
    }

    @RequestMapping("jh-reg-f")
    @ResponseBody
    public Map<String,Object> findRegByDateF(@RequestParam("currPage") Integer currPage,
                                            @RequestParam("pageSize")Integer pageSize,
                                            @RequestParam("uid") Long uid){
        Page<Object> objects = PageHelper.startPage(currPage, pageSize);
        Map<String,Object> map=new HashMap<>();
        map.put("rows", regs.findRegByDateF(new Date(new java.util.Date().getTime()),uid));
        map.put("total",objects.getTotal());

        return map;
    }

    @RequestMapping("init-reg-now")
    @ResponseBody
    public Map<String,Object> findByNow(@RequestParam("currPage") Integer currPage,
                                         @RequestParam("pageSize")Integer pageSize){
        Page<Object> objects = PageHelper.startPage(currPage, pageSize);
        Map<String,Object> map=new HashMap<>();
        map.put("rows", regs.findByNow());
        map.put("total",objects.getTotal());

        return map;
    }

    @RequestMapping("dz-reg")
    @ResponseBody
    public int daozhen(@RequestBody RegistrationEntity re){
       return regs.changeState(re,11);
    }

    @RequestMapping("lz-reg")
    @ResponseBody
    public int lizhen(@RequestBody RegistrationEntity re){
        return regs.changeState(re,1);
    }
}
