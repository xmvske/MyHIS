package cn.gson.springmvc.controller;


import cn.gson.springmvc.model.services.UserServiceLt;
import com.alibaba.fastjson.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class UserControllerLt {

    @Autowired
    UserServiceLt userService;

    @RequestMapping("user-kys")
    @ResponseBody
    public Map<String,Object> findAdd(Integer keshi,String role){
        JSONArray jsonpObject = userService.Userkysfin(keshi,role);
        Map<String,Object> map=new HashMap<>();
        map.put("rows",jsonpObject);
        System.out.println("出现吧："+map);
        return map;
    }

}
