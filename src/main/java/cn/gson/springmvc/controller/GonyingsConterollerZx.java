package cn.gson.springmvc.controller;

import cn.gson.springmvc.model.pojos.GonyingsEntity;
import cn.gson.springmvc.model.services.GonyingsServicesZx;
import com.alibaba.fastjson.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class GonyingsConterollerZx {

    @Autowired
    GonyingsServicesZx gonyingsServicesZx;


    @RequestMapping("/gonyins-s")
    @ResponseBody
    public Map<String,Object> findAlls(){
        JSONArray objects= gonyingsServicesZx.findAlll();

        Map<String,Object> map=new HashMap<>();
        map.put("rows",objects);
        return map;
    }

    //新增
    @RequestMapping("/gonyings-add")
    @ResponseBody
    public void insertSupplier(@RequestBody GonyingsEntity xz){
        gonyingsServicesZx.insertGonyings(xz);
    }


}
