package cn.gson.springmvc.controller;


import cn.gson.springmvc.model.services.KeshiServiceLt;
import com.alibaba.fastjson.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class KeshiControllerLt {

    @Autowired
    KeshiServiceLt keshiService;

    @RequestMapping("keshi-all")
    @ResponseBody
    public Map<String,Object> findAdd(){
        JSONArray jsonpObject = keshiService.Userkysfin();
        Map<String,Object> map=new HashMap<>();
        map.put("rows",jsonpObject);
        System.out.println("出现吧："+map);
        return map;
    }

    @RequestMapping("keshiid-bed")
    @ResponseBody
    public Map<String,Object> keshibed(Integer ksid){
        JSONArray jsonpObject = keshiService.keshibed(ksid);
        Map<String,Object> map=new HashMap<>();
        map.put("rows",jsonpObject);
        System.out.println("出现bed---："+map);
        return map;
    }

    @RequestMapping("keshiid-room")
    @ResponseBody
    public Map<String,Object> keshiroom(Integer ksidroom){
        JSONArray jsonpObject = keshiService.keshiroom(ksidroom);
        Map<String,Object> map=new HashMap<>();
        map.put("rows",jsonpObject);
        System.out.println("出现room---："+map);
        return map;
    }

}

