package cn.gson.springmvc.controller;

import cn.gson.springmvc.model.services.RepertoryService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class RepertoryController {

    @Autowired
    RepertoryService repes;

    @RequestMapping("init-yao-mz")
    @ResponseBody
    public Map<String,Object> findDemo(Integer currPage,Integer pageSize){
        Page<Object> objects = PageHelper.startPage(currPage, pageSize);
        Map<String,Object> map=new HashMap<>();
        map.put("rows",repes.MFindAllMenZhen());
        map.put("total",objects.getTotal());
        return map;
    }
}
