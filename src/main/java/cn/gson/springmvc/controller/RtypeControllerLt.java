package cn.gson.springmvc.controller;



import cn.gson.springmvc.model.services.RtypeServiceLt;
import com.alibaba.fastjson.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class RtypeControllerLt {

    @Autowired
    RtypeServiceLt rtypeServiceLtl;

    @RequestMapping("leixi-all")
    @ResponseBody
    public Map<String,Object> leixifindAdd(){
        JSONArray jsonpObject =rtypeServiceLtl.rtypefinall();
        Map<String,Object> map=new HashMap<>();
        map.put("rows",jsonpObject);
        System.out.println("所有类型："+map);
        return map;
    }



}

