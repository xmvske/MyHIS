package cn.gson.springmvc.controller;

import cn.gson.springmvc.model.pojos.DiagnoseEntity;
import cn.gson.springmvc.model.services.DiagnoseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class DiagnoseController {

    @Autowired
    DiagnoseService dias;


    @RequestMapping("save-dia")
    @ResponseBody
    public int addDemo(@RequestBody DiagnoseEntity Diagnose){

        return dias.SaveKanZhen(Diagnose);
    }

    @RequestMapping("bymid-dia")
    @ResponseBody
    public Map<String,Object> addDemo(/*@RequestBody MedicalCardEntity me*/ Long mid){
        Map<String,Object> map=new HashMap<>();
//        map.put("rows",dias.FindAllByMid(new Long(me.getMedicalId())));
        map.put("rows",dias.FindAllByMid(mid));
        return map;
    }
}
