package cn.gson.springmvc.controller;



import cn.gson.springmvc.model.services.OpSqServiceLt;
import cn.gson.springmvc.model.services.RoomServiceLt;
import com.alibaba.fastjson.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class OpSqControllerLt {

    @Autowired
    OpSqServiceLt opSqServiceLt;

    @RequestMapping("opsq-ltall")
    @ResponseBody
    public Map<String,Object> leixifindAdd(){
        JSONArray jsonpObject =opSqServiceLt.opsqltall();
        Map<String,Object> map=new HashMap<>();
        map.put("rows",jsonpObject);
        System.out.println("手术申请单："+map);
        return map;
    }



}

