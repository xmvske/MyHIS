package cn.gson.springmvc.controller;

import cn.gson.springmvc.model.services.LZLBumenService;
import cn.gson.springmvc.model.services.LZLRoleService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class LZLRoleController {
    @Autowired
    LZLRoleService lzlRoleService;

    @RequestMapping("juese-list")
    @ResponseBody
    public Map<String,Object> updateStatus(Integer currPage,Integer pageSize){
        Page<Object> page = PageHelper.startPage(currPage, pageSize);
        Map<String,Object> map = new HashMap<>();
        map.put("total",page.getTotal());
        map.put("rows",lzlRoleService.findAll());
        return map;
    }
}