package cn.gson.springmvc.controller;

import cn.gson.springmvc.model.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class UserController {
    @Autowired
    UserService users;

    @RequestMapping("init-userByKeshi")
    @ResponseBody
    public Map<String,Object> findUserByKeshi(@RequestParam("keshi") Long keshi){
        Map<String,Object> map=new HashMap<>();
        map.put("rows", users.findUserByKeshi(keshi,"医生"));
        return map;
    }
}
