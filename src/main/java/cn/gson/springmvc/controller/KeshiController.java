package cn.gson.springmvc.controller;

import cn.gson.springmvc.model.services.KeshiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class KeshiController {
    @Autowired
    KeshiService kess;

    @RequestMapping("init-Keshi")
    @ResponseBody
    public Map<String,Object> demofind(){
        Map<String,Object> map=new HashMap<>();
        map.put("rows", kess.findAllmapper());
        return map;
    }
}
