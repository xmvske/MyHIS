package cn.gson.springmvc.controller;

import cn.gson.springmvc.model.pojos.YaJilvEntity;
import cn.gson.springmvc.model.services.YaJilvServiceLt;
import com.alibaba.fastjson.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;


@Controller
public class YaJilvControllerLt {

    @Autowired
    YaJilvServiceLt yaJilvServiceLt;



    @RequestMapping("add-yjlv")
    @ResponseBody
    public void addyaji(@RequestBody YaJilvEntity jilvEntity){
        yaJilvServiceLt.addyaji(jilvEntity);
    }

    @RequestMapping("all-yjjl")
    @ResponseBody
    public Map<String,Object> allyjjl(Long hospid){
        JSONArray jsonpObject = yaJilvServiceLt.allyjjl(hospid);
        Map<String,Object> map=new HashMap<>();
        map.put("rows",jsonpObject);
        System.out.println("all押金记录---："+map);
        return map;
    }

}

