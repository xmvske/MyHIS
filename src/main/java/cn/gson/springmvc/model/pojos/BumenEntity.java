package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "bumen_", schema = "his", catalog = "")
public class BumenEntity {
    private int bumenId;
    private String bumenName;
    private Integer bumenNumber;
    private Integer bumenManage;
    private Collection<UserEntity> usersByBumenId;

    @Id
    @Column(name = "bumen_id", nullable = false)
    public int getBumenId() {
        return bumenId;
    }

    public void setBumenId(int bumenId) {
        this.bumenId = bumenId;
    }

    @Basic
    @Column(name = "bumen_name", nullable = true, length = 30)
    public String getBumenName() {
        return bumenName;
    }

    public void setBumenName(String bumenName) {
        this.bumenName = bumenName;
    }

    @Basic
    @Column(name = "bumen_number", nullable = true)
    public Integer getBumenNumber() {
        return bumenNumber;
    }

    public void setBumenNumber(Integer bumenNumber) {
        this.bumenNumber = bumenNumber;
    }

    @Basic
    @Column(name = "bumen_manage", nullable = true)
    public Integer getBumenManage() {
        return bumenManage;
    }

    public void setBumenManage(Integer bumenManage) {
        this.bumenManage = bumenManage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BumenEntity that = (BumenEntity) o;
        return bumenId == that.bumenId &&
                Objects.equals(bumenName, that.bumenName) &&
                Objects.equals(bumenNumber, that.bumenNumber) &&
                Objects.equals(bumenManage, that.bumenManage);
    }

    @Override
    public int hashCode() {

        return Objects.hash(bumenId, bumenName, bumenNumber, bumenManage);
    }

    @OneToMany(mappedBy = "bumenByBumenId")
    public Collection<UserEntity> getUsersByBumenId() {
        return usersByBumenId;
    }

    public void setUsersByBumenId(Collection<UserEntity> usersByBumenId) {
        this.usersByBumenId = usersByBumenId;
    }
}
