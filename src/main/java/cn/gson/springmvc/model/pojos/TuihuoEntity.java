package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "tuihuo_", schema = "his", catalog = "")
public class TuihuoEntity {
    private int tuihuoId;
    private Timestamp tuihuoData;
    private String tuihuoBezhu;
    private String tuihuoRen;
    private String tuihuoFzr;
    private Integer tuihuoZonshu;
    private Collection<GonyingsEntity> gonyingsByTuihuoId;
    private Collection<ThxqEntity> thxqsByTuihuoId;

    @Id
    @Column(name = "tuihuo_id", nullable = false)
    public int getTuihuoId() {
        return tuihuoId;
    }

    public void setTuihuoId(int tuihuoId) {
        this.tuihuoId = tuihuoId;
    }

    @Basic
    @Column(name = "tuihuo_data", nullable = true)
    public Timestamp getTuihuoData() {
        return tuihuoData;
    }

    public void setTuihuoData(Timestamp tuihuoData) {
        this.tuihuoData = tuihuoData;
    }

    @Basic
    @Column(name = "tuihuo_bezhu", nullable = true, length = 100)
    public String getTuihuoBezhu() {
        return tuihuoBezhu;
    }

    public void setTuihuoBezhu(String tuihuoBezhu) {
        this.tuihuoBezhu = tuihuoBezhu;
    }

    @Basic
    @Column(name = "tuihuo_ren", nullable = true, length = 20)
    public String getTuihuoRen() {
        return tuihuoRen;
    }

    public void setTuihuoRen(String tuihuoRen) {
        this.tuihuoRen = tuihuoRen;
    }

    @Basic
    @Column(name = "tuihuo_fzr", nullable = true, length = 20)
    public String getTuihuoFzr() {
        return tuihuoFzr;
    }

    public void setTuihuoFzr(String tuihuoFzr) {
        this.tuihuoFzr = tuihuoFzr;
    }

    @Basic
    @Column(name = "tuihuo_zonshu", nullable = true)
    public Integer getTuihuoZonshu() {
        return tuihuoZonshu;
    }

    public void setTuihuoZonshu(Integer tuihuoZonshu) {
        this.tuihuoZonshu = tuihuoZonshu;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TuihuoEntity that = (TuihuoEntity) o;
        return tuihuoId == that.tuihuoId &&
                Objects.equals(tuihuoData, that.tuihuoData) &&
                Objects.equals(tuihuoBezhu, that.tuihuoBezhu) &&
                Objects.equals(tuihuoRen, that.tuihuoRen) &&
                Objects.equals(tuihuoFzr, that.tuihuoFzr) &&
                Objects.equals(tuihuoZonshu, that.tuihuoZonshu);
    }

    @Override
    public int hashCode() {

        return Objects.hash(tuihuoId, tuihuoData, tuihuoBezhu, tuihuoRen, tuihuoFzr, tuihuoZonshu);
    }

    @OneToMany(mappedBy = "tuihuoByTuihuoId")
    public Collection<GonyingsEntity> getGonyingsByTuihuoId() {
        return gonyingsByTuihuoId;
    }

    public void setGonyingsByTuihuoId(Collection<GonyingsEntity> gonyingsByTuihuoId) {
        this.gonyingsByTuihuoId = gonyingsByTuihuoId;
    }

    @OneToMany(mappedBy = "tuihuoByTuihuoId")
    public Collection<ThxqEntity> getThxqsByTuihuoId() {
        return thxqsByTuihuoId;
    }

    public void setThxqsByTuihuoId(Collection<ThxqEntity> thxqsByTuihuoId) {
        this.thxqsByTuihuoId = thxqsByTuihuoId;
    }
}
