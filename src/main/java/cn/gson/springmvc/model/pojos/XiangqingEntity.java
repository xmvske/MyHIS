package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "xiangqing_", schema = "his", catalog = "")
public class XiangqingEntity {
    private int xiangqingId;
    private Integer xiangqingSum;
    private BigDecimal xiangqingZongjia;
    private PurchasingEntity purchasingByPurchasingId;
    private YaopinEntity yaopinByYaopinId;

    @Id
    @Column(name = "xiangqing_id", nullable = false)
    public int getXiangqingId() {
        return xiangqingId;
    }

    public void setXiangqingId(int xiangqingId) {
        this.xiangqingId = xiangqingId;
    }

    @Basic
    @Column(name = "xiangqing_sum", nullable = true)
    public Integer getXiangqingSum() {
        return xiangqingSum;
    }

    public void setXiangqingSum(Integer xiangqingSum) {
        this.xiangqingSum = xiangqingSum;
    }

    @Basic
    @Column(name = "xiangqing_zongjia", nullable = true, precision = 2)
    public BigDecimal getXiangqingZongjia() {
        return xiangqingZongjia;
    }

    public void setXiangqingZongjia(BigDecimal xiangqingZongjia) {
        this.xiangqingZongjia = xiangqingZongjia;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        XiangqingEntity that = (XiangqingEntity) o;
        return xiangqingId == that.xiangqingId &&
                Objects.equals(xiangqingSum, that.xiangqingSum) &&
                Objects.equals(xiangqingZongjia, that.xiangqingZongjia);
    }

    @Override
    public int hashCode() {

        return Objects.hash(xiangqingId, xiangqingSum, xiangqingZongjia);
    }

    @ManyToOne
    @JoinColumn(name = "purchasing_id", referencedColumnName = "purchasing_id")
    public PurchasingEntity getPurchasingByPurchasingId() {
        return purchasingByPurchasingId;
    }

    public void setPurchasingByPurchasingId(PurchasingEntity purchasingByPurchasingId) {
        this.purchasingByPurchasingId = purchasingByPurchasingId;
    }

    @ManyToOne
    @JoinColumn(name = "yaopin_id", referencedColumnName = "yaopin_id")
    public YaopinEntity getYaopinByYaopinId() {
        return yaopinByYaopinId;
    }

    public void setYaopinByYaopinId(YaopinEntity yaopinByYaopinId) {
        this.yaopinByYaopinId = yaopinByYaopinId;
    }
}
