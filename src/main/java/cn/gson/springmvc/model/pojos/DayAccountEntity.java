package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "day_account_", schema = "his", catalog = "")
public class DayAccountEntity {
    private int dayId;
    private Integer dayMonry;
    private String dayOpreator;
    private MedicalCardEntity medicalCardByMedicalId;

    @Id
    @Column(name = "day_id", nullable = false)
    public int getDayId() {
        return dayId;
    }

    public void setDayId(int dayId) {
        this.dayId = dayId;
    }

    @Basic
    @Column(name = "day_monry", nullable = true, precision = 0)
    public Integer getDayMonry() {
        return dayMonry;
    }

    public void setDayMonry(Integer dayMonry) {
        this.dayMonry = dayMonry;
    }

    @Basic
    @Column(name = "day_opreator", nullable = true, length = 30)
    public String getDayOpreator() {
        return dayOpreator;
    }

    public void setDayOpreator(String dayOpreator) {
        this.dayOpreator = dayOpreator;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DayAccountEntity that = (DayAccountEntity) o;
        return dayId == that.dayId &&
                Objects.equals(dayMonry, that.dayMonry) &&
                Objects.equals(dayOpreator, that.dayOpreator);
    }

    @Override
    public int hashCode() {

        return Objects.hash(dayId, dayMonry, dayOpreator);
    }

    @ManyToOne
    @JoinColumn(name = "medical_id", referencedColumnName = "medical_id")
    public MedicalCardEntity getMedicalCardByMedicalId() {
        return medicalCardByMedicalId;
    }

    public void setMedicalCardByMedicalId(MedicalCardEntity medicalCardByMedicalId) {
        this.medicalCardByMedicalId = medicalCardByMedicalId;
    }

    public static class DiaVo {
        private DiagnoseEntity de;
        private YizhuEntity ye;
        private List<YizhuDetailEntity> yde;

        public DiaVo() {
            super();
        }

        public DiaVo(DiagnoseEntity de, YizhuEntity ye, List<YizhuDetailEntity> yde) {
            this.de = de;
            this.ye = ye;
            this.yde = yde;
        }



        public YizhuEntity getYe() {
            return ye;
        }

        public void setYe(YizhuEntity ye) {
            this.ye = ye;
        }

        public List<YizhuDetailEntity> getYde() {
            return yde;
        }

        public void setYde(List<YizhuDetailEntity> yde) {
            this.yde = yde;
        }

        public DiagnoseEntity getDe() {
            return de;
        }

        public void setDe(DiagnoseEntity de) {
            this.de = de;
        }

    }
}
