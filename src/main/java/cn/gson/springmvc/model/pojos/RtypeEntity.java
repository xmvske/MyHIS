package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "rtype_", schema = "his", catalog = "")
public class RtypeEntity {
    private int rtypeId;
    private String rtypeName;
    private Collection<OperationEntity> operationsByRtypeId;

    @Id
    @Column(name = "rtype_id", nullable = false)
    public int getRtypeId() {
        return rtypeId;
    }

    public void setRtypeId(int rtypeId) {
        this.rtypeId = rtypeId;
    }

    @Basic
    @Column(name = "rtype_name", nullable = true, length = 30)
    public String getRtypeName() {
        return rtypeName;
    }

    public void setRtypeName(String rtypeName) {
        this.rtypeName = rtypeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RtypeEntity that = (RtypeEntity) o;
        return rtypeId == that.rtypeId &&
                Objects.equals(rtypeName, that.rtypeName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(rtypeId, rtypeName);
    }

    @OneToMany(mappedBy = "rtypeByRtypeId")
    public Collection<OperationEntity> getOperationsByRtypeId() {
        return operationsByRtypeId;
    }

    public void setOperationsByRtypeId(Collection<OperationEntity> operationsByRtypeId) {
        this.operationsByRtypeId = operationsByRtypeId;
    }
}
