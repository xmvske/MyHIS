package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "bed_change_", schema = "his", catalog = "")
public class BedChangeEntity {
    private int changeId;
    private Date changeTime;
    private MedicalCardEntity medicalCardByMedicalId;
    private BedEntity bedByBedId;

    @Id
    @Column(name = "change_id", nullable = false)
    public int getChangeId() {
        return changeId;
    }

    public void setChangeId(int changeId) {
        this.changeId = changeId;
    }

    @Basic
    @Column(name = "change_time", nullable = true)
    public Date getChangeTime() {
        return changeTime;
    }

    public void setChangeTime(Date changeTime) {
        this.changeTime = changeTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BedChangeEntity that = (BedChangeEntity) o;
        return changeId == that.changeId &&
                Objects.equals(changeTime, that.changeTime);
    }

    @Override
    public int hashCode() {

        return Objects.hash(changeId, changeTime);
    }

    @ManyToOne
    @JoinColumn(name = "medical_id", referencedColumnName = "medical_id")
    public MedicalCardEntity getMedicalCardByMedicalId() {
        return medicalCardByMedicalId;
    }

    public void setMedicalCardByMedicalId(MedicalCardEntity medicalCardByMedicalId) {
        this.medicalCardByMedicalId = medicalCardByMedicalId;
    }

    @ManyToOne
    @JoinColumn(name = "bed_id", referencedColumnName = "bed_id")
    public BedEntity getBedByBedId() {
        return bedByBedId;
    }

    public void setBedByBedId(BedEntity bedByBedId) {
        this.bedByBedId = bedByBedId;
    }
}
