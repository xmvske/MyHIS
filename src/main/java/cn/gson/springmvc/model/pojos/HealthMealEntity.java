package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "health_meal_", schema = "his", catalog = "")
public class HealthMealEntity {
    private int mealId;
    private String mealName;
    private Integer mealPrice;
    private Collection<HealthDetailEntity> healthDetailsByMealId;

    @Id
    @Column(name = "meal_id", nullable = false)
    public int getMealId() {
        return mealId;
    }

    public void setMealId(int mealId) {
        this.mealId = mealId;
    }

    @Basic
    @Column(name = "meal_name", nullable = true, length = 30)
    public String getMealName() {
        return mealName;
    }

    public void setMealName(String mealName) {
        this.mealName = mealName;
    }

    @Basic
    @Column(name = "meal_price", nullable = true, precision = 0)
    public Integer getMealPrice() {
        return mealPrice;
    }

    public void setMealPrice(Integer mealPrice) {
        this.mealPrice = mealPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HealthMealEntity that = (HealthMealEntity) o;
        return mealId == that.mealId &&
                Objects.equals(mealName, that.mealName) &&
                Objects.equals(mealPrice, that.mealPrice);
    }

    @Override
    public int hashCode() {

        return Objects.hash(mealId, mealName, mealPrice);
    }

    @OneToMany(mappedBy = "healthMealByMealId")
    public Collection<HealthDetailEntity> getHealthDetailsByMealId() {
        return healthDetailsByMealId;
    }

    public void setHealthDetailsByMealId(Collection<HealthDetailEntity> healthDetailsByMealId) {
        this.healthDetailsByMealId = healthDetailsByMealId;
    }
}
