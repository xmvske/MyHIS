package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "room_", schema = "his", catalog = "")
public class RoomEntity {
    private int roomId;
    private String roomName;
    private Integer roomState;
    private Collection<OperationEntity> operationsByRoomId;
    private KeshiEntity keshiByKeshiId;

    @Id
    @Column(name = "room_id", nullable = false)
    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    @Basic
    @Column(name = "room_name", nullable = true, length = 20)
    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    @Basic
    @Column(name = "room_state", nullable = true)
    public Integer getRoomState() {
        return roomState;
    }

    public void setRoomState(Integer roomState) {
        this.roomState = roomState;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoomEntity that = (RoomEntity) o;
        return roomId == that.roomId &&
                Objects.equals(roomName, that.roomName) &&
                Objects.equals(roomState, that.roomState);
    }

    @Override
    public int hashCode() {

        return Objects.hash(roomId, roomName, roomState);
    }

    @OneToMany(mappedBy = "roomByRoomId")
    public Collection<OperationEntity> getOperationsByRoomId() {
        return operationsByRoomId;
    }

    public void setOperationsByRoomId(Collection<OperationEntity> operationsByRoomId) {
        this.operationsByRoomId = operationsByRoomId;
    }

    @ManyToOne
    @JoinColumn(name = "keshi_id", referencedColumnName = "keshi_id")
    public KeshiEntity getKeshiByKeshiId() {
        return keshiByKeshiId;
    }

    public void setKeshiByKeshiId(KeshiEntity keshiByKeshiId) {
        this.keshiByKeshiId = keshiByKeshiId;
    }
}
