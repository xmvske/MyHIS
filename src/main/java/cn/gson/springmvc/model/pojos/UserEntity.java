package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "user_", schema = "his", catalog = "")
public class UserEntity {
    private int userId;
    private String userName;
    private String userPass;
    private Date userDate;
    private Integer userFhl;
    private Integer userState;
    private Collection<HospEntity> hospsByUserId;
    private Collection<MedicalRecordEntity> medicalRecordsByUserId;
    private Collection<OuthospitalEntity> outhospitalsByUserId;
    private Collection<RegistrationEntity> registrationsByUserId;
    private Collection<RegistrationEntity> registrationsByPhysician;
    private BumenEntity bumenByBumenId;
    private KeshiEntity keshiByKeshiId;
    private Collection<UserDetailEntity> userDetailsByUserId;
    private Collection<YizhuEntity> yizhusByUserId;

    @Id
    @Column(name = "user_id", nullable = false)
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "user_name", nullable = true, length = 30)
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Basic
    @Column(name = "user_pass", nullable = true, length = 30)
    public String getUserPass() {
        return userPass;
    }

    public void setUserPass(String userPass) {
        this.userPass = userPass;
    }

    @Basic
    @Column(name = "user_date", nullable = true)
    public Date getUserDate() {
        return userDate;
    }

    public void setUserDate(Date userDate) {
        this.userDate = userDate;
    }

    @Basic
    @Column(name = "user_fhl", nullable = true)
    public Integer getUserFhl() {
        return userFhl;
    }

    public void setUserFhl(Integer userFhl) {
        this.userFhl = userFhl;
    }

    @Basic
    @Column(name = "user_state", nullable = true)
    public Integer getUserState() {
        return userState;
    }

    public void setUserState(Integer userState) {
        this.userState = userState;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserEntity that = (UserEntity) o;
        return userId == that.userId &&
                Objects.equals(userName, that.userName) &&
                Objects.equals(userPass, that.userPass) &&
                Objects.equals(userDate, that.userDate) &&
                Objects.equals(userFhl, that.userFhl) &&
                Objects.equals(userState, that.userState);
    }

    @Override
    public int hashCode() {

        return Objects.hash(userId, userName, userPass, userDate, userFhl, userState);
    }

    @OneToMany(mappedBy = "userByUserId")
    public Collection<HospEntity> getHospsByUserId() {
        return hospsByUserId;
    }

    public void setHospsByUserId(Collection<HospEntity> hospsByUserId) {
        this.hospsByUserId = hospsByUserId;
    }

    @OneToMany(mappedBy = "userByUserId")
    public Collection<MedicalRecordEntity> getMedicalRecordsByUserId() {
        return medicalRecordsByUserId;
    }

    public void setMedicalRecordsByUserId(Collection<MedicalRecordEntity> medicalRecordsByUserId) {
        this.medicalRecordsByUserId = medicalRecordsByUserId;
    }

    @OneToMany(mappedBy = "userByUserId")
    public Collection<OuthospitalEntity> getOuthospitalsByUserId() {
        return outhospitalsByUserId;
    }

    public void setOuthospitalsByUserId(Collection<OuthospitalEntity> outhospitalsByUserId) {
        this.outhospitalsByUserId = outhospitalsByUserId;
    }

    @OneToMany(mappedBy = "userByUserId")
    public Collection<RegistrationEntity> getRegistrationsByUserId() {
        return registrationsByUserId;
    }

    public void setRegistrationsByUserId(Collection<RegistrationEntity> registrationsByUserId) {
        this.registrationsByUserId = registrationsByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "bumen_id", referencedColumnName = "bumen_id")
    public BumenEntity getBumenByBumenId() {
        return bumenByBumenId;
    }

    public void setBumenByBumenId(BumenEntity bumenByBumenId) {
        this.bumenByBumenId = bumenByBumenId;
    }

    @ManyToOne
    @JoinColumn(name = "keshi_id", referencedColumnName = "keshi_id")
    public KeshiEntity getKeshiByKeshiId() {
        return keshiByKeshiId;
    }

    public void setKeshiByKeshiId(KeshiEntity keshiByKeshiId) {
        this.keshiByKeshiId = keshiByKeshiId;
    }

    @OneToMany(mappedBy = "userByUserId")
    public Collection<UserDetailEntity> getUserDetailsByUserId() {
        return userDetailsByUserId;
    }

    public void setUserDetailsByUserId(Collection<UserDetailEntity> userDetailsByUserId) {
        this.userDetailsByUserId = userDetailsByUserId;
    }

    @OneToMany(mappedBy = "userByUserId")
    public Collection<YizhuEntity> getYizhusByUserId() {
        return yizhusByUserId;
    }

    public void setYizhusByUserId(Collection<YizhuEntity> yizhusByUserId) {
        this.yizhusByUserId = yizhusByUserId;
    }

    @OneToMany(mappedBy = "userByPhysician")
    public Collection<RegistrationEntity> getRegistrationsByPhysician() {
        return registrationsByPhysician;
    }

    public void setRegistrationsByPhysician(Collection<RegistrationEntity> registrationsByPhysician) {
        this.registrationsByPhysician = registrationsByPhysician;
    }
}
