package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "yaokupandian_", schema = "his", catalog = "")
public class YaokupandianEntity {
    private int yaokupandianId;
    private Timestamp yaokupandianTime;
    private String yaokupandianFzr;
    private BigDecimal yaokupandianJe;
    private Integer yaokupandianSl;

    @Id
    @Column(name = "yaokupandian_id", nullable = false)
    public int getYaokupandianId() {
        return yaokupandianId;
    }

    public void setYaokupandianId(int yaokupandianId) {
        this.yaokupandianId = yaokupandianId;
    }

    @Basic
    @Column(name = "yaokupandian_time", nullable = true)
    public Timestamp getYaokupandianTime() {
        return yaokupandianTime;
    }

    public void setYaokupandianTime(Timestamp yaokupandianTime) {
        this.yaokupandianTime = yaokupandianTime;
    }

    @Basic
    @Column(name = "yaokupandian_fzr", nullable = true, length = 20)
    public String getYaokupandianFzr() {
        return yaokupandianFzr;
    }

    public void setYaokupandianFzr(String yaokupandianFzr) {
        this.yaokupandianFzr = yaokupandianFzr;
    }

    @Basic
    @Column(name = "yaokupandian_je", nullable = true, precision = 2)
    public BigDecimal getYaokupandianJe() {
        return yaokupandianJe;
    }

    public void setYaokupandianJe(BigDecimal yaokupandianJe) {
        this.yaokupandianJe = yaokupandianJe;
    }

    @Basic
    @Column(name = "yaokupandian_sl", nullable = true)
    public Integer getYaokupandianSl() {
        return yaokupandianSl;
    }

    public void setYaokupandianSl(Integer yaokupandianSl) {
        this.yaokupandianSl = yaokupandianSl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        YaokupandianEntity that = (YaokupandianEntity) o;
        return yaokupandianId == that.yaokupandianId &&
                Objects.equals(yaokupandianTime, that.yaokupandianTime) &&
                Objects.equals(yaokupandianFzr, that.yaokupandianFzr) &&
                Objects.equals(yaokupandianJe, that.yaokupandianJe) &&
                Objects.equals(yaokupandianSl, that.yaokupandianSl);
    }

    @Override
    public int hashCode() {

        return Objects.hash(yaokupandianId, yaokupandianTime, yaokupandianFzr, yaokupandianJe, yaokupandianSl);
    }
}
