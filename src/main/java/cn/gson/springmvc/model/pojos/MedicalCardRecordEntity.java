package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "medical_card_record_", schema = "his", catalog = "")
public class MedicalCardRecordEntity {
    private int recordId;
    private Integer recordType;
    private Integer recordMoney;
    private Date recordDate;
    private YaopinEntity yaopinByYaopinId;
    private MedicalCardEntity medicalCardByMedicalId;
    private ChargeEntity chargeByChargeId;



    @Id
    @Column(name = "record_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getRecordId() {
        return recordId;
    }

    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    @Basic
    @Column(name = "record_type", nullable = true)
    public Integer getRecordType() {
        return recordType;
    }

    public void setRecordType(Integer recordType) {
        this.recordType = recordType;
    }

    @Basic
    @Column(name = "record_money", nullable = true, precision = 0)
    public Integer getRecordMoney() {
        return recordMoney;
    }

    public void setRecordMoney(Integer recordMoney) {
        this.recordMoney = recordMoney;
    }

    @Basic
    @Column(name = "record_date", nullable = true)
    public Date getRecordDate() {
        return recordDate;
    }

    public void setRecordDate(Date recordDate) {
        this.recordDate = recordDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicalCardRecordEntity that = (MedicalCardRecordEntity) o;
        return recordId == that.recordId &&
                Objects.equals(recordType, that.recordType) &&
                Objects.equals(recordMoney, that.recordMoney) &&
                Objects.equals(recordDate, that.recordDate);
    }

    @Override
    public int hashCode() {

        return Objects.hash(recordId, recordType, recordMoney, recordDate);
    }

    @ManyToOne
    @JoinColumn(name = "yaopin_id", referencedColumnName = "yaopin_id")
    public YaopinEntity getYaopinByYaopinId() {
        return yaopinByYaopinId;
    }

    public void setYaopinByYaopinId(YaopinEntity yaopinByYaopinId) {
        this.yaopinByYaopinId = yaopinByYaopinId;
    }

    @ManyToOne
    @JoinColumn(name = "medical_id", referencedColumnName = "medical_id")
    public MedicalCardEntity getMedicalCardByMedicalId() {
        return medicalCardByMedicalId;
    }

    public void setMedicalCardByMedicalId(MedicalCardEntity medicalCardByMedicalId) {
        this.medicalCardByMedicalId = medicalCardByMedicalId;
    }

    @ManyToOne
    @JoinColumn(name = "charge_id", referencedColumnName = "charge_id")
    public ChargeEntity getChargeByChargeId() {
        return chargeByChargeId;
    }

    public void setChargeByChargeId(ChargeEntity chargeByChargeId) {
        this.chargeByChargeId = chargeByChargeId;
    }
}
