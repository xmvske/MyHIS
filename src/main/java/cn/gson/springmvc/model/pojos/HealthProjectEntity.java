package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "health_project_", schema = "his", catalog = "")
public class HealthProjectEntity {
    private int projectId;
    private String projectName;
    private Integer projectPrice;
    private Collection<ChargeDetailEntity> chargeDetailsByProjectId;
    private Collection<HealthAppointmentEntity> healthAppointmentsByProjectId;
    private KeshiEntity keshiByKeshiId;
    private HealthAppointmentEntity healthAppointmentByAppointmentId;
    private Collection<HealthReportEntity> healthReportsByProjectId;

    @Id
    @Column(name = "project_id", nullable = false)
    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    @Basic
    @Column(name = "project_name", nullable = true, length = 30)
    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    @Basic
    @Column(name = "project_price", nullable = true, precision = 0)
    public Integer getProjectPrice() {
        return projectPrice;
    }

    public void setProjectPrice(Integer projectPrice) {
        this.projectPrice = projectPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HealthProjectEntity that = (HealthProjectEntity) o;
        return projectId == that.projectId &&
                Objects.equals(projectName, that.projectName) &&
                Objects.equals(projectPrice, that.projectPrice);
    }

    @Override
    public int hashCode() {

        return Objects.hash(projectId, projectName, projectPrice);
    }

    @OneToMany(mappedBy = "healthProjectByProjectId")
    public Collection<ChargeDetailEntity> getChargeDetailsByProjectId() {
        return chargeDetailsByProjectId;
    }

    public void setChargeDetailsByProjectId(Collection<ChargeDetailEntity> chargeDetailsByProjectId) {
        this.chargeDetailsByProjectId = chargeDetailsByProjectId;
    }

    @OneToMany(mappedBy = "healthProjectByProjectId")
    public Collection<HealthAppointmentEntity> getHealthAppointmentsByProjectId() {
        return healthAppointmentsByProjectId;
    }

    public void setHealthAppointmentsByProjectId(Collection<HealthAppointmentEntity> healthAppointmentsByProjectId) {
        this.healthAppointmentsByProjectId = healthAppointmentsByProjectId;
    }

    @ManyToOne
    @JoinColumn(name = "keshi_id", referencedColumnName = "keshi_id")
    public KeshiEntity getKeshiByKeshiId() {
        return keshiByKeshiId;
    }

    public void setKeshiByKeshiId(KeshiEntity keshiByKeshiId) {
        this.keshiByKeshiId = keshiByKeshiId;
    }

    @ManyToOne
    @JoinColumn(name = "appointment_id", referencedColumnName = "appointment_id")
    public HealthAppointmentEntity getHealthAppointmentByAppointmentId() {
        return healthAppointmentByAppointmentId;
    }

    public void setHealthAppointmentByAppointmentId(HealthAppointmentEntity healthAppointmentByAppointmentId) {
        this.healthAppointmentByAppointmentId = healthAppointmentByAppointmentId;
    }

    @OneToMany(mappedBy = "healthProjectByProjectId")
    public Collection<HealthReportEntity> getHealthReportsByProjectId() {
        return healthReportsByProjectId;
    }

    public void setHealthReportsByProjectId(Collection<HealthReportEntity> healthReportsByProjectId) {
        this.healthReportsByProjectId = healthReportsByProjectId;
    }
}
