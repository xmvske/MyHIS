package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "tuiyao_", schema = "his", catalog = "")
public class TuiyaoEntity {
    private int tuiyaoId;
    private String tuiyaoTyr;
    private Integer tuiyaoZsl;
    private Timestamp tuiyaoData;
    private Collection<TuiyaoxqEntity> tuiyaoxqsByTuiyaoId;

    @Id
    @Column(name = "tuiyao_id", nullable = false)
    public int getTuiyaoId() {
        return tuiyaoId;
    }

    public void setTuiyaoId(int tuiyaoId) {
        this.tuiyaoId = tuiyaoId;
    }

    @Basic
    @Column(name = "tuiyao_tyr", nullable = true, length = 20)
    public String getTuiyaoTyr() {
        return tuiyaoTyr;
    }

    public void setTuiyaoTyr(String tuiyaoTyr) {
        this.tuiyaoTyr = tuiyaoTyr;
    }

    @Basic
    @Column(name = "tuiyao_zsl", nullable = true)
    public Integer getTuiyaoZsl() {
        return tuiyaoZsl;
    }

    public void setTuiyaoZsl(Integer tuiyaoZsl) {
        this.tuiyaoZsl = tuiyaoZsl;
    }

    @Basic
    @Column(name = "tuiyao_data", nullable = true)
    public Timestamp getTuiyaoData() {
        return tuiyaoData;
    }

    public void setTuiyaoData(Timestamp tuiyaoData) {
        this.tuiyaoData = tuiyaoData;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TuiyaoEntity that = (TuiyaoEntity) o;
        return tuiyaoId == that.tuiyaoId &&
                Objects.equals(tuiyaoTyr, that.tuiyaoTyr) &&
                Objects.equals(tuiyaoZsl, that.tuiyaoZsl) &&
                Objects.equals(tuiyaoData, that.tuiyaoData);
    }

    @Override
    public int hashCode() {

        return Objects.hash(tuiyaoId, tuiyaoTyr, tuiyaoZsl, tuiyaoData);
    }

    @OneToMany(mappedBy = "tuiyaoByTuiyaoId")
    public Collection<TuiyaoxqEntity> getTuiyaoxqsByTuiyaoId() {
        return tuiyaoxqsByTuiyaoId;
    }

    public void setTuiyaoxqsByTuiyaoId(Collection<TuiyaoxqEntity> tuiyaoxqsByTuiyaoId) {
        this.tuiyaoxqsByTuiyaoId = tuiyaoxqsByTuiyaoId;
    }
}
