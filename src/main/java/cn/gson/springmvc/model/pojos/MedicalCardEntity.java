package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "medical_card_", schema = "his", catalog = "")
public class MedicalCardEntity {
    private int medicalId;
    private String medicalName;
    private String medicalSex;
    private String medicalTel;
    private String medicalLoc;
    private Timestamp medicalDate;
    private String medicalSfz;
    private Integer medicalState;
    private String medicalKa;
    private BigDecimal medicalMoney;
    private Collection<BedChangeEntity> bedChangesByMedicalId;
    private Collection<ChargeEntity> chargesByMedicalId;
    private Collection<DayAccountEntity> dayAccountsByMedicalId;
    private Collection<DiagnoseEntity> diagnosesByMedicalId;
    private Collection<HealthEntity> healthByMedicalId;
    private Collection<HealthAppointmentEntity> healthAppointmentsByMedicalId;
    private Collection<HospEntity> hospsByMedicalId;
    private Collection<MedicalCardRecordEntity> medicalCardRecordsByMedicalId;
    private Collection<MedicalRecordEntity> medicalRecordsByMedicalId;
    private Collection<OpSqEntity> opSqsByMedicalId;
    private Collection<OuthospitalEntity> outhospitalsByMedicalId;
    private Collection<RegistrationEntity> registrationsByMedicalId;
    private Collection<YizhuEntity> yizhusByMedicalId;
    private Collection<ZhuyuanApplyEntity> zhuyuanAppliesByMedicalId;

    @Id
    @Column(name = "medical_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getMedicalId() {
        return medicalId;
    }

    public void setMedicalId(int medicalId) {
        this.medicalId = medicalId;
    }

    @Basic
    @Column(name = "medical_name", nullable = true, length = 30)
    public String getMedicalName() {
        return medicalName;
    }

    public void setMedicalName(String medicalName) {
        this.medicalName = medicalName;
    }

    @Basic
    @Column(name = "medical_sex", nullable = true, length = 3)
    public String getMedicalSex() {
        return medicalSex;
    }

    public void setMedicalSex(String medicalSex) {
        this.medicalSex = medicalSex;
    }

    @Basic
    @Column(name = "medical_tel", nullable = true, length = 20)
    public String getMedicalTel() {
        return medicalTel;
    }

    public void setMedicalTel(String medicalTel) {
        this.medicalTel = medicalTel;
    }

    @Basic
    @Column(name = "medical_loc", nullable = true, length = 250)
    public String getMedicalLoc() {
        return medicalLoc;
    }

    public void setMedicalLoc(String medicalLoc) {
        this.medicalLoc = medicalLoc;
    }

    @Basic
    @Column(name = "medical_date", nullable = true)
    public Timestamp getMedicalDate() {
        return medicalDate;
    }

    public void setMedicalDate(Timestamp medicalDate) {
        this.medicalDate = medicalDate;
    }

    @Basic
    @Column(name = "medical_sfz", nullable = true, length = 20)
    public String getMedicalSfz() {
        return medicalSfz;
    }

    public void setMedicalSfz(String medicalSfz) {
        this.medicalSfz = medicalSfz;
    }

    @Basic
    @Column(name = "medical_state", nullable = true)
    public Integer getMedicalState() {
        return medicalState;
    }

    public void setMedicalState(Integer medicalState) {
        this.medicalState = medicalState;
    }

    @Basic
    @Column(name = "medical_ka", nullable = false, length = 30)
    public String getMedicalKa() {
        return medicalKa;
    }

    public void setMedicalKa(String medicalKa) {
        this.medicalKa = medicalKa;
    }


    @Basic
    @Column(name = "medical_money", nullable = true)
    public BigDecimal getMedicalMoney() {
        return medicalMoney;
    }

    public void setMedicalMoney(BigDecimal medicalMoney) {
        this.medicalMoney = medicalMoney;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicalCardEntity that = (MedicalCardEntity) o;
        return medicalId == that.medicalId &&
                Objects.equals(medicalName, that.medicalName) &&
                Objects.equals(medicalSex, that.medicalSex) &&
                Objects.equals(medicalTel, that.medicalTel) &&
                Objects.equals(medicalLoc, that.medicalLoc) &&
                Objects.equals(medicalDate, that.medicalDate) &&
                Objects.equals(medicalSfz, that.medicalSfz) &&
                Objects.equals(medicalState, that.medicalState) &&
                Objects.equals(medicalKa, that.medicalKa);
    }

    @Override
    public int hashCode() {

        return Objects.hash(medicalId, medicalName, medicalSex, medicalTel, medicalLoc, medicalDate, medicalSfz, medicalState, medicalKa);
    }

    @OneToMany(mappedBy = "medicalCardByMedicalId")
    public Collection<BedChangeEntity> getBedChangesByMedicalId() {
        return bedChangesByMedicalId;
    }

    public void setBedChangesByMedicalId(Collection<BedChangeEntity> bedChangesByMedicalId) {
        this.bedChangesByMedicalId = bedChangesByMedicalId;
    }

    @OneToMany(mappedBy = "medicalCardByMedicalId")
    public Collection<ChargeEntity> getChargesByMedicalId() {
        return chargesByMedicalId;
    }

    public void setChargesByMedicalId(Collection<ChargeEntity> chargesByMedicalId) {
        this.chargesByMedicalId = chargesByMedicalId;
    }

    @OneToMany(mappedBy = "medicalCardByMedicalId")
    public Collection<DayAccountEntity> getDayAccountsByMedicalId() {
        return dayAccountsByMedicalId;
    }

    public void setDayAccountsByMedicalId(Collection<DayAccountEntity> dayAccountsByMedicalId) {
        this.dayAccountsByMedicalId = dayAccountsByMedicalId;
    }

    @OneToMany(mappedBy = "medicalCardByMedicalId")
    public Collection<DiagnoseEntity> getDiagnosesByMedicalId() {
        return diagnosesByMedicalId;
    }

    public void setDiagnosesByMedicalId(Collection<DiagnoseEntity> diagnosesByMedicalId) {
        this.diagnosesByMedicalId = diagnosesByMedicalId;
    }

    @OneToMany(mappedBy = "medicalCardByMedicalId")
    public Collection<HealthEntity> getHealthByMedicalId() {
        return healthByMedicalId;
    }

    public void setHealthByMedicalId(Collection<HealthEntity> healthByMedicalId) {
        this.healthByMedicalId = healthByMedicalId;
    }

    @OneToMany(mappedBy = "medicalCardByMedicalId")
    public Collection<HealthAppointmentEntity> getHealthAppointmentsByMedicalId() {
        return healthAppointmentsByMedicalId;
    }

    public void setHealthAppointmentsByMedicalId(Collection<HealthAppointmentEntity> healthAppointmentsByMedicalId) {
        this.healthAppointmentsByMedicalId = healthAppointmentsByMedicalId;
    }

    @OneToMany(mappedBy = "medicalCardByMedicalId")
    public Collection<HospEntity> getHospsByMedicalId() {
        return hospsByMedicalId;
    }

    public void setHospsByMedicalId(Collection<HospEntity> hospsByMedicalId) {
        this.hospsByMedicalId = hospsByMedicalId;
    }

    @OneToMany(mappedBy = "medicalCardByMedicalId")
    public Collection<MedicalCardRecordEntity> getMedicalCardRecordsByMedicalId() {
        return medicalCardRecordsByMedicalId;
    }

    public void setMedicalCardRecordsByMedicalId(Collection<MedicalCardRecordEntity> medicalCardRecordsByMedicalId) {
        this.medicalCardRecordsByMedicalId = medicalCardRecordsByMedicalId;
    }

    @OneToMany(mappedBy = "medicalCardByMedicalId")
    public Collection<MedicalRecordEntity> getMedicalRecordsByMedicalId() {
        return medicalRecordsByMedicalId;
    }

    public void setMedicalRecordsByMedicalId(Collection<MedicalRecordEntity> medicalRecordsByMedicalId) {
        this.medicalRecordsByMedicalId = medicalRecordsByMedicalId;
    }

    @OneToMany(mappedBy = "medicalCardByMedicalId")
    public Collection<OpSqEntity> getOpSqsByMedicalId() {
        return opSqsByMedicalId;
    }

    public void setOpSqsByMedicalId(Collection<OpSqEntity> opSqsByMedicalId) {
        this.opSqsByMedicalId = opSqsByMedicalId;
    }

    @OneToMany(mappedBy = "medicalCardByMedicalId")
    public Collection<OuthospitalEntity> getOuthospitalsByMedicalId() {
        return outhospitalsByMedicalId;
    }

    public void setOuthospitalsByMedicalId(Collection<OuthospitalEntity> outhospitalsByMedicalId) {
        this.outhospitalsByMedicalId = outhospitalsByMedicalId;
    }

    @OneToMany(mappedBy = "medicalCardByMedicalId")
    public Collection<RegistrationEntity> getRegistrationsByMedicalId() {
        return registrationsByMedicalId;
    }

    public void setRegistrationsByMedicalId(Collection<RegistrationEntity> registrationsByMedicalId) {
        this.registrationsByMedicalId = registrationsByMedicalId;
    }

    @OneToMany(mappedBy = "medicalCardByMedicalId")
    public Collection<YizhuEntity> getYizhusByMedicalId() {
        return yizhusByMedicalId;
    }

    public void setYizhusByMedicalId(Collection<YizhuEntity> yizhusByMedicalId) {
        this.yizhusByMedicalId = yizhusByMedicalId;
    }

    @OneToMany(mappedBy = "medicalCardByMedicalId")
    public Collection<ZhuyuanApplyEntity> getZhuyuanAppliesByMedicalId() {
        return zhuyuanAppliesByMedicalId;
    }

    public void setZhuyuanAppliesByMedicalId(Collection<ZhuyuanApplyEntity> zhuyuanAppliesByMedicalId) {
        this.zhuyuanAppliesByMedicalId = zhuyuanAppliesByMedicalId;
    }
}
