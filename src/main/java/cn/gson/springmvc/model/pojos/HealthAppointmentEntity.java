package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "health_appointment_", schema = "his", catalog = "")
public class HealthAppointmentEntity {
    private int appointmentId;
    private Date appointmentTime;
    private String appointmentPeople;
    private Integer appointmentState;
    private HealthProjectEntity healthProjectByProjectId;
    private MedicalCardEntity medicalCardByMedicalId;
    private Collection<HealthProjectEntity> healthProjectsByAppointmentId;

    @Id
    @Column(name = "appointment_id", nullable = false)
    public int getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(int appointmentId) {
        this.appointmentId = appointmentId;
    }

    @Basic
    @Column(name = "appointment_time", nullable = true)
    public Date getAppointmentTime() {
        return appointmentTime;
    }

    public void setAppointmentTime(Date appointmentTime) {
        this.appointmentTime = appointmentTime;
    }

    @Basic
    @Column(name = "appointment_people", nullable = true, length = 30)
    public String getAppointmentPeople() {
        return appointmentPeople;
    }

    public void setAppointmentPeople(String appointmentPeople) {
        this.appointmentPeople = appointmentPeople;
    }

    @Basic
    @Column(name = "appointment_state", nullable = true)
    public Integer getAppointmentState() {
        return appointmentState;
    }

    public void setAppointmentState(Integer appointmentState) {
        this.appointmentState = appointmentState;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HealthAppointmentEntity that = (HealthAppointmentEntity) o;
        return appointmentId == that.appointmentId &&
                Objects.equals(appointmentTime, that.appointmentTime) &&
                Objects.equals(appointmentPeople, that.appointmentPeople) &&
                Objects.equals(appointmentState, that.appointmentState);
    }

    @Override
    public int hashCode() {

        return Objects.hash(appointmentId, appointmentTime, appointmentPeople, appointmentState);
    }

    @ManyToOne
    @JoinColumn(name = "project_id", referencedColumnName = "project_id")
    public HealthProjectEntity getHealthProjectByProjectId() {
        return healthProjectByProjectId;
    }

    public void setHealthProjectByProjectId(HealthProjectEntity healthProjectByProjectId) {
        this.healthProjectByProjectId = healthProjectByProjectId;
    }

    @ManyToOne
    @JoinColumn(name = "medical_id", referencedColumnName = "medical_id")
    public MedicalCardEntity getMedicalCardByMedicalId() {
        return medicalCardByMedicalId;
    }

    public void setMedicalCardByMedicalId(MedicalCardEntity medicalCardByMedicalId) {
        this.medicalCardByMedicalId = medicalCardByMedicalId;
    }

    @OneToMany(mappedBy = "healthAppointmentByAppointmentId")
    public Collection<HealthProjectEntity> getHealthProjectsByAppointmentId() {
        return healthProjectsByAppointmentId;
    }

    public void setHealthProjectsByAppointmentId(Collection<HealthProjectEntity> healthProjectsByAppointmentId) {
        this.healthProjectsByAppointmentId = healthProjectsByAppointmentId;
    }
}
