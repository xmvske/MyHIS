package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "hosp_", schema = "his", catalog = "")
public class HospEntity {
    private Long hospId;
    private Integer hospHao;
    private Date hospDate;
    private String hospDoctorname;
    private String hospNursename;
    private Integer hospCashMoney;
    private BedEntity bedByBedId;
    private Integer hospState;
    private KeshiEntity keshiByKeshiId;
    private ZhuyuanApplyEntity zhuyuanApplyByZhuyuanApplyId;
    private MedicalCardEntity medicalCardByMedicalId;
    private UserEntity userByUserId;
    private Collection<YaJilvEntity> yaJilvsByHospId;

    @Id
    @Column(name = "hosp_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getHospId() {
        return hospId;
    }

    public void setHospId(Long hospId) {
        this.hospId = hospId;
    }

    @Basic
    @Column(name = "hosp_state", nullable = true)
    public Integer getHospState() {
        return hospState;
    }

    public void setHospState(Integer hospState) {
        this.hospState = hospState;
    }

    @Basic
    @Column(name = "hosp_hao", nullable = true)
    public Integer getHospHao() {
        return hospHao;
    }

    public void setHospHao(Integer hospHao) {
        this.hospHao = hospHao;
    }

    @Basic
    @Column(name = "hosp_date", nullable = true)
    public Date getHospDate() {
        return hospDate;
    }

    public void setHospDate(Date hospDate) {
        this.hospDate = hospDate;
    }

    @Basic
    @Column(name = "hosp_doctorname", nullable = true, length = 10)
    public String getHospDoctorname() {
        return hospDoctorname;
    }

    public void setHospDoctorname(String hospDoctorname) {
        this.hospDoctorname = hospDoctorname;
    }

    @Basic
    @Column(name = "hosp_nursename", nullable = true, length = 20)
    public String getHospNursename() {
        return hospNursename;
    }

    public void setHospNursename(String hospNursename) {
        this.hospNursename = hospNursename;
    }

    @Basic
    @Column(name = "hosp_cash_money", nullable = true, precision = 0)
    public Integer getHospCashMoney() {
        return hospCashMoney;
    }

    public void setHospCashMoney(Integer hospCashMoney) {
        this.hospCashMoney = hospCashMoney;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HospEntity that = (HospEntity) o;
        return hospId == that.hospId &&
                Objects.equals(hospHao, that.hospHao) &&
                Objects.equals(hospDate, that.hospDate) &&
                Objects.equals(hospDoctorname, that.hospDoctorname) &&
                Objects.equals(hospNursename, that.hospNursename) &&
                Objects.equals(hospCashMoney, that.hospCashMoney);
    }

    @Override
    public int hashCode() {

        return Objects.hash(hospId, hospHao, hospDate, hospDoctorname, hospNursename, hospCashMoney);
    }

    @ManyToOne
    @JoinColumn(name = "bed_id", referencedColumnName = "bed_id")
    public BedEntity getBedByBedId() {
        return bedByBedId;
    }

    public void setBedByBedId(BedEntity bedByBedId) {
        this.bedByBedId = bedByBedId;
    }

    @ManyToOne
    @JoinColumn(name = "keshi_id", referencedColumnName = "keshi_id")
    public KeshiEntity getKeshiByKeshiId() {
        return keshiByKeshiId;
    }

    public void setKeshiByKeshiId(KeshiEntity keshiByKeshiId) {
        this.keshiByKeshiId = keshiByKeshiId;
    }

    @ManyToOne
    @JoinColumn(name = "zhuyuan_apply_id", referencedColumnName = "zhuyuan_apply_id")
    public ZhuyuanApplyEntity getZhuyuanApplyByZhuyuanApplyId() {
        return zhuyuanApplyByZhuyuanApplyId;
    }

    public void setZhuyuanApplyByZhuyuanApplyId(ZhuyuanApplyEntity zhuyuanApplyByZhuyuanApplyId) {
        this.zhuyuanApplyByZhuyuanApplyId = zhuyuanApplyByZhuyuanApplyId;
    }

    @ManyToOne
    @JoinColumn(name = "medical_id", referencedColumnName = "medical_id")
    public MedicalCardEntity getMedicalCardByMedicalId() {
        return medicalCardByMedicalId;
    }

    public void setMedicalCardByMedicalId(MedicalCardEntity medicalCardByMedicalId) {
        this.medicalCardByMedicalId = medicalCardByMedicalId;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getUserByUserId() {
        return userByUserId;
    }

    public void setUserByUserId(UserEntity userByUserId) {
        this.userByUserId = userByUserId;
    }

    @OneToMany(mappedBy = "hospByHospId")
    public Collection<YaJilvEntity> getYaJilvsByHospId() {
        return yaJilvsByHospId;
    }

    public void setYaJilvsByHospId(Collection<YaJilvEntity> yaJilvsByHospId) {
        this.yaJilvsByHospId = yaJilvsByHospId;
    }
}
