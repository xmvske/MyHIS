package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "tuiyaoxq_", schema = "his", catalog = "")
public class TuiyaoxqEntity {
    private int tuiyaoxqId;
    private Integer tuiyaoxqDgsl;
    private BigDecimal tuiyaoxqJg;
    private String tuiyaoxqBz;
    private Collection<RepertoryEntity> repertoriesByTuiyaoxqId;
    private TuiyaoEntity tuiyaoByTuiyaoId;

    @Id
    @Column(name = "tuiyaoxq_id", nullable = false)
    public int getTuiyaoxqId() {
        return tuiyaoxqId;
    }

    public void setTuiyaoxqId(int tuiyaoxqId) {
        this.tuiyaoxqId = tuiyaoxqId;
    }

    @Basic
    @Column(name = "tuiyaoxq_dgsl", nullable = true)
    public Integer getTuiyaoxqDgsl() {
        return tuiyaoxqDgsl;
    }

    public void setTuiyaoxqDgsl(Integer tuiyaoxqDgsl) {
        this.tuiyaoxqDgsl = tuiyaoxqDgsl;
    }

    @Basic
    @Column(name = "tuiyaoxq_jg", nullable = true, precision = 2)
    public BigDecimal getTuiyaoxqJg() {
        return tuiyaoxqJg;
    }

    public void setTuiyaoxqJg(BigDecimal tuiyaoxqJg) {
        this.tuiyaoxqJg = tuiyaoxqJg;
    }

    @Basic
    @Column(name = "tuiyaoxq_bz", nullable = true, length = 100)
    public String getTuiyaoxqBz() {
        return tuiyaoxqBz;
    }

    public void setTuiyaoxqBz(String tuiyaoxqBz) {
        this.tuiyaoxqBz = tuiyaoxqBz;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TuiyaoxqEntity that = (TuiyaoxqEntity) o;
        return tuiyaoxqId == that.tuiyaoxqId &&
                Objects.equals(tuiyaoxqDgsl, that.tuiyaoxqDgsl) &&
                Objects.equals(tuiyaoxqJg, that.tuiyaoxqJg) &&
                Objects.equals(tuiyaoxqBz, that.tuiyaoxqBz);
    }

    @Override
    public int hashCode() {

        return Objects.hash(tuiyaoxqId, tuiyaoxqDgsl, tuiyaoxqJg, tuiyaoxqBz);
    }

    @OneToMany(mappedBy = "tuiyaoxqByTuiyaoxqId")
    public Collection<RepertoryEntity> getRepertoriesByTuiyaoxqId() {
        return repertoriesByTuiyaoxqId;
    }

    public void setRepertoriesByTuiyaoxqId(Collection<RepertoryEntity> repertoriesByTuiyaoxqId) {
        this.repertoriesByTuiyaoxqId = repertoriesByTuiyaoxqId;
    }

    @ManyToOne
    @JoinColumn(name = "tuiyao_id", referencedColumnName = "tuiyao_id")
    public TuiyaoEntity getTuiyaoByTuiyaoId() {
        return tuiyaoByTuiyaoId;
    }

    public void setTuiyaoByTuiyaoId(TuiyaoEntity tuiyaoByTuiyaoId) {
        this.tuiyaoByTuiyaoId = tuiyaoByTuiyaoId;
    }
}
