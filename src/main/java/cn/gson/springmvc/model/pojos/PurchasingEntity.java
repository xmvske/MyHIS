package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "purchasing_", schema = "his", catalog = "")
public class PurchasingEntity {
    private Long purchasingId;
    private String purchasingName;
    private Timestamp purchasingData;
    private String purchasingShenpir;
    private BigDecimal zongjine;
    private Integer purchaseingZuantai;
    private GonyingsEntity gonyingsByPurchasingId;
    private Collection<XiangqingEntity> xiangqingsByPurchasingId;

    @Id
    @Column(name = "purchasing_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getPurchasingId() {
        return purchasingId;
    }

    public void setPurchasingId(Long purchasingId) {
        this.purchasingId = purchasingId;
    }

    @Basic
    @Column(name = "purchasing_name", nullable = true, length = 20)
    public String getPurchasingName() {
        return purchasingName;
    }

    public void setPurchasingName(String purchasingName) {
        this.purchasingName = purchasingName;
    }

    @Basic
    @Column(name = "purchasing_data", nullable = true)
    public Timestamp getPurchasingData() {
        return purchasingData;
    }

    public void setPurchasingData(Timestamp purchasingData) {
        this.purchasingData = purchasingData;
    }

    @Basic
    @Column(name = "purchasing_shenpir", nullable = true, length = 20)
    public String getPurchasingShenpir() {
        return purchasingShenpir;
    }

    public void setPurchasingShenpir(String purchasingShenpir) {
        this.purchasingShenpir = purchasingShenpir;
    }

    @Basic
    @Column(name = "zongjine", nullable = true, precision = 2)
    public BigDecimal getZongjine() {
        return zongjine;
    }

    public void setZongjine(BigDecimal zongjine) {
        this.zongjine = zongjine;
    }

    @Basic
    @Column(name = "purchaseing_zuantai", nullable = true)
    public Integer getPurchaseingZuantai() {
        return purchaseingZuantai;
    }

    public void setPurchaseingZuantai(Integer purchaseingZuantai) {
        this.purchaseingZuantai = purchaseingZuantai;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PurchasingEntity that = (PurchasingEntity) o;
        return purchasingId == that.purchasingId &&
                Objects.equals(purchasingName, that.purchasingName) &&
                Objects.equals(purchasingData, that.purchasingData) &&
                Objects.equals(purchasingShenpir, that.purchasingShenpir) &&
                Objects.equals(zongjine, that.zongjine) &&
                Objects.equals(purchaseingZuantai, that.purchaseingZuantai);
    }

    @Override
    public int hashCode() {

        return Objects.hash(purchasingId, purchasingName, purchasingData, purchasingShenpir, zongjine, purchaseingZuantai);
    }

    @ManyToOne
    @JoinColumn(name="gonyings_id" , referencedColumnName = "gonyings_id")
    public GonyingsEntity getGonyingsByPurchasingId() {
        return gonyingsByPurchasingId;
    }

    public void setGonyingsByPurchasingId(GonyingsEntity gonyingsByPurchasingId) {
        this.gonyingsByPurchasingId = gonyingsByPurchasingId;
    }

    @OneToMany(mappedBy = "purchasingByPurchasingId")
    public Collection<XiangqingEntity> getXiangqingsByPurchasingId() {
        return xiangqingsByPurchasingId;
    }

    public void setXiangqingsByPurchasingId(Collection<XiangqingEntity> xiangqingsByPurchasingId) {
        this.xiangqingsByPurchasingId = xiangqingsByPurchasingId;
    }
}
