package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "hospital_project_", schema = "his", catalog = "")
public class HospitalProjectEntity {
    private int hospitalProjectId;
    private Integer hospitalProjectPrice;
    private String hospitalProjectName;
    private Collection<ChargeDetailEntity> chargeDetailsByHospitalProjectId;

    @Id
    @Column(name = "hospital_project_id", nullable = false)
    public int getHospitalProjectId() {
        return hospitalProjectId;
    }

    public void setHospitalProjectId(int hospitalProjectId) {
        this.hospitalProjectId = hospitalProjectId;
    }

    @Basic
    @Column(name = "hospital_project_price", nullable = true, precision = 0)
    public Integer getHospitalProjectPrice() {
        return hospitalProjectPrice;
    }

    public void setHospitalProjectPrice(Integer hospitalProjectPrice) {
        this.hospitalProjectPrice = hospitalProjectPrice;
    }

    @Basic
    @Column(name = "hospital_project_name", nullable = true, length = 50)
    public String getHospitalProjectName() {
        return hospitalProjectName;
    }

    public void setHospitalProjectName(String hospitalProjectName) {
        this.hospitalProjectName = hospitalProjectName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HospitalProjectEntity that = (HospitalProjectEntity) o;
        return hospitalProjectId == that.hospitalProjectId &&
                Objects.equals(hospitalProjectPrice, that.hospitalProjectPrice) &&
                Objects.equals(hospitalProjectName, that.hospitalProjectName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(hospitalProjectId, hospitalProjectPrice, hospitalProjectName);
    }

    @OneToMany(mappedBy = "hospitalProjectByHospitalProjectId")
    public Collection<ChargeDetailEntity> getChargeDetailsByHospitalProjectId() {
        return chargeDetailsByHospitalProjectId;
    }

    public void setChargeDetailsByHospitalProjectId(Collection<ChargeDetailEntity> chargeDetailsByHospitalProjectId) {
        this.chargeDetailsByHospitalProjectId = chargeDetailsByHospitalProjectId;
    }
}
