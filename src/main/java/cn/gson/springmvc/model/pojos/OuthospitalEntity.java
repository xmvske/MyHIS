package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "outhospital_", schema = "his", catalog = "")
public class OuthospitalEntity {
    private int outId;
    private String outOpreator;
    private Integer outTotalMoney;
    private String outAdvice;
    private Date outTime;
    private Integer outState;
    private KeshiEntity keshiByKeshiId;
    private BedEntity bedByBedId;
    private MedicalCardEntity medicalCardByMedicalId;
    private UserEntity userByUserId;

    @Id
    @Column(name = "out_id", nullable = false)
    public int getOutId() {
        return outId;
    }

    public void setOutId(int outId) {
        this.outId = outId;
    }

    @Basic
    @Column(name = "out_opreator", nullable = true, length = 30)
    public String getOutOpreator() {
        return outOpreator;
    }

    public void setOutOpreator(String outOpreator) {
        this.outOpreator = outOpreator;
    }

    @Basic
    @Column(name = "out_total_money", nullable = true, precision = 0)
    public Integer getOutTotalMoney() {
        return outTotalMoney;
    }

    public void setOutTotalMoney(Integer outTotalMoney) {
        this.outTotalMoney = outTotalMoney;
    }

    @Basic
    @Column(name = "out_advice", nullable = true, length = 100)
    public String getOutAdvice() {
        return outAdvice;
    }

    public void setOutAdvice(String outAdvice) {
        this.outAdvice = outAdvice;
    }

    @Basic
    @Column(name = "out_time", nullable = true)
    public Date getOutTime() {
        return outTime;
    }

    public void setOutTime(Date outTime) {
        this.outTime = outTime;
    }

    @Basic
    @Column(name = "out_state", nullable = true)
    public Integer getOutState() {
        return outState;
    }

    public void setOutState(Integer outState) {
        this.outState = outState;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OuthospitalEntity that = (OuthospitalEntity) o;
        return outId == that.outId &&
                Objects.equals(outOpreator, that.outOpreator) &&
                Objects.equals(outTotalMoney, that.outTotalMoney) &&
                Objects.equals(outAdvice, that.outAdvice) &&
                Objects.equals(outTime, that.outTime) &&
                Objects.equals(outState, that.outState);
    }

    @Override
    public int hashCode() {

        return Objects.hash(outId, outOpreator, outTotalMoney, outAdvice, outTime, outState);
    }

    @ManyToOne
    @JoinColumn(name = "keshi_id", referencedColumnName = "keshi_id")
    public KeshiEntity getKeshiByKeshiId() {
        return keshiByKeshiId;
    }

    public void setKeshiByKeshiId(KeshiEntity keshiByKeshiId) {
        this.keshiByKeshiId = keshiByKeshiId;
    }

    @ManyToOne
    @JoinColumn(name = "bed_id", referencedColumnName = "bed_id")
    public BedEntity getBedByBedId() {
        return bedByBedId;
    }

    public void setBedByBedId(BedEntity bedByBedId) {
        this.bedByBedId = bedByBedId;
    }

    @ManyToOne
    @JoinColumn(name = "medical_id", referencedColumnName = "medical_id")
    public MedicalCardEntity getMedicalCardByMedicalId() {
        return medicalCardByMedicalId;
    }

    public void setMedicalCardByMedicalId(MedicalCardEntity medicalCardByMedicalId) {
        this.medicalCardByMedicalId = medicalCardByMedicalId;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getUserByUserId() {
        return userByUserId;
    }

    public void setUserByUserId(UserEntity userByUserId) {
        this.userByUserId = userByUserId;
    }
}
