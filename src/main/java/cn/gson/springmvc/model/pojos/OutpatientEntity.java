package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "outpatient_", schema = "his", catalog = "")
public class OutpatientEntity {
    private int outpatientId;
    private Integer outpatientSl;
    private Collection<ChukuxqEntity> chukuxqsByOutpatientId;
    private Collection<InventoryDetailsEntity> inventoryDetailsByOutpatientId;
    private YaopinEntity yaopinByYaopinId;

    @Id
    @Column(name = "outpatient_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getOutpatientId() {
        return outpatientId;
    }

    public void setOutpatientId(int outpatientId) {
        this.outpatientId = outpatientId;
    }

    @Basic
    @Column(name = "outpatient_sl", nullable = true)
    public Integer getOutpatientSl() {
        return outpatientSl;
    }

    public void setOutpatientSl(Integer outpatientSl) {
        this.outpatientSl = outpatientSl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OutpatientEntity that = (OutpatientEntity) o;
        return outpatientId == that.outpatientId &&
                Objects.equals(outpatientSl, that.outpatientSl);
    }

    @Override
    public int hashCode() {

        return Objects.hash(outpatientId, outpatientSl);
    }

    @OneToMany(mappedBy = "outpatientByOutpatientId")
    public Collection<ChukuxqEntity> getChukuxqsByOutpatientId() {
        return chukuxqsByOutpatientId;
    }

    public void setChukuxqsByOutpatientId(Collection<ChukuxqEntity> chukuxqsByOutpatientId) {
        this.chukuxqsByOutpatientId = chukuxqsByOutpatientId;
    }

    @OneToMany(mappedBy = "outpatientByOutpatientId")
    public Collection<InventoryDetailsEntity> getInventoryDetailsByOutpatientId() {
        return inventoryDetailsByOutpatientId;
    }

    public void setInventoryDetailsByOutpatientId(Collection<InventoryDetailsEntity> inventoryDetailsByOutpatientId) {
        this.inventoryDetailsByOutpatientId = inventoryDetailsByOutpatientId;
    }

    @ManyToOne
    @JoinColumn(name = "yaopin_id", referencedColumnName = "yaopin_id")
    public YaopinEntity getYaopinByYaopinId() {
        return yaopinByYaopinId;
    }

    public void setYaopinByYaopinId(YaopinEntity yaopinByYaopinId) {
        this.yaopinByYaopinId = yaopinByYaopinId;
    }
}
