package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "illness_", schema = "his", catalog = "")
public class IllnessEntity {
    private int illnessId;
    private String illnessName;
    private String illnessType;
    private Integer illnessIscr;
    private Collection<MedicalRecordEntity> medicalRecordsByIllnessId;

    @Id
    @Column(name = "illness_id", nullable = false)
    public int getIllnessId() {
        return illnessId;
    }

    public void setIllnessId(int illnessId) {
        this.illnessId = illnessId;
    }

    @Basic
    @Column(name = "illness_name", nullable = true, length = 50)
    public String getIllnessName() {
        return illnessName;
    }

    public void setIllnessName(String illnessName) {
        this.illnessName = illnessName;
    }

    @Basic
    @Column(name = "illness_type", nullable = true, length = 30)
    public String getIllnessType() {
        return illnessType;
    }

    public void setIllnessType(String illnessType) {
        this.illnessType = illnessType;
    }

    @Basic
    @Column(name = "illness_iscr", nullable = true)
    public Integer getIllnessIscr() {
        return illnessIscr;
    }

    public void setIllnessIscr(Integer illnessIscr) {
        this.illnessIscr = illnessIscr;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IllnessEntity that = (IllnessEntity) o;
        return illnessId == that.illnessId &&
                Objects.equals(illnessName, that.illnessName) &&
                Objects.equals(illnessType, that.illnessType) &&
                Objects.equals(illnessIscr, that.illnessIscr);
    }

    @Override
    public int hashCode() {

        return Objects.hash(illnessId, illnessName, illnessType, illnessIscr);
    }

    @OneToMany(mappedBy = "illnessByIllnessId")
    public Collection<MedicalRecordEntity> getMedicalRecordsByIllnessId() {
        return medicalRecordsByIllnessId;
    }

    public void setMedicalRecordsByIllnessId(Collection<MedicalRecordEntity> medicalRecordsByIllnessId) {
        this.medicalRecordsByIllnessId = medicalRecordsByIllnessId;
    }
}
