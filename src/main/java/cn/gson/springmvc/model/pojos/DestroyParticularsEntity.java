package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "destroy_particulars_", schema = "his", catalog = "")
public class DestroyParticularsEntity {
    private int destroyParticularsId;
    private Integer destroyParticularsSl;
    private YaopinEntity yaopinByYaopinId;
    private DestroyEntity destroyByDestroyId;

    @Id
    @Column(name = "destroy_particulars_id", nullable = false)
    public int getDestroyParticularsId() {
        return destroyParticularsId;
    }

    public void setDestroyParticularsId(int destroyParticularsId) {
        this.destroyParticularsId = destroyParticularsId;
    }

    @Basic
    @Column(name = "destroy_particulars_sl", nullable = true)
    public Integer getDestroyParticularsSl() {
        return destroyParticularsSl;
    }

    public void setDestroyParticularsSl(Integer destroyParticularsSl) {
        this.destroyParticularsSl = destroyParticularsSl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DestroyParticularsEntity that = (DestroyParticularsEntity) o;
        return destroyParticularsId == that.destroyParticularsId &&
                Objects.equals(destroyParticularsSl, that.destroyParticularsSl);
    }

    @Override
    public int hashCode() {

        return Objects.hash(destroyParticularsId, destroyParticularsSl);
    }

    @ManyToOne
    @JoinColumn(name = "yaopin_id", referencedColumnName = "yaopin_id")
    public YaopinEntity getYaopinByYaopinId() {
        return yaopinByYaopinId;
    }

    public void setYaopinByYaopinId(YaopinEntity yaopinByYaopinId) {
        this.yaopinByYaopinId = yaopinByYaopinId;
    }

    @ManyToOne
    @JoinColumn(name = "destroy_id", referencedColumnName = "destroy_id")
    public DestroyEntity getDestroyByDestroyId() {
        return destroyByDestroyId;
    }

    public void setDestroyByDestroyId(DestroyEntity destroyByDestroyId) {
        this.destroyByDestroyId = destroyByDestroyId;
    }
}
