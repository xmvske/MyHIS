package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "charge_detail_", schema = "his", catalog = "")
public class ChargeDetailEntity {
    private int chargeDetailId;
    private Integer price;
    private YizhuDetailEntity yizhuDetailByYizhuDetailId;
    private HealthProjectEntity healthProjectByProjectId;
    private ChargeEntity chargeByChargeId;
    private HospitalProjectEntity hospitalProjectByHospitalProjectId;

    @Id
    @Column(name = "charge_detail_id", nullable = false)
    public int getChargeDetailId() {
        return chargeDetailId;
    }

    public void setChargeDetailId(int chargeDetailId) {
        this.chargeDetailId = chargeDetailId;
    }

    @Basic
    @Column(name = "price", nullable = true, precision = 0)
    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChargeDetailEntity that = (ChargeDetailEntity) o;
        return chargeDetailId == that.chargeDetailId &&
                Objects.equals(price, that.price);
    }

    @Override
    public int hashCode() {

        return Objects.hash(chargeDetailId, price);
    }

    @ManyToOne
    @JoinColumn(name = "yizhu_detail_id", referencedColumnName = "yizhu_detail_id")
    public YizhuDetailEntity getYizhuDetailByYizhuDetailId() {
        return yizhuDetailByYizhuDetailId;
    }

    public void setYizhuDetailByYizhuDetailId(YizhuDetailEntity yizhuDetailByYizhuDetailId) {
        this.yizhuDetailByYizhuDetailId = yizhuDetailByYizhuDetailId;
    }

    @ManyToOne
    @JoinColumn(name = "project_id", referencedColumnName = "project_id")
    public HealthProjectEntity getHealthProjectByProjectId() {
        return healthProjectByProjectId;
    }

    public void setHealthProjectByProjectId(HealthProjectEntity healthProjectByProjectId) {
        this.healthProjectByProjectId = healthProjectByProjectId;
    }

    @ManyToOne
    @JoinColumn(name = "charge_id", referencedColumnName = "charge_id")
    public ChargeEntity getChargeByChargeId() {
        return chargeByChargeId;
    }

    public void setChargeByChargeId(ChargeEntity chargeByChargeId) {
        this.chargeByChargeId = chargeByChargeId;
    }

    @ManyToOne
    @JoinColumn(name = "hospital_project_id", referencedColumnName = "hospital_project_id")
    public HospitalProjectEntity getHospitalProjectByHospitalProjectId() {
        return hospitalProjectByHospitalProjectId;
    }

    public void setHospitalProjectByHospitalProjectId(HospitalProjectEntity hospitalProjectByHospitalProjectId) {
        this.hospitalProjectByHospitalProjectId = hospitalProjectByHospitalProjectId;
    }
}
