package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "function_", schema = "his", catalog = "")
public class FunctionEntity {
    private int funId;
    private String funName;

    @Id
    @Column(name = "fun_id", nullable = false)
    public int getFunId() {
        return funId;
    }

    public void setFunId(int funId) {
        this.funId = funId;
    }

    @Basic
    @Column(name = "fun_name", nullable = true, length = 30)
    public String getFunName() {
        return funName;
    }

    public void setFunName(String funName) {
        this.funName = funName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FunctionEntity that = (FunctionEntity) o;
        return funId == that.funId &&
                Objects.equals(funName, that.funName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(funId, funName);
    }
}
