package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "charge_", schema = "his", catalog = "")
public class ChargeEntity {
    private int chargeId;
    private Integer chargePrice;
    private Timestamp chargeDate;
    private Integer chargeState;
    private Integer chargeUiuz;
    private MedicalCardEntity medicalCardByMedicalId;
    private Collection<ChargeDetailEntity> chargeDetailsByChargeId;
    private RegistrationEntity registrationByRegistrationId;
    private Collection<DiagnoseEntity> diagnosesByChargeId;
    private Collection<MedicalCardRecordEntity> medicalCardRecordsByChargeId;
    private YizhuEntity yizhuByYizhuId;
    private HealthEntity healthByHealthId;


    @Id
    @Column(name = "charge_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getChargeId() {
        return chargeId;
    }

    public void setChargeId(int chargeId) {
        this.chargeId = chargeId;
    }

    @Basic
    @Column(name = "charge_price", nullable = true, precision = 0)
    public Integer getChargePrice() {
        return chargePrice;
    }

    public void setChargePrice(Integer chargePrice) {
        this.chargePrice = chargePrice;
    }

    @Basic
    @Column(name = "charge_date", nullable = true)
    public Timestamp getChargeDate() {
        return chargeDate;
    }

    public void setChargeDate(Timestamp chargeDate) {
        this.chargeDate = chargeDate;
    }

    @Basic
    @Column(name = "charge_state", nullable = true)
    public Integer getChargeState() {
        return chargeState;
    }

    public void setChargeState(Integer chargeState) {
        this.chargeState = chargeState;
    }

    @Basic
    @Column(name = "charge_uiuz", nullable = true)
    public Integer getChargeUiuz() {
        return chargeUiuz;
    }

    public void setChargeUiuz(Integer chargeUiuz) {
        this.chargeUiuz = chargeUiuz;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChargeEntity that = (ChargeEntity) o;
        return chargeId == that.chargeId &&
                Objects.equals(chargePrice, that.chargePrice) &&
                Objects.equals(chargeDate, that.chargeDate);
    }

    @Override
    public int hashCode() {

        return Objects.hash(chargeId, chargePrice, chargeDate);
    }

    @ManyToOne
    @JoinColumn(name = "medical_id", referencedColumnName = "medical_id")
    public MedicalCardEntity getMedicalCardByMedicalId() {
        return medicalCardByMedicalId;
    }

    public void setMedicalCardByMedicalId(MedicalCardEntity medicalCardByMedicalId) {
        this.medicalCardByMedicalId = medicalCardByMedicalId;
    }

    @OneToMany(mappedBy = "chargeByChargeId")
    public Collection<ChargeDetailEntity> getChargeDetailsByChargeId() {
        return chargeDetailsByChargeId;
    }

    public void setChargeDetailsByChargeId(Collection<ChargeDetailEntity> chargeDetailsByChargeId) {
        this.chargeDetailsByChargeId = chargeDetailsByChargeId;
    }

    @OneToMany(mappedBy = "chargeByChargeId")
    public Collection<DiagnoseEntity> getDiagnosesByChargeId() {
        return diagnosesByChargeId;
    }

    public void setDiagnosesByChargeId(Collection<DiagnoseEntity> diagnosesByChargeId) {
        this.diagnosesByChargeId = diagnosesByChargeId;
    }

    @OneToMany(mappedBy = "chargeByChargeId")
    public Collection<MedicalCardRecordEntity> getMedicalCardRecordsByChargeId() {
        return medicalCardRecordsByChargeId;
    }

    public void setMedicalCardRecordsByChargeId(Collection<MedicalCardRecordEntity> medicalCardRecordsByChargeId) {
        this.medicalCardRecordsByChargeId = medicalCardRecordsByChargeId;
    }

    @ManyToOne
    @JoinColumn(name = "registration_id", referencedColumnName = "registration_id")
    public RegistrationEntity getRegistrationByRegistrationId() {
        return registrationByRegistrationId;
    }

    public void setRegistrationByRegistrationId(RegistrationEntity registrationByRegistrationId) {
        this.registrationByRegistrationId = registrationByRegistrationId;
    }



    @ManyToOne
    @JoinColumn(name = "yizhu_id", referencedColumnName = "yizhu_id")
    public YizhuEntity getYizhuByYizhuId() {
        return yizhuByYizhuId;
    }


    public void setYizhuByYizhuId(YizhuEntity yizhuByYizhuId) {
        this.yizhuByYizhuId = yizhuByYizhuId;
    }

    @ManyToOne
    @JoinColumn(name = "health_id", referencedColumnName = "health_id")
    public HealthEntity getHealthByHealthId() {
        return healthByHealthId;
    }

    public void setHealthByHealthId(HealthEntity healthByHealthId) {
        this.healthByHealthId = healthByHealthId;
    }
}
