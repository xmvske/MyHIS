package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "registration_", schema = "his", catalog = "")
public class RegistrationEntity {
    private int registrationId;
    private Timestamp registrationDate;
    private Integer registrationType;
    private String registrationHao;
    private Integer registrationState;
    private Collection<DiagnoseEntity> diagnosesByRegistrationId;
    private Collection<ChargeEntity> chargesByRegistrationId;
    private MedicalCardEntity medicalCardByMedicalId;
    private KeshiEntity keshiByKeshiId;
    private UserEntity userByUserId;
    private UserEntity userByPhysician;



    @Id
    @Column(name = "registration_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(int registrationId) {
        this.registrationId = registrationId;
    }

    @Basic
    @Column(name = "registration_date", nullable = true)
    public Timestamp getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Timestamp registrationDate) {
        this.registrationDate = registrationDate;
    }

    @Basic
    @Column(name = "registration_type", nullable = true)
    public Integer getRegistrationType() {
        return registrationType;
    }

    public void setRegistrationType(Integer registrationType) {
        this.registrationType = registrationType;
    }

    @Basic
    @Column(name = "registration_hao", nullable = true, length = 30)
    public String getRegistrationHao() {
        return registrationHao;
    }

    public void setRegistrationHao(String registrationHao) {
        this.registrationHao = registrationHao;
    }

    @Basic
    @Column(name = "registration_state", nullable = true, length = 30)
    public Integer getRegistrationState() {
        return registrationState;
    }

    public void setRegistrationState(Integer registrationState) {
        this.registrationState = registrationState;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RegistrationEntity that = (RegistrationEntity) o;
        return registrationId == that.registrationId &&
                Objects.equals(registrationDate, that.registrationDate) &&
                Objects.equals(registrationType, that.registrationType) &&
                Objects.equals(registrationHao, that.registrationHao);
    }

    @Override
    public int hashCode() {

        return Objects.hash(registrationId, registrationDate, registrationType,  registrationHao);
    }

    @OneToMany(mappedBy = "registrationByRegistrationId")
    public Collection<DiagnoseEntity> getDiagnosesByRegistrationId() {
        return diagnosesByRegistrationId;
    }

    public void setDiagnosesByRegistrationId(Collection<DiagnoseEntity> diagnosesByRegistrationId) {
        this.diagnosesByRegistrationId = diagnosesByRegistrationId;
    }

    @OneToMany(mappedBy = "registrationByRegistrationId")
    public Collection<ChargeEntity> getChargesByRegistrationId() {
        return chargesByRegistrationId;
    }

    public void setChargesByRegistrationId(Collection<ChargeEntity> chargesByRegistrationId) {
        this.chargesByRegistrationId = chargesByRegistrationId;
    }

    @ManyToOne
    @JoinColumn(name = "medical_id", referencedColumnName = "medical_id")
    public MedicalCardEntity getMedicalCardByMedicalId() {
        return medicalCardByMedicalId;
    }

    public void setMedicalCardByMedicalId(MedicalCardEntity medicalCardByMedicalId) {
        this.medicalCardByMedicalId = medicalCardByMedicalId;
    }

    @ManyToOne
    @JoinColumn(name = "keshi_id", referencedColumnName = "keshi_id")
    public KeshiEntity getKeshiByKeshiId() {
        return keshiByKeshiId;
    }

    public void setKeshiByKeshiId(KeshiEntity keshiByKeshiId) {
        this.keshiByKeshiId = keshiByKeshiId;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getUserByUserId() {
        return userByUserId;
    }

    public void setUserByUserId(UserEntity userByUserId) {
        this.userByUserId = userByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "physician", referencedColumnName = "user_id")
    public UserEntity getUserByPhysician() {
        return userByPhysician;
    }

    public void setUserByPhysician(UserEntity userByPhysician) {
        this.userByPhysician = userByPhysician;
    }
}
