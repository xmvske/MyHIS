package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "rukutable_", schema = "his", catalog = "")
public class RukutableEntity {
    private int rukutableId;
    private Timestamp rukutableData;
    private String rukutableRen;
    private Integer rukutableZongshu;
    private Collection<InventoryDetailsEntity> inventoryDetailsByRukutableId;

    @Id
    @Column(name = "rukutable_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getRukutableId() {
        return rukutableId;
    }

    public void setRukutableId(int rukutableId) {
        this.rukutableId = rukutableId;
    }

    @Basic
    @Column(name = "rukutable_data", nullable = true)
    public Timestamp getRukutableData() {
        return rukutableData;
    }

    public void setRukutableData(Timestamp rukutableData) {
        this.rukutableData = rukutableData;
    }

    @Basic
    @Column(name = "rukutable_ren", nullable = true, length = 20)
    public String getRukutableRen() {
        return rukutableRen;
    }

    public void setRukutableRen(String rukutableRen) {
        this.rukutableRen = rukutableRen;
    }

    @Basic
    @Column(name = "rukutable_zongshu", nullable = true)
    public Integer getRukutableZongshu() {
        return rukutableZongshu;
    }

    public void setRukutableZongshu(Integer rukutableZongshu) {
        this.rukutableZongshu = rukutableZongshu;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RukutableEntity that = (RukutableEntity) o;
        return rukutableId == that.rukutableId &&
                Objects.equals(rukutableData, that.rukutableData) &&
                Objects.equals(rukutableRen, that.rukutableRen) &&
                Objects.equals(rukutableZongshu, that.rukutableZongshu);
    }

    @Override
    public int hashCode() {

        return Objects.hash(rukutableId, rukutableData, rukutableRen, rukutableZongshu);
    }

    @OneToMany(mappedBy = "rukutableByRukutableId")
    public Collection<InventoryDetailsEntity> getInventoryDetailsByRukutableId() {
        return inventoryDetailsByRukutableId;
    }

    public void setInventoryDetailsByRukutableId(Collection<InventoryDetailsEntity> inventoryDetailsByRukutableId) {
        this.inventoryDetailsByRukutableId = inventoryDetailsByRukutableId;
    }
}
