package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "health_detail_", schema = "his", catalog = "")
public class HealthDetailEntity {
    private int healthDetailId;
    private Integer hdNumber;
    private Integer hdAynum;
    private Integer hdState;
    private HealthMealEntity healthMealByMealId;
    private HealthEntity healthByHealthId;
    private Collection<HealthReportEntity> healthReportsByHealthDetailId;

    @Id
    @Column(name = "health_detail_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getHealthDetailId() {
        return healthDetailId;
    }

    public void setHealthDetailId(int healthDetailId) {
        this.healthDetailId = healthDetailId;
    }

    @Basic
    @Column(name = "hd_number", nullable = true, precision = 0)
    public Integer getHdNumber() {
        return hdNumber;
    }

    public void setHdNumber(Integer hdNumber) {
        this.hdNumber = hdNumber;
    }

    @Basic
    @Column(name = "hd_aynum", nullable = true, precision = 0)
    public Integer getHdAynum() {
        return hdAynum;
    }

    public void setHdAynum(Integer hdAynum) {
        this.hdAynum = hdAynum;
    }

    @Basic
    @Column(name = "hd_state", nullable = true)
    public Integer getHdState() {
        return hdState;
    }

    public void setHdState(Integer hdState) {
        this.hdState = hdState;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HealthDetailEntity that = (HealthDetailEntity) o;
        return healthDetailId == that.healthDetailId &&
                Objects.equals(hdNumber, that.hdNumber) &&
                Objects.equals(hdAynum, that.hdAynum) &&
                Objects.equals(hdState, that.hdState);
    }

    @Override
    public int hashCode() {

        return Objects.hash(healthDetailId, hdNumber, hdAynum, hdState);
    }

    @ManyToOne
    @JoinColumn(name = "meal_id", referencedColumnName = "meal_id")
    public HealthMealEntity getHealthMealByMealId() {
        return healthMealByMealId;
    }

    public void setHealthMealByMealId(HealthMealEntity healthMealByMealId) {
        this.healthMealByMealId = healthMealByMealId;
    }

    @ManyToOne
    @JoinColumn(name = "health_id", referencedColumnName = "health_id")
    public HealthEntity getHealthByHealthId() {
        return healthByHealthId;
    }

    public void setHealthByHealthId(HealthEntity healthByHealthId) {
        this.healthByHealthId = healthByHealthId;
    }

    @OneToMany(mappedBy = "healthDetailByHealthDetailId")
    public Collection<HealthReportEntity> getHealthReportsByHealthDetailId() {
        return healthReportsByHealthDetailId;
    }

    public void setHealthReportsByHealthDetailId(Collection<HealthReportEntity> healthReportsByHealthDetailId) {
        this.healthReportsByHealthDetailId = healthReportsByHealthDetailId;
    }
}
