package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "bed_", schema = "his", catalog = "")
public class BedEntity {
    private int bedId;
    private String bedWei;
    private Integer bedMoney;
    private Integer bedState;
    private KeshiEntity keshiByKeshiId;
    private Collection<BedChangeEntity> bedChangesByBedId;
    private Collection<HospEntity> hospsByBedId;
    private Collection<OuthospitalEntity> outhospitalsByBedId;

    @Id
    @Column(name = "bed_id", nullable = false)
    public int getBedId() {
        return bedId;
    }

    public void setBedId(int bedId) {
        this.bedId = bedId;
    }

    @Basic
    @Column(name = "bed_wei", nullable = true, length = 30)
    public String getBedWei() {
        return bedWei;
    }

    public void setBedWei(String bedWei) {
        this.bedWei = bedWei;
    }

    @Basic
    @Column(name = "bed_money", nullable = true, precision = 0)
    public Integer getBedMoney() {
        return bedMoney;
    }

    public void setBedMoney(Integer bedMoney) {
        this.bedMoney = bedMoney;
    }

    @Basic
    @Column(name = "bed_state", nullable = true)
    public Integer getBedState() {
        return bedState;
    }

    public void setBedState(Integer bedState) {
        this.bedState = bedState;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BedEntity bedEntity = (BedEntity) o;
        return bedId == bedEntity.bedId &&
                Objects.equals(bedWei, bedEntity.bedWei) &&
                Objects.equals(bedMoney, bedEntity.bedMoney) &&
                Objects.equals(bedState, bedEntity.bedState);
    }

    @Override
    public int hashCode() {

        return Objects.hash(bedId, bedWei, bedMoney, bedState);
    }

    @ManyToOne
    @JoinColumn(name = "keshi_id", referencedColumnName = "keshi_id")
    public KeshiEntity getKeshiByKeshiId() {
        return keshiByKeshiId;
    }

    public void setKeshiByKeshiId(KeshiEntity keshiByKeshiId) {
        this.keshiByKeshiId = keshiByKeshiId;
    }

    @OneToMany(mappedBy = "bedByBedId")
    public Collection<BedChangeEntity> getBedChangesByBedId() {
        return bedChangesByBedId;
    }

    public void setBedChangesByBedId(Collection<BedChangeEntity> bedChangesByBedId) {
        this.bedChangesByBedId = bedChangesByBedId;
    }

    @OneToMany(mappedBy = "bedByBedId")
    public Collection<HospEntity> getHospsByBedId() {
        return hospsByBedId;
    }

    public void setHospsByBedId(Collection<HospEntity> hospsByBedId) {
        this.hospsByBedId = hospsByBedId;
    }

    @OneToMany(mappedBy = "bedByBedId")
    public Collection<OuthospitalEntity> getOuthospitalsByBedId() {
        return outhospitalsByBedId;
    }

    public void setOuthospitalsByBedId(Collection<OuthospitalEntity> outhospitalsByBedId) {
        this.outhospitalsByBedId = outhospitalsByBedId;
    }
}
