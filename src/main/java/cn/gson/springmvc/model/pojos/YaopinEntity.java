package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "yaopin_", schema = "his", catalog = "")
public class YaopinEntity {
    private Long yaopinId;
    private String yaopinName;
    private BigDecimal yaopinDj;
    private String yaopinGuige;
    private String yaopinLeixin;
    private BigDecimal yaopinPfj;
    private BigDecimal yaopinZongjia;
    private Timestamp yaopingYxq;
    private Timestamp yaopinScrq;
    private Collection<DestroyParticularsEntity> destroyParticularsByYaopinId;
    private Collection<MedicalCardRecordEntity> medicalCardRecordsByYaopinId;
    private Collection<OutpatientEntity> outpatientsByYaopinId;
    private Collection<RepertoryEntity> repertoriesByYaopinId;
    private Collection<ThxqEntity> thxqsByYaopinId;
    private Collection<XiangqingEntity> xiangqingsByYaopinId;
    private GonyingsEntity gonyingsByGonyingsId;
    private Collection<YizhuDetailEntity> yizhuDetailsByYaopinId;
    private Collection<DrugapplyTableEntity> drugapplyTableByYaopinId;

    @Id
    @Column(name = "yaopin_id", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public Long getYaopinId() {
        return yaopinId;
    }

    public void setYaopinId(Long yaopinId) {
        this.yaopinId = yaopinId;
    }

    @Basic
    @Column(name = "yaopin_name", nullable = true, length = 20)
    public String getYaopinName() {
        return yaopinName;
    }

    public void setYaopinName(String yaopinName) {
        this.yaopinName = yaopinName;
    }

    @Basic
    @Column(name = "yaopin_dj", nullable = true, precision = 2)
    public BigDecimal getYaopinDj() {
        return yaopinDj;
    }

    public void setYaopinDj(BigDecimal yaopinDj) {
        this.yaopinDj = yaopinDj;
    }

    @Basic
    @Column(name = "yaopin_guige", nullable = true, length = 20)
    public String getYaopinGuige() {
        return yaopinGuige;
    }

    public void setYaopinGuige(String yaopinGuige) {
        this.yaopinGuige = yaopinGuige;
    }

    @Basic
    @Column(name = "yaopin_leixin", nullable = true, length = 20)
    public String getYaopinLeixin() {
        return yaopinLeixin;
    }

    public void setYaopinLeixin(String yaopinLeixin) {
        this.yaopinLeixin = yaopinLeixin;
    }

    @Basic
    @Column(name = "yaopin_pfj", nullable = true, precision = 2)
    public BigDecimal getYaopinPfj() {
        return yaopinPfj;
    }

    public void setYaopinPfj(BigDecimal yaopinPfj) {
        this.yaopinPfj = yaopinPfj;
    }

    @Basic
    @Column(name = "yaopin_zongjia", nullable = true, precision = 2)
    public BigDecimal getYaopinZongjia() {
        return yaopinZongjia;
    }

    public void setYaopinZongjia(BigDecimal yaopinZongjia) {
        this.yaopinZongjia = yaopinZongjia;
    }

    @Basic
    @Column(name = "yaoping_yxq", nullable = true)
    public Timestamp getYaopingYxq() {
        return yaopingYxq;
    }

    public void setYaopingYxq(Timestamp yaopingYxq) {
        this.yaopingYxq = yaopingYxq;
    }

    @Basic
    @Column(name = "yaopin_scrq", nullable = true)
    public Timestamp getYaopinScrq() {
        return yaopinScrq;
    }

    public void setYaopinScrq(Timestamp yaopinScrq) {
        this.yaopinScrq = yaopinScrq;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        YaopinEntity that = (YaopinEntity) o;
        return yaopinId == that.yaopinId &&
                Objects.equals(yaopinName, that.yaopinName) &&
                Objects.equals(yaopinDj, that.yaopinDj) &&
                Objects.equals(yaopinGuige, that.yaopinGuige) &&
                Objects.equals(yaopinLeixin, that.yaopinLeixin) &&
                Objects.equals(yaopinPfj, that.yaopinPfj) &&
                Objects.equals(yaopinZongjia, that.yaopinZongjia) &&
                Objects.equals(yaopingYxq, that.yaopingYxq) &&
                Objects.equals(yaopinScrq, that.yaopinScrq);
    }

    @Override
    public int hashCode() {

        return Objects.hash(yaopinId, yaopinName, yaopinDj, yaopinGuige, yaopinLeixin, yaopinPfj, yaopinZongjia, yaopingYxq, yaopinScrq);
    }

    @OneToMany(mappedBy = "yaopinByYaopinId")
    public Collection<DestroyParticularsEntity> getDestroyParticularsByYaopinId() {
        return destroyParticularsByYaopinId;
    }

    public void setDestroyParticularsByYaopinId(Collection<DestroyParticularsEntity> destroyParticularsByYaopinId) {
        this.destroyParticularsByYaopinId = destroyParticularsByYaopinId;
    }

    @OneToMany(mappedBy = "yaopinByYaopinId")
    public Collection<MedicalCardRecordEntity> getMedicalCardRecordsByYaopinId() {
        return medicalCardRecordsByYaopinId;
    }

    public void setMedicalCardRecordsByYaopinId(Collection<MedicalCardRecordEntity> medicalCardRecordsByYaopinId) {
        this.medicalCardRecordsByYaopinId = medicalCardRecordsByYaopinId;
    }

    @OneToMany(mappedBy = "yaopinByYaopinId")
    public Collection<OutpatientEntity> getOutpatientsByYaopinId() {
        return outpatientsByYaopinId;
    }

    public void setOutpatientsByYaopinId(Collection<OutpatientEntity> outpatientsByYaopinId) {
        this.outpatientsByYaopinId = outpatientsByYaopinId;
    }

    @OneToMany(mappedBy = "yaopinByYaopinId")
    public Collection<RepertoryEntity> getRepertoriesByYaopinId() {
        return repertoriesByYaopinId;
    }

    public void setRepertoriesByYaopinId(Collection<RepertoryEntity> repertoriesByYaopinId) {
        this.repertoriesByYaopinId = repertoriesByYaopinId;
    }

    @OneToMany(mappedBy = "yaopinByYaopinId")
    public Collection<ThxqEntity> getThxqsByYaopinId() {
        return thxqsByYaopinId;
    }

    public void setThxqsByYaopinId(Collection<ThxqEntity> thxqsByYaopinId) {
        this.thxqsByYaopinId = thxqsByYaopinId;
    }

    @OneToMany(mappedBy = "yaopinByYaopinId")
    public Collection<XiangqingEntity> getXiangqingsByYaopinId() {
        return xiangqingsByYaopinId;
    }

    public void setXiangqingsByYaopinId(Collection<XiangqingEntity> xiangqingsByYaopinId) {
        this.xiangqingsByYaopinId = xiangqingsByYaopinId;
    }

    @OneToMany(mappedBy = "yaopinByYaopinId")
    public Collection<DrugapplyTableEntity> getDrugapplyTableByYaopinId() {
        return drugapplyTableByYaopinId;
    }

    public void setDrugapplyTableByYaopinId(Collection<DrugapplyTableEntity> drugapplyTableByYaopinId) {
        this.drugapplyTableByYaopinId = drugapplyTableByYaopinId;
    }

    @ManyToOne
    @JoinColumn(name = "gonyings_id", referencedColumnName = "gonyings_id")
    public GonyingsEntity getGonyingsByGonyingsId() {
        return gonyingsByGonyingsId;
    }

    public void setGonyingsByGonyingsId(GonyingsEntity gonyingsByGonyingsId) {
        this.gonyingsByGonyingsId = gonyingsByGonyingsId;
    }

    @OneToMany(mappedBy = "yaopinByYaopinId")
    public Collection<YizhuDetailEntity> getYizhuDetailsByYaopinId() {
        return yizhuDetailsByYaopinId;
    }

    public void setYizhuDetailsByYaopinId(Collection<YizhuDetailEntity> yizhuDetailsByYaopinId) {
        this.yizhuDetailsByYaopinId = yizhuDetailsByYaopinId;
    }
}
