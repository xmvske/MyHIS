package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "yizhu_detail_", schema = "his", catalog = "")
public class YizhuDetailEntity {
    private Long yizhuDetailId;
    private Integer drugNumber;
    private Integer drugOnce;
    private Integer yizhuState;
    private String drugUsage;
    private Collection<ChargeDetailEntity> chargeDetailsByYizhuDetailId;
    private Collection<FayaoxqEntity> fayaoxqsByYizhuDetailId;
    private YizhuEntity yizhuByYizhuId;
    private YaopinEntity yaopinByYaopinId;

    @Id
    @Column(name = "yizhu_detail_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getYizhuDetailId() {
        return yizhuDetailId;
    }

    public void setYizhuDetailId(Long yizhuDetailId) {
        this.yizhuDetailId = yizhuDetailId;
    }

    @Basic
    @Column(name = "drug_number", nullable = true)
    public Integer getDrugNumber() {
        return drugNumber;
    }

    public void setDrugNumber(Integer drugNumber) {
        this.drugNumber = drugNumber;
    }

    @Basic
    @Column(name = "drug_once", nullable = true)
    public Integer getDrugOnce() {
        return drugOnce;
    }

    public void setDrugOnce(Integer drugOnce) {
        this.drugOnce = drugOnce;
    }

    @Basic
    @Column(name = "yizhu_state", nullable = true)
    public Integer getYizhuState() {
        return yizhuState;
    }

    public void setYizhuState(Integer yizhuState) {
        this.yizhuState = yizhuState;
    }

    @Basic
    @Column(name = "drug_usage", nullable = true)
    public String getDrugUsage() {
        return drugUsage;
    }

    public void setDrugUsage(String drugUsage) {
        this.drugUsage = drugUsage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        YizhuDetailEntity that = (YizhuDetailEntity) o;
        return yizhuDetailId == that.yizhuDetailId &&
                Objects.equals(drugNumber, that.drugNumber) &&
                Objects.equals(drugOnce, that.drugOnce) &&
                Objects.equals(yizhuState, that.yizhuState);
    }

    @Override
    public int hashCode() {

        return Objects.hash(yizhuDetailId, drugNumber, drugOnce, yizhuState);
    }

    @OneToMany(mappedBy = "yizhuDetailByYizhuDetailId")
    public Collection<ChargeDetailEntity> getChargeDetailsByYizhuDetailId() {
        return chargeDetailsByYizhuDetailId;
    }

    public void setChargeDetailsByYizhuDetailId(Collection<ChargeDetailEntity> chargeDetailsByYizhuDetailId) {
        this.chargeDetailsByYizhuDetailId = chargeDetailsByYizhuDetailId;
    }

    @OneToMany(mappedBy = "yizhuDetailByYizhuDetailId")
    public Collection<FayaoxqEntity> getFayaoxqsByYizhuDetailId() {
        return fayaoxqsByYizhuDetailId;
    }

    public void setFayaoxqsByYizhuDetailId(Collection<FayaoxqEntity> fayaoxqsByYizhuDetailId) {
        this.fayaoxqsByYizhuDetailId = fayaoxqsByYizhuDetailId;
    }

    @ManyToOne
    @JoinColumn(name = "yizhu_id", referencedColumnName = "yizhu_id")
    public YizhuEntity getYizhuByYizhuId() {
        return yizhuByYizhuId;
    }

    public void setYizhuByYizhuId(YizhuEntity yizhuByYizhuId) {
        this.yizhuByYizhuId = yizhuByYizhuId;
    }

    @ManyToOne
    @JoinColumn(name = "yaopin_id", referencedColumnName = "yaopin_id")
    public YaopinEntity getYaopinByYaopinId() {
        return yaopinByYaopinId;
    }

    public void setYaopinByYaopinId(YaopinEntity yaopinByYaopinId) {
        this.yaopinByYaopinId = yaopinByYaopinId;
    }
}
