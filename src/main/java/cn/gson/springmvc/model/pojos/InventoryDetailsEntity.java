package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "inventory_details_", schema = "his", catalog = "")
public class InventoryDetailsEntity {
    private int inventoryDetailsId;
    private Integer inventoryDetailsSl;
    private OutpatientEntity outpatientByOutpatientId;
    private RukutableEntity rukutableByRukutableId;

    @Id
    @Column(name = "inventory_details_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getInventoryDetailsId() {
        return inventoryDetailsId;
    }

    public void setInventoryDetailsId(int inventoryDetailsId) {
        this.inventoryDetailsId = inventoryDetailsId;
    }

    @Basic
    @Column(name = "inventory_details_sl", nullable = true)
    public Integer getInventoryDetailsSl() {
        return inventoryDetailsSl;
    }

    public void setInventoryDetailsSl(Integer inventoryDetailsSl) {
        this.inventoryDetailsSl = inventoryDetailsSl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InventoryDetailsEntity that = (InventoryDetailsEntity) o;
        return inventoryDetailsId == that.inventoryDetailsId &&
                Objects.equals(inventoryDetailsSl, that.inventoryDetailsSl);
    }

    @Override
    public int hashCode() {

        return Objects.hash(inventoryDetailsId, inventoryDetailsSl);
    }

    @ManyToOne
    @JoinColumn(name = "outpatient_id", referencedColumnName = "outpatient_id")
    public OutpatientEntity getOutpatientByOutpatientId() {
        return outpatientByOutpatientId;
    }

    public void setOutpatientByOutpatientId(OutpatientEntity outpatientByOutpatientId) {
        this.outpatientByOutpatientId = outpatientByOutpatientId;
    }

    @ManyToOne
    @JoinColumn(name = "rukutable_id", referencedColumnName = "rukutable_id")
    public RukutableEntity getRukutableByRukutableId() {
        return rukutableByRukutableId;
    }

    public void setRukutableByRukutableId(RukutableEntity rukutableByRukutableId) {
        this.rukutableByRukutableId = rukutableByRukutableId;
    }
}
