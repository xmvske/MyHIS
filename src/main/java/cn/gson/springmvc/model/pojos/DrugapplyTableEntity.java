package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "drugapply_table_", schema = "his", catalog = "")
public class DrugapplyTableEntity {
    private int drugapplyTableId;
    private Integer drugapplyTableYaolian;
    private BigDecimal drugapplyTableDj;
    private DrugapplyEntity drugapplyByDrugapplytableId;
    private YaopinEntity  yaopinByYaopinId;

    @Id
    @Column(name = "drugapply_table_id", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int getDrugapplyTableId() {
        return drugapplyTableId;
    }

    public void setDrugapplyTableId(int drugapplyTableId) {
        this.drugapplyTableId = drugapplyTableId;
    }

    @Basic
    @Column(name = "drugapply_table_yaolian", nullable = true)
    public Integer getDrugapplyTableYaolian() {
        return drugapplyTableYaolian;
    }

    public void setDrugapplyTableYaolian(Integer drugapplyTableYaolian) {
        this.drugapplyTableYaolian = drugapplyTableYaolian;
    }

    @Basic
    @Column(name = "drugapply_table_dj", nullable = true, precision = 2)
    public BigDecimal getDrugapplyTableDj() {
        return drugapplyTableDj;
    }

    public void setDrugapplyTableDj(BigDecimal drugapplyTableDj) {
        this.drugapplyTableDj = drugapplyTableDj;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DrugapplyTableEntity that = (DrugapplyTableEntity) o;
        return drugapplyTableId == that.drugapplyTableId &&
                Objects.equals(drugapplyTableYaolian, that.drugapplyTableYaolian) &&
                Objects.equals(drugapplyTableDj, that.drugapplyTableDj);
    }

    @ManyToOne
    @JoinColumn(name = "drugapply_id" ,referencedColumnName = "drugapply_id")
    public DrugapplyEntity getDrugapplyByDrugapplytableId() {
        return drugapplyByDrugapplytableId;
    }

    public void setDrugapplyByDrugapplytableId(DrugapplyEntity drugapplyByDrugapplytableId) {
        this.drugapplyByDrugapplytableId = drugapplyByDrugapplytableId;
    }

    @ManyToOne
    @JoinColumn(name = "yaopin_id" ,referencedColumnName = "yaopin_id")
    public YaopinEntity getYaopinByYaopinId() {
        return yaopinByYaopinId;
    }

    public void setYaopinByYaopinId(YaopinEntity yaopinByYaopinId) {
        this.yaopinByYaopinId = yaopinByYaopinId;
    }

    @Override
    public int hashCode() {

        return Objects.hash(drugapplyTableId, drugapplyTableYaolian, drugapplyTableDj);
    }
}
