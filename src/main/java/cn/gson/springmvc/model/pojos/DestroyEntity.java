package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "destroy_", schema = "his", catalog = "")
public class DestroyEntity {
    private int destroyId;
    private Timestamp destroyTime;
    private Integer destroyZt;
    private String destroyYy;
    private String destroyFzr;
    private Collection<DestroyParticularsEntity> destroyParticularsByDestroyId;

    @Id
    @Column(name = "destroy_id", nullable = false)
    public int getDestroyId() {
        return destroyId;
    }

    public void setDestroyId(int destroyId) {
        this.destroyId = destroyId;
    }

    @Basic
    @Column(name = "destroy_time", nullable = true)
    public Timestamp getDestroyTime() {
        return destroyTime;
    }

    public void setDestroyTime(Timestamp destroyTime) {
        this.destroyTime = destroyTime;
    }

    @Basic
    @Column(name = "destroy_zt", nullable = true)
    public Integer getDestroyZt() {
        return destroyZt;
    }

    public void setDestroyZt(Integer destroyZt) {
        this.destroyZt = destroyZt;
    }

    @Basic
    @Column(name = "destroy_yy", nullable = true, length = 100)
    public String getDestroyYy() {
        return destroyYy;
    }

    public void setDestroyYy(String destroyYy) {
        this.destroyYy = destroyYy;
    }

    @Basic
    @Column(name = "destroy_fzr", nullable = true, length = 20)
    public String getDestroyFzr() {
        return destroyFzr;
    }

    public void setDestroyFzr(String destroyFzr) {
        this.destroyFzr = destroyFzr;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DestroyEntity that = (DestroyEntity) o;
        return destroyId == that.destroyId &&
                Objects.equals(destroyTime, that.destroyTime) &&
                Objects.equals(destroyZt, that.destroyZt) &&
                Objects.equals(destroyYy, that.destroyYy) &&
                Objects.equals(destroyFzr, that.destroyFzr);
    }

    @Override
    public int hashCode() {

        return Objects.hash(destroyId, destroyTime, destroyZt, destroyYy, destroyFzr);
    }

    @OneToMany(mappedBy = "destroyByDestroyId")
    public Collection<DestroyParticularsEntity> getDestroyParticularsByDestroyId() {
        return destroyParticularsByDestroyId;
    }

    public void setDestroyParticularsByDestroyId(Collection<DestroyParticularsEntity> destroyParticularsByDestroyId) {
        this.destroyParticularsByDestroyId = destroyParticularsByDestroyId;
    }
}
