package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "zhuyuan_apply_", schema = "his", catalog = "")
public class ZhuyuanApplyEntity {
    private Long zhuyuanApplyId;
    private Date applyDate;
    private Integer applyState;
    private Collection<HospEntity> hospsByZhuyuanApplyId;
    private MedicalCardEntity medicalCardByMedicalId;

    @Id
    @Column(name = "zhuyuan_apply_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    public Long getZhuyuanApplyId() {
        return zhuyuanApplyId;
    }

    public void setZhuyuanApplyId(Long zhuyuanApplyId) {
        this.zhuyuanApplyId = zhuyuanApplyId;
    }

    @Basic
    @Column(name = "apply_date", nullable = true)
    public Date getApplyDate() {
        return applyDate;
    }

    public void setApplyDate(Date applyDate) {
        this.applyDate = applyDate;
    }

    @Basic
    @Column(name = "apply_state", nullable = true)
    public Integer getApplyState() {
        return applyState;
    }

    public void setApplyState(Integer applyState) {
        this.applyState = applyState;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ZhuyuanApplyEntity that = (ZhuyuanApplyEntity) o;
        return zhuyuanApplyId == that.zhuyuanApplyId &&
                Objects.equals(applyDate, that.applyDate) &&
                Objects.equals(applyState, that.applyState);
    }

    @Override
    public int hashCode() {

        return Objects.hash(zhuyuanApplyId, applyDate, applyState);
    }

    @OneToMany(mappedBy = "zhuyuanApplyByZhuyuanApplyId")
    public Collection<HospEntity> getHospsByZhuyuanApplyId() {
        return hospsByZhuyuanApplyId;
    }

    public void setHospsByZhuyuanApplyId(Collection<HospEntity> hospsByZhuyuanApplyId) {
        this.hospsByZhuyuanApplyId = hospsByZhuyuanApplyId;
    }

    @ManyToOne
    @JoinColumn(name = "medical_id", referencedColumnName = "medical_id")
    public MedicalCardEntity getMedicalCardByMedicalId() {
        return medicalCardByMedicalId;
    }

    public void setMedicalCardByMedicalId(MedicalCardEntity medicalCardByMedicalId) {
        this.medicalCardByMedicalId = medicalCardByMedicalId;
    }
}
