package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "health_report_", schema = "his", catalog = "")
public class HealthReportEntity {
    private int reportId;
    private String reportResult;
    private HealthDetailEntity healthDetailByHealthDetailId;
    private HealthProjectEntity healthProjectByProjectId;

    @Id
    @Column(name = "report_id", nullable = false)
    public int getReportId() {
        return reportId;
    }

    public void setReportId(int reportId) {
        this.reportId = reportId;
    }

    @Basic
    @Column(name = "report_result", nullable = true, length = 200)
    public String getReportResult() {
        return reportResult;
    }

    public void setReportResult(String reportResult) {
        this.reportResult = reportResult;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HealthReportEntity that = (HealthReportEntity) o;
        return reportId == that.reportId &&
                Objects.equals(reportResult, that.reportResult);
    }

    @Override
    public int hashCode() {

        return Objects.hash(reportId, reportResult);
    }

    @ManyToOne
    @JoinColumn(name = "health_detail_id", referencedColumnName = "health_detail_id")
    public HealthDetailEntity getHealthDetailByHealthDetailId() {
        return healthDetailByHealthDetailId;
    }

    public void setHealthDetailByHealthDetailId(HealthDetailEntity healthDetailByHealthDetailId) {
        this.healthDetailByHealthDetailId = healthDetailByHealthDetailId;
    }

    @ManyToOne
    @JoinColumn(name = "project_id", referencedColumnName = "project_id")
    public HealthProjectEntity getHealthProjectByProjectId() {
        return healthProjectByProjectId;
    }

    public void setHealthProjectByProjectId(HealthProjectEntity healthProjectByProjectId) {
        this.healthProjectByProjectId = healthProjectByProjectId;
    }
}
