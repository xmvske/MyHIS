package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "keshi_", schema = "his", catalog = "")
public class KeshiEntity {
    private int keshiId;
    private String keshiName;
    private Integer keshiGhmoney;
    private Collection<BedEntity> bedsByKeshiId;
    private Collection<DiagnoseEntity> diagnosesByKeshiId;
    private Collection<HealthProjectEntity> healthProjectsByKeshiId;
    private Collection<HospEntity> hospsByKeshiId;
    private Collection<OperationEntity> operationsByKeshiId;
    private Collection<OuthospitalEntity> outhospitalsByKeshiId;
    private Collection<RegistrationEntity> registrationsByKeshiId;
    private Collection<RoomEntity> roomsByKeshiId;
    private Collection<UserEntity> usersByKeshiId;

    @Id
    @Column(name = "keshi_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getKeshiId() {
        return keshiId;
    }

    public void setKeshiId(int keshiId) {
        this.keshiId = keshiId;
    }

    @Basic
    @Column(name = "keshi_name", nullable = true, length = 30)
    public String getKeshiName() {
        return keshiName;
    }

    public void setKeshiName(String keshiName) {
        this.keshiName = keshiName;
    }

    @Basic
    @Column(name = "keshi_ghmoney", nullable = true)
    public Integer getKeshiGhmoney() {
        return keshiGhmoney;
    }

    public void setKeshiGhmoney(Integer keshiGhmoney) {
        this.keshiGhmoney = keshiGhmoney;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        KeshiEntity that = (KeshiEntity) o;
        return keshiId == that.keshiId &&
                Objects.equals(keshiName, that.keshiName) &&
                Objects.equals(keshiGhmoney, that.keshiGhmoney);
    }

    @Override
    public int hashCode() {

        return Objects.hash(keshiId, keshiName, keshiGhmoney);
    }

    @OneToMany(mappedBy = "keshiByKeshiId")
    public Collection<BedEntity> getBedsByKeshiId() {
        return bedsByKeshiId;
    }

    public void setBedsByKeshiId(Collection<BedEntity> bedsByKeshiId) {
        this.bedsByKeshiId = bedsByKeshiId;
    }

    @OneToMany(mappedBy = "keshiByKeshiId")
    public Collection<DiagnoseEntity> getDiagnosesByKeshiId() {
        return diagnosesByKeshiId;
    }

    public void setDiagnosesByKeshiId(Collection<DiagnoseEntity> diagnosesByKeshiId) {
        this.diagnosesByKeshiId = diagnosesByKeshiId;
    }

    @OneToMany(mappedBy = "keshiByKeshiId")
    public Collection<HealthProjectEntity> getHealthProjectsByKeshiId() {
        return healthProjectsByKeshiId;
    }

    public void setHealthProjectsByKeshiId(Collection<HealthProjectEntity> healthProjectsByKeshiId) {
        this.healthProjectsByKeshiId = healthProjectsByKeshiId;
    }

    @OneToMany(mappedBy = "keshiByKeshiId")
    public Collection<HospEntity> getHospsByKeshiId() {
        return hospsByKeshiId;
    }

    public void setHospsByKeshiId(Collection<HospEntity> hospsByKeshiId) {
        this.hospsByKeshiId = hospsByKeshiId;
    }

    @OneToMany(mappedBy = "keshiByKeshiId")
    public Collection<OperationEntity> getOperationsByKeshiId() {
        return operationsByKeshiId;
    }

    public void setOperationsByKeshiId(Collection<OperationEntity> operationsByKeshiId) {
        this.operationsByKeshiId = operationsByKeshiId;
    }

    @OneToMany(mappedBy = "keshiByKeshiId")
    public Collection<OuthospitalEntity> getOuthospitalsByKeshiId() {
        return outhospitalsByKeshiId;
    }

    public void setOuthospitalsByKeshiId(Collection<OuthospitalEntity> outhospitalsByKeshiId) {
        this.outhospitalsByKeshiId = outhospitalsByKeshiId;
    }

    @OneToMany(mappedBy = "keshiByKeshiId")
    public Collection<RegistrationEntity> getRegistrationsByKeshiId() {
        return registrationsByKeshiId;
    }

    public void setRegistrationsByKeshiId(Collection<RegistrationEntity> registrationsByKeshiId) {
        this.registrationsByKeshiId = registrationsByKeshiId;
    }

    @OneToMany(mappedBy = "keshiByKeshiId")
    public Collection<RoomEntity> getRoomsByKeshiId() {
        return roomsByKeshiId;
    }

    public void setRoomsByKeshiId(Collection<RoomEntity> roomsByKeshiId) {
        this.roomsByKeshiId = roomsByKeshiId;
    }

    @OneToMany(mappedBy = "keshiByKeshiId")
    public Collection<UserEntity> getUsersByKeshiId() {
        return usersByKeshiId;
    }

    public void setUsersByKeshiId(Collection<UserEntity> usersByKeshiId) {
        this.usersByKeshiId = usersByKeshiId;
    }
}
