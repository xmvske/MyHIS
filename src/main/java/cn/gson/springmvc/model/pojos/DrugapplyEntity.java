package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "drugapply_", schema = "his", catalog = "")
public class DrugapplyEntity {
    private int drugapplyId;
    private Integer drugapplySl;
    private BigDecimal drugapplyDj;
    private String drugapplyGuige;
    private BigDecimal drugapplyJe;
    private String drugapplyFzr;
    private String drugapplyClzt;
    private RepertoryEntity repertoryByRepertoryId;
    private Collection<DrugapplyTableEntity> drugapplyTableByDrugapplyId;

    @Id
    @Column(name = "drugapply_id", nullable = false)
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public int getDrugapplyId() {
        return drugapplyId;
    }

    public void setDrugapplyId(int drugapplyId) {
        this.drugapplyId = drugapplyId;
    }

    @Basic
    @Column(name = "drugapply_sl", nullable = true)
    public Integer getDrugapplySl() {
        return drugapplySl;
    }

    public void setDrugapplySl(Integer drugapplySl) {
        this.drugapplySl = drugapplySl;
    }

    @Basic
    @Column(name = "drugapply_dj", nullable = true, precision = 2)
    public BigDecimal getDrugapplyDj() {
        return drugapplyDj;
    }

    public void setDrugapplyDj(BigDecimal drugapplyDj) {
        this.drugapplyDj = drugapplyDj;
    }

    @Basic
    @Column(name = "drugapply_guige", nullable = true, length = 20)
    public String getDrugapplyGuige() {
        return drugapplyGuige;
    }

    public void setDrugapplyGuige(String drugapplyGuige) {
        this.drugapplyGuige = drugapplyGuige;
    }

    @Basic
    @Column(name = "drugapply_je", nullable = true, precision = 2)
    public BigDecimal getDrugapplyJe() {
        return drugapplyJe;
    }

    public void setDrugapplyJe(BigDecimal drugapplyJe) {
        this.drugapplyJe = drugapplyJe;
    }

    @Basic
    @Column(name = "drugapply_fzr", nullable = true, length = 20)
    public String getDrugapplyFzr() {
        return drugapplyFzr;
    }

    public void setDrugapplyFzr(String drugapplyFzr) {
        this.drugapplyFzr = drugapplyFzr;
    }

    @Basic
    @Column(name = "drugapply_clzt", nullable = true, length = 20)
    public String getDrugapplyClzt() {
        return drugapplyClzt;
    }

    public void setDrugapplyClzt(String drugapplyClzt) {
        this.drugapplyClzt = drugapplyClzt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DrugapplyEntity that = (DrugapplyEntity) o;
        return drugapplyId == that.drugapplyId &&
                Objects.equals(drugapplySl, that.drugapplySl) &&
                Objects.equals(drugapplyDj, that.drugapplyDj) &&
                Objects.equals(drugapplyGuige, that.drugapplyGuige) &&
                Objects.equals(drugapplyJe, that.drugapplyJe) &&
                Objects.equals(drugapplyFzr, that.drugapplyFzr) &&
                Objects.equals(drugapplyClzt, that.drugapplyClzt);
    }

    @Override
    public int hashCode() {

        return Objects.hash(drugapplyId, drugapplySl, drugapplyDj, drugapplyGuige, drugapplyJe, drugapplyFzr, drugapplyClzt);
    }

    @OneToMany(mappedBy = "drugapplyByDrugapplytableId")
    public Collection<DrugapplyTableEntity> getDrugapplyTableByDrugapplyId() {
        return drugapplyTableByDrugapplyId;
    }

    public void setDrugapplyTableByDrugapplyId(Collection<DrugapplyTableEntity> drugapplyTableByDrugapplyId) {
        this.drugapplyTableByDrugapplyId = drugapplyTableByDrugapplyId;
    }

    @ManyToOne
    @JoinColumn(name = "repertory_id", referencedColumnName = "repertory_id")
    public RepertoryEntity getRepertoryByRepertoryId() {
        return repertoryByRepertoryId;
    }

    public void setRepertoryByRepertoryId(RepertoryEntity repertoryByRepertoryId) {
        this.repertoryByRepertoryId = repertoryByRepertoryId;
    }
}
