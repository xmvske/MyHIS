package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "op_record_", schema = "his", catalog = "")
public class OpRecordEntity {
    private Long opRecordId;
    private Date opDtate;
    private Timestamp opOutdate;
    private OperationEntity operationByOperationId;
    private String opResult;

    @Id
    @Column(name = "op_record_id", nullable = false)

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getOpRecordId() {
        return opRecordId;
    }

    public void setOpRecordId(Long opRecordId) {
        this.opRecordId = opRecordId;
    }

    @Basic
    @Column(name = "op_dtate", nullable = true)
    public Date getOpDtate() {
        return opDtate;
    }

    public void setOpDtate(Date opDtate) {
        this.opDtate = opDtate;
    }

    @Basic
    @Column(name = "op_outdate", nullable = true)
    public Timestamp getOpOutdate() {
        return opOutdate;
    }


    public void setOpOutdate(Timestamp opOutdate) {
        this.opOutdate = opOutdate;
    }


    @Basic
    @Column(name = "op_result", nullable = true)
    public String getOpResult() {
        return opResult;
    }

    public void setOpResult(String opResult) {
        this.opResult = opResult;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OpRecordEntity that = (OpRecordEntity) o;
        return opRecordId == that.opRecordId &&
                Objects.equals(opDtate, that.opDtate) &&
                Objects.equals(opOutdate, that.opOutdate)&&
                Objects.equals(opResult, that.opResult);
    }

    @Override
    public int hashCode() {

        return Objects.hash(opRecordId, opDtate, opOutdate,opResult);
    }

    @ManyToOne
    @JoinColumn(name = "operation_id", referencedColumnName = "operation_id")
    public OperationEntity getOperationByOperationId() {
        return operationByOperationId;
    }

    public void setOperationByOperationId(OperationEntity operationByOperationId) {
        this.operationByOperationId = operationByOperationId;
    }
}
