package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "medical_record_", schema = "his", catalog = "")
public class MedicalRecordEntity {
    private int medicalRecordId;
    private String symptom;
    private String cause;
    private Date mrDate;
    private String mrTemp;
    private String mrBreathe;
    private String mrPulse;
    private String mrBp;
    private Collection<DiagnoseEntity> diagnosesByMedicalRecordId;
    private IllnessEntity illnessByIllnessId;
    private UserEntity userByUserId;
    private MedicalCardEntity medicalCardByMedicalId;

    @Id
    @Column(name = "medical_record_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getMedicalRecordId() {
        return medicalRecordId;
    }

    public void setMedicalRecordId(int medicalRecordId) {
        this.medicalRecordId = medicalRecordId;
    }

    @Basic
    @Column(name = "symptom", nullable = true, length = 100)
    public String getSymptom() {
        return symptom;
    }

    public void setSymptom(String symptom) {
        this.symptom = symptom;
    }

    @Basic
    @Column(name = "cause", nullable = true, length = 100)
    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    @Basic
    @Column(name = "mr_date", nullable = true)
    public Date getMrDate() {
        return mrDate;
    }

    public void setMrDate(Date mrDate) {
        this.mrDate = mrDate;
    }

    @Basic
    @Column(name = "mr_temp", nullable = true, length = 30)
    public String getMrTemp() {
        return mrTemp;
    }

    public void setMrTemp(String mrTemp) {
        this.mrTemp = mrTemp;
    }

    @Basic
    @Column(name = "mr_breathe", nullable = true, length = 50)
    public String getMrBreathe() {
        return mrBreathe;
    }

    public void setMrBreathe(String mrBreathe) {
        this.mrBreathe = mrBreathe;
    }

    @Basic
    @Column(name = "mr_pulse", nullable = true, length = 50)
    public String getMrPulse() {
        return mrPulse;
    }

    public void setMrPulse(String mrPulse) {
        this.mrPulse = mrPulse;
    }

    @Basic
    @Column(name = "mr_bp", nullable = true, length = 50)
    public String getMrBp() {
        return mrBp;
    }

    public void setMrBp(String mrBp) {
        this.mrBp = mrBp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicalRecordEntity that = (MedicalRecordEntity) o;
        return medicalRecordId == that.medicalRecordId &&
                Objects.equals(symptom, that.symptom) &&
                Objects.equals(cause, that.cause) &&
                Objects.equals(mrDate, that.mrDate) &&
                Objects.equals(mrTemp, that.mrTemp) &&
                Objects.equals(mrBreathe, that.mrBreathe) &&
                Objects.equals(mrPulse, that.mrPulse) &&
                Objects.equals(mrBp, that.mrBp);
    }

    @Override
    public int hashCode() {

        return Objects.hash(medicalRecordId, symptom, cause, mrDate, mrTemp, mrBreathe, mrPulse, mrBp);
    }

    @OneToMany(mappedBy = "medicalRecordByMedicalRecordId")
    public Collection<DiagnoseEntity> getDiagnosesByMedicalRecordId() {
        return diagnosesByMedicalRecordId;
    }

    public void setDiagnosesByMedicalRecordId(Collection<DiagnoseEntity> diagnosesByMedicalRecordId) {
        this.diagnosesByMedicalRecordId = diagnosesByMedicalRecordId;
    }

    @ManyToOne
    @JoinColumn(name = "illness_id", referencedColumnName = "illness_id")
    public IllnessEntity getIllnessByIllnessId() {
        return illnessByIllnessId;
    }

    public void setIllnessByIllnessId(IllnessEntity illnessByIllnessId) {
        this.illnessByIllnessId = illnessByIllnessId;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getUserByUserId() {
        return userByUserId;
    }

    public void setUserByUserId(UserEntity userByUserId) {
        this.userByUserId = userByUserId;
    }

    @ManyToOne
    @JoinColumn(name = "medical_id", referencedColumnName = "medical_id")
    public MedicalCardEntity getMedicalCardByMedicalId() {
        return medicalCardByMedicalId;
    }

    public void setMedicalCardByMedicalId(MedicalCardEntity medicalCardByMedicalId) {
        this.medicalCardByMedicalId = medicalCardByMedicalId;
    }
}
