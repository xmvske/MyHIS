package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "operation_", schema = "his", catalog = "")
public class OperationEntity {
    private Long operationId;
    private String operationDocName;
    private String operationHelper;
    private String operationReason;
    private Integer operationState;
    private Timestamp operationDate;
    private Integer operationMoney;
    private String operationHao;
    private Collection<OpRecordEntity> opRecordsByOperationId;
    private OpSqEntity opSqBySqRshenqingId2;
    private RoomEntity roomByRoomId;
    private RtypeEntity rtypeByRtypeId;
    private KeshiEntity keshiByKeshiId;

    @Id
    @Column(name = "operation_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getOperationId() {
        return operationId;
    }

    public void setOperationId(Long operationId) {
        this.operationId = operationId;
    }

    @Basic
    @Column(name = "operation_doc_name", nullable = true, length = 30)
    public String getOperationDocName() {
        return operationDocName;
    }

    public void setOperationDocName(String operationDocName) {
        this.operationDocName = operationDocName;
    }

    @Basic
    @Column(name = "operation_helper", nullable = true, length = 30)
    public String getOperationHelper() {
        return operationHelper;
    }

    public void setOperationHelper(String operationHelper) {
        this.operationHelper = operationHelper;
    }

    @Basic
    @Column(name = "operation_reason", nullable = true, length = 200)
    public String getOperationReason() {
        return operationReason;
    }

    public void setOperationReason(String operationReason) {
        this.operationReason = operationReason;
    }

    @Basic
    @Column(name = "operation_state", nullable = true)
    public Integer getOperationState() {
        return operationState;
    }

    public void setOperationState(Integer operationState) {
        this.operationState = operationState;
    }

    @Basic
    @Column(name = "operation_date", nullable = true)
    public Timestamp getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(Timestamp operationDate) {
        this.operationDate = operationDate;
    }

    @Basic
    @Column(name = "operation_money", nullable = true, precision = 0)
    public Integer getOperationMoney() {
        return operationMoney;
    }

    public void setOperationMoney(Integer operationMoney) {
        this.operationMoney = operationMoney;
    }

    @Basic
    @Column(name = "operation_hao", nullable = true, precision = 0)
    public String getOperationHao() {
        return operationHao;
    }

    public void setOperationHao(String operationHao) {
        this.operationHao = operationHao;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OperationEntity that = (OperationEntity) o;
        return operationId == that.operationId &&
                Objects.equals(operationDocName, that.operationDocName) &&
                Objects.equals(operationHelper, that.operationHelper) &&
                Objects.equals(operationReason, that.operationReason) &&
                Objects.equals(operationState, that.operationState) &&
                Objects.equals(operationDate, that.operationDate) &&
                Objects.equals(operationMoney, that.operationMoney)&&
                Objects.equals(operationHao, that.operationHao);
    }

    @Override
    public int hashCode() {

        return Objects.hash(operationId, operationDocName, operationHelper, operationReason, operationState, operationDate, operationMoney,operationHao);
    }

    @OneToMany(mappedBy = "operationByOperationId")
    public Collection<OpRecordEntity> getOpRecordsByOperationId() {
        return opRecordsByOperationId;
    }

    public void setOpRecordsByOperationId(Collection<OpRecordEntity> opRecordsByOperationId) {
        this.opRecordsByOperationId = opRecordsByOperationId;
    }

    @ManyToOne
    @JoinColumn(name = "sq_rshenqing_id2", referencedColumnName = "sq_rshenqing_id2")
    public OpSqEntity getOpSqBySqRshenqingId2() {
        return opSqBySqRshenqingId2;
    }

    public void setOpSqBySqRshenqingId2(OpSqEntity opSqBySqRshenqingId2) {
        this.opSqBySqRshenqingId2 = opSqBySqRshenqingId2;
    }

    @ManyToOne
    @JoinColumn(name = "room_id", referencedColumnName = "room_id")
    public RoomEntity getRoomByRoomId() {
        return roomByRoomId;
    }

    public void setRoomByRoomId(RoomEntity roomByRoomId) {
        this.roomByRoomId = roomByRoomId;
    }

    @ManyToOne
    @JoinColumn(name = "rtype_id", referencedColumnName = "rtype_id")
    public RtypeEntity getRtypeByRtypeId() {
        return rtypeByRtypeId;
    }

    public void setRtypeByRtypeId(RtypeEntity rtypeByRtypeId) {
        this.rtypeByRtypeId = rtypeByRtypeId;
    }

    @ManyToOne
    @JoinColumn(name = "keshi_id", referencedColumnName = "keshi_id")
    public KeshiEntity getKeshiByKeshiId() {
        return keshiByKeshiId;
    }

    public void setKeshiByKeshiId(KeshiEntity keshiByKeshiId) {
        this.keshiByKeshiId = keshiByKeshiId;
    }
}
