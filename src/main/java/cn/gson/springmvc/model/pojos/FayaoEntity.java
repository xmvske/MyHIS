package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "fayao_", schema = "his", catalog = "")
public class FayaoEntity {
    private int fayaoId;
    private Integer fayaoSl;
    private String fayaoLx;
    private BigDecimal fayaoZje;
    private String fayaoName;
    private String fayaoLqname;
    private RepertoryEntity repertoryByRepertoryId;
    private Collection<FayaoxqEntity> fayaoxqsByFayaoId;

    @Id
    @Column(name = "fayao_id", nullable = false)
    public int getFayaoId() {
        return fayaoId;
    }

    public void setFayaoId(int fayaoId) {
        this.fayaoId = fayaoId;
    }

    @Basic
    @Column(name = "fayao_sl", nullable = true)
    public Integer getFayaoSl() {
        return fayaoSl;
    }

    public void setFayaoSl(Integer fayaoSl) {
        this.fayaoSl = fayaoSl;
    }

    @Basic
    @Column(name = "fayao_lx", nullable = true, length = 20)
    public String getFayaoLx() {
        return fayaoLx;
    }

    public void setFayaoLx(String fayaoLx) {
        this.fayaoLx = fayaoLx;
    }

    @Basic
    @Column(name = "fayao_zje", nullable = true, precision = 2)
    public BigDecimal getFayaoZje() {
        return fayaoZje;
    }

    public void setFayaoZje(BigDecimal fayaoZje) {
        this.fayaoZje = fayaoZje;
    }

    @Basic
    @Column(name = "fayao_name", nullable = true, length = 20)
    public String getFayaoName() {
        return fayaoName;
    }

    public void setFayaoName(String fayaoName) {
        this.fayaoName = fayaoName;
    }

    @Basic
    @Column(name = "fayao_lqname", nullable = true, length = 20)
    public String getFayaoLqname() {
        return fayaoLqname;
    }

    public void setFayaoLqname(String fayaoLqname) {
        this.fayaoLqname = fayaoLqname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FayaoEntity that = (FayaoEntity) o;
        return fayaoId == that.fayaoId &&
                Objects.equals(fayaoSl, that.fayaoSl) &&
                Objects.equals(fayaoLx, that.fayaoLx) &&
                Objects.equals(fayaoZje, that.fayaoZje) &&
                Objects.equals(fayaoName, that.fayaoName) &&
                Objects.equals(fayaoLqname, that.fayaoLqname);
    }

    @Override
    public int hashCode() {

        return Objects.hash(fayaoId, fayaoSl, fayaoLx, fayaoZje, fayaoName, fayaoLqname);
    }

    @ManyToOne
    @JoinColumn(name = "repertory_id", referencedColumnName = "repertory_id")
    public RepertoryEntity getRepertoryByRepertoryId() {
        return repertoryByRepertoryId;
    }

    public void setRepertoryByRepertoryId(RepertoryEntity repertoryByRepertoryId) {
        this.repertoryByRepertoryId = repertoryByRepertoryId;
    }

    @OneToMany(mappedBy = "fayaoByFayaoId")
    public Collection<FayaoxqEntity> getFayaoxqsByFayaoId() {
        return fayaoxqsByFayaoId;
    }

    public void setFayaoxqsByFayaoId(Collection<FayaoxqEntity> fayaoxqsByFayaoId) {
        this.fayaoxqsByFayaoId = fayaoxqsByFayaoId;
    }
}
