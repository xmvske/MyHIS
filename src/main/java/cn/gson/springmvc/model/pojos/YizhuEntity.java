package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "yizhu_", schema = "his", catalog = "")
public class YizhuEntity {
    private Long yizhuId;
    private String yizhuValue;
    private Integer yizhuType;
    private Date yizhuBegin;
    private Date yizhuFinish;
    private Integer yizhMumeZhu;
    private Collection<DiagnoseEntity> diagnosesByYizhuId;
    private Collection<YizhuDetailEntity> yizhuDetailsByYizhuId;
    private Collection<ChargeEntity> chargesByYizhuId;
    private MedicalCardEntity medicalCardByMedicalId;
    private UserEntity userByUserId;

    @OneToMany(mappedBy = "yizhuByYizhuId")
    public Collection<YizhuDetailEntity> getYizhuDetailsByYizhuId() {
        return yizhuDetailsByYizhuId;
    }

    public void setYizhuDetailsByYizhuId(Collection<YizhuDetailEntity> yizhuDetailsByYizhuId) {
        this.yizhuDetailsByYizhuId = yizhuDetailsByYizhuId;
    }

    @Id
    @Column(name = "yizhu_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getYizhuId() {
        return yizhuId;
    }

    public void setYizhuId(Long yizhuId) {
        this.yizhuId = yizhuId;
    }

    @Basic
    @Column(name = "yizhu_value", nullable = true, length = 100)
    public String getYizhuValue() {
        return yizhuValue;
    }

    public void setYizhuValue(String yizhuValue) {
        this.yizhuValue = yizhuValue;
    }

    @Basic
    @Column(name = "yizhu_type", nullable = true)
    public Integer getYizhuType() {
        return yizhuType;
    }

    public void setYizhuType(Integer yizhuType) {
        this.yizhuType = yizhuType;
    }

    @Basic
    @Column(name = "yizhu_begin", nullable = true)
    public Date getYizhuBegin() {
        return yizhuBegin;
    }

    public void setYizhuBegin(Date yizhuBegin) {
        this.yizhuBegin = yizhuBegin;
    }

    @Basic
    @Column(name = "yizhu_finish", nullable = true)
    public Date getYizhuFinish() {
        return yizhuFinish;
    }

    public void setYizhuFinish(Date yizhuFinish) {
        this.yizhuFinish = yizhuFinish;
    }

    @Basic
    @Column(name = "yizhu_me_zhu", nullable = true)
    public Integer getYizhMumeZhu() {
        return yizhMumeZhu;
    }

    public void setYizhMumeZhu(Integer yizhMumeZhu) {
        this.yizhMumeZhu = yizhMumeZhu;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        YizhuEntity that = (YizhuEntity) o;
        return yizhuId == that.yizhuId &&
                Objects.equals(yizhuValue, that.yizhuValue) &&
                Objects.equals(yizhuType, that.yizhuType) &&
                Objects.equals(yizhuBegin, that.yizhuBegin) &&
                Objects.equals(yizhuFinish, that.yizhuFinish) &&
                Objects.equals(yizhuBegin, that.yizhMumeZhu);
    }

    @Override
    public int hashCode() {

        return Objects.hash(yizhuId, yizhuValue, yizhuType, yizhuBegin, yizhuFinish,yizhMumeZhu);
    }

    @OneToMany(mappedBy = "yizhuByYizhuId")
    public Collection<DiagnoseEntity> getDiagnosesByYizhuId() {
        return diagnosesByYizhuId;
    }

    public void setDiagnosesByYizhuId(Collection<DiagnoseEntity> diagnosesByYizhuId) {
        this.diagnosesByYizhuId = diagnosesByYizhuId;
    }

    @OneToMany(mappedBy = "yizhuByYizhuId")
    public Collection<ChargeEntity> getChargesByYizhuId() {
        return chargesByYizhuId;
    }

    public void setChargesByYizhuId(Collection<ChargeEntity> chargesByYizhuId) {
        this.chargesByYizhuId = chargesByYizhuId;
    }

    @ManyToOne
    @JoinColumn(name = "medical_id", referencedColumnName = "medical_id")
    public MedicalCardEntity getMedicalCardByMedicalId() {
        return medicalCardByMedicalId;
    }

    public void setMedicalCardByMedicalId(MedicalCardEntity medicalCardByMedicalId) {
        this.medicalCardByMedicalId = medicalCardByMedicalId;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getUserByUserId() {
        return userByUserId;
    }

    public void setUserByUserId(UserEntity userByUserId) {
        this.userByUserId = userByUserId;
    }
}
