package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "repertory_", schema = "his", catalog = "")
public class RepertoryEntity {
    private int repertoryId;
    private Integer repertorySl;
    private Integer repertoryShankc;
    private Collection<DrugapplyEntity> drugappliesByRepertoryId;
    private Collection<FayaoEntity> fayaosByRepertoryId;
    private YaopinEntity yaopinByYaopinId;
    private TuiyaoxqEntity tuiyaoxqByTuiyaoxqId;
    private YaofanpdEntity yaofanpdByYaofanpdId;

    @Id
    @Column(name = "repertory_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getRepertoryId() {
        return repertoryId;
    }

    public void setRepertoryId(int repertoryId) {
        this.repertoryId = repertoryId;
    }

    @Basic
    @Column(name = "repertory_sl", nullable = true)
    public Integer getRepertorySl() {
        return repertorySl;
    }

    public void setRepertorySl(Integer repertorySl) {
        this.repertorySl = repertorySl;
    }

    @Basic
    @Column(name = "repertory_shankc", nullable = true)
    public Integer getRepertoryShankc() {
        return repertoryShankc;
    }

    public void setRepertoryShankc(Integer repertoryShankc) {
        this.repertoryShankc = repertoryShankc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RepertoryEntity that = (RepertoryEntity) o;
        return repertoryId == that.repertoryId &&
                Objects.equals(repertorySl, that.repertorySl) &&
                Objects.equals(repertoryShankc, that.repertoryShankc);
    }

    @Override
    public int hashCode() {

        return Objects.hash(repertoryId, repertorySl, repertoryShankc);
    }

    @OneToMany(mappedBy = "repertoryByRepertoryId")
    public Collection<DrugapplyEntity> getDrugappliesByRepertoryId() {
        return drugappliesByRepertoryId;
    }

    public void setDrugappliesByRepertoryId(Collection<DrugapplyEntity> drugappliesByRepertoryId) {
        this.drugappliesByRepertoryId = drugappliesByRepertoryId;
    }

    @OneToMany(mappedBy = "repertoryByRepertoryId")
    public Collection<FayaoEntity> getFayaosByRepertoryId() {
        return fayaosByRepertoryId;
    }

    public void setFayaosByRepertoryId(Collection<FayaoEntity> fayaosByRepertoryId) {
        this.fayaosByRepertoryId = fayaosByRepertoryId;
    }

    @ManyToOne
    @JoinColumn(name = "yaopin_id", referencedColumnName = "yaopin_id")
    public YaopinEntity getYaopinByYaopinId() {
        return yaopinByYaopinId;
    }

    public void setYaopinByYaopinId(YaopinEntity yaopinByYaopinId) {
        this.yaopinByYaopinId = yaopinByYaopinId;
    }

    @ManyToOne
    @JoinColumn(name = "tuiyaoxq_id", referencedColumnName = "tuiyaoxq_id")
    public TuiyaoxqEntity getTuiyaoxqByTuiyaoxqId() {
        return tuiyaoxqByTuiyaoxqId;
    }

    public void setTuiyaoxqByTuiyaoxqId(TuiyaoxqEntity tuiyaoxqByTuiyaoxqId) {
        this.tuiyaoxqByTuiyaoxqId = tuiyaoxqByTuiyaoxqId;
    }

    @ManyToOne
    @JoinColumn(name = "yaofanpd_id", referencedColumnName = "yaofanpd_id")
    public YaofanpdEntity getYaofanpdByYaofanpdId() {
        return yaofanpdByYaofanpdId;
    }

    public void setYaofanpdByYaofanpdId(YaofanpdEntity yaofanpdByYaofanpdId) {
        this.yaofanpdByYaofanpdId = yaofanpdByYaofanpdId;
    }
}
