package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "diagnose_", schema = "his", catalog = "")
public class DiagnoseEntity {
    private int diagnoseId;
    private Timestamp diagnoseDate;
    private Integer diagnoseType;
    private String diagnoseResult;
    private RegistrationEntity registrationByRegistrationId;
    private HealthEntity healthByHealthId;
    private YizhuEntity yizhuByYizhuId;
    private KeshiEntity keshiByKeshiId;
    private MedicalRecordEntity medicalRecordByMedicalRecordId;
    private MedicalCardEntity medicalCardByMedicalId;
    private ChargeEntity chargeByChargeId;

    @Id
    @Column(name = "diagnose_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getDiagnoseId() {
        return diagnoseId;
    }

    public void setDiagnoseId(int diagnoseId) {
        this.diagnoseId = diagnoseId;
    }

    @Basic
    @Column(name = "diagnose_date", nullable = true)
    public Timestamp getDiagnoseDate() {
        return diagnoseDate;
    }

    public void setDiagnoseDate(Timestamp diagnoseDate) {
        this.diagnoseDate = diagnoseDate;
    }

    @Basic
    @Column(name = "diagnose_type", nullable = true)
    public Integer getDiagnoseType() {
        return diagnoseType;
    }

    public void setDiagnoseType(Integer diagnoseType) {
        this.diagnoseType = diagnoseType;
    }

    @Basic
    @Column(name = "diagnose_result", nullable = true, length = 200)
    public String getDiagnoseResult() {
        return diagnoseResult;
    }

    public void setDiagnoseResult(String diagnoseResult) {
        this.diagnoseResult = diagnoseResult;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DiagnoseEntity that = (DiagnoseEntity) o;
        return diagnoseId == that.diagnoseId &&
                Objects.equals(diagnoseDate, that.diagnoseDate) &&
                Objects.equals(diagnoseType, that.diagnoseType) &&
                Objects.equals(diagnoseResult, that.diagnoseResult);
    }

    @Override
    public int hashCode() {

        return Objects.hash(diagnoseId, diagnoseDate, diagnoseType, diagnoseResult);
    }

    @ManyToOne
    @JoinColumn(name = "registration_id", referencedColumnName = "registration_id")
    public RegistrationEntity getRegistrationByRegistrationId() {
        return registrationByRegistrationId;
    }

    public void setRegistrationByRegistrationId(RegistrationEntity registrationByRegistrationId) {
        this.registrationByRegistrationId = registrationByRegistrationId;
    }

    @ManyToOne
    @JoinColumn(name = "health_id", referencedColumnName = "health_id")
    public HealthEntity getHealthByHealthId() {
        return healthByHealthId;
    }

    public void setHealthByHealthId(HealthEntity healthByHealthId) {
        this.healthByHealthId = healthByHealthId;
    }

    @ManyToOne
    @JoinColumn(name = "yizhu_id", referencedColumnName = "yizhu_id")
    public YizhuEntity getYizhuByYizhuId() {
        return yizhuByYizhuId;
    }

    public void setYizhuByYizhuId(YizhuEntity yizhuByYizhuId) {
        this.yizhuByYizhuId = yizhuByYizhuId;
    }

    @ManyToOne
    @JoinColumn(name = "keshi_id", referencedColumnName = "keshi_id")
    public KeshiEntity getKeshiByKeshiId() {
        return keshiByKeshiId;
    }

    public void setKeshiByKeshiId(KeshiEntity keshiByKeshiId) {
        this.keshiByKeshiId = keshiByKeshiId;
    }

    @ManyToOne
    @JoinColumn(name = "medical_record_id", referencedColumnName = "medical_record_id")
    public MedicalRecordEntity getMedicalRecordByMedicalRecordId() {
        return medicalRecordByMedicalRecordId;
    }

    public void setMedicalRecordByMedicalRecordId(MedicalRecordEntity medicalRecordByMedicalRecordId) {
        this.medicalRecordByMedicalRecordId = medicalRecordByMedicalRecordId;
    }

    @ManyToOne
    @JoinColumn(name = "medical_id", referencedColumnName = "medical_id")
    public MedicalCardEntity getMedicalCardByMedicalId() {
        return medicalCardByMedicalId;
    }

    public void setMedicalCardByMedicalId(MedicalCardEntity medicalCardByMedicalId) {
        this.medicalCardByMedicalId = medicalCardByMedicalId;
    }

    @ManyToOne
    @JoinColumn(name = "charge_id", referencedColumnName = "charge_id")
    public ChargeEntity getChargeByChargeId() {
        return chargeByChargeId;
    }

    public void setChargeByChargeId(ChargeEntity chargeByChargeId) {
        this.chargeByChargeId = chargeByChargeId;
    }
}
