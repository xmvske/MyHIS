package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "thxq_", schema = "his", catalog = "")
public class ThxqEntity {
    private int thxqId;
    private Integer thxqSl;
    private YaopinEntity yaopinByYaopinId;
    private TuihuoEntity tuihuoByTuihuoId;

    @Id
    @Column(name = "thxq_id", nullable = false)
    public int getThxqId() {
        return thxqId;
    }

    public void setThxqId(int thxqId) {
        this.thxqId = thxqId;
    }

    @Basic
    @Column(name = "thxq_sl", nullable = true)
    public Integer getThxqSl() {
        return thxqSl;
    }

    public void setThxqSl(Integer thxqSl) {
        this.thxqSl = thxqSl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ThxqEntity that = (ThxqEntity) o;
        return thxqId == that.thxqId &&
                Objects.equals(thxqSl, that.thxqSl);
    }

    @Override
    public int hashCode() {

        return Objects.hash(thxqId, thxqSl);
    }

    @ManyToOne
    @JoinColumn(name = "yaopin_id", referencedColumnName = "yaopin_id")
    public YaopinEntity getYaopinByYaopinId() {
        return yaopinByYaopinId;
    }

    public void setYaopinByYaopinId(YaopinEntity yaopinByYaopinId) {
        this.yaopinByYaopinId = yaopinByYaopinId;
    }

    @ManyToOne
    @JoinColumn(name = "tuihuo_id", referencedColumnName = "tuihuo_id")
    public TuihuoEntity getTuihuoByTuihuoId() {
        return tuihuoByTuihuoId;
    }

    public void setTuihuoByTuihuoId(TuihuoEntity tuihuoByTuihuoId) {
        this.tuihuoByTuihuoId = tuihuoByTuihuoId;
    }
}
