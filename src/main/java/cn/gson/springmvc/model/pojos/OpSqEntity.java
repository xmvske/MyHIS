package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "op_sq", schema = "his", catalog = "")
public class OpSqEntity {
    private Long sqRshenqingId2;
    private Date sqDate;
    private Integer sqState;
    private MedicalCardEntity medicalCardByMedicalId;
    private Collection<OperationEntity> operationsBySqRshenqingId2;

    @Id
    @Column(name = "sq_rshenqing_id2", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    public Long getSqRshenqingId2() {
        return sqRshenqingId2;
    }

    public void setSqRshenqingId2(Long sqRshenqingId2) {
        this.sqRshenqingId2 = sqRshenqingId2;
    }

    @Basic
    @Column(name = "sq_date", nullable = true)
    public Date getSqDate() {
        return sqDate;
    }

    public void setSqDate(Date sqDate) {
        this.sqDate = sqDate;
    }

    @Basic
    @Column(name = "sq_state", nullable = true)
    public Integer getSqState() {
        return sqState;
    }

    public void setSqState(Integer sqState) {
        this.sqState = sqState;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OpSqEntity that = (OpSqEntity) o;
        return sqRshenqingId2 == that.sqRshenqingId2 &&
                Objects.equals(sqDate, that.sqDate) &&
                Objects.equals(sqState, that.sqState);
    }

    @Override
    public int hashCode() {

        return Objects.hash(sqRshenqingId2, sqDate, sqState);
    }

    @ManyToOne
    @JoinColumn(name = "medical_id", referencedColumnName = "medical_id")
    public MedicalCardEntity getMedicalCardByMedicalId() {
        return medicalCardByMedicalId;
    }

    public void setMedicalCardByMedicalId(MedicalCardEntity medicalCardByMedicalId) {
        this.medicalCardByMedicalId = medicalCardByMedicalId;
    }

    @OneToMany(mappedBy = "opSqBySqRshenqingId2")
    public Collection<OperationEntity> getOperationsBySqRshenqingId2() {
        return operationsBySqRshenqingId2;
    }

    public void setOperationsBySqRshenqingId2(Collection<OperationEntity> operationsBySqRshenqingId2) {
        this.operationsBySqRshenqingId2 = operationsBySqRshenqingId2;
    }
}
