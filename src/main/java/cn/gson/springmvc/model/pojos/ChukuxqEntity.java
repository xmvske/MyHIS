package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "chukuxq_", schema = "his", catalog = "")
public class ChukuxqEntity {
    private int chukuxqId;
    private Timestamp chukuxqTime;
    private ChukuEntity chukuByChukuId;
    private OutpatientEntity outpatientByOutpatientId;

    @Id
    @Column(name = "chukuxq_id", nullable = false)
    public int getChukuxqId() {
        return chukuxqId;
    }

    public void setChukuxqId(int chukuxqId) {
        this.chukuxqId = chukuxqId;
    }

    @Basic
    @Column(name = "chukuxq_time", nullable = true)
    public Timestamp getChukuxqTime() {
        return chukuxqTime;
    }

    public void setChukuxqTime(Timestamp chukuxqTime) {
        this.chukuxqTime = chukuxqTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChukuxqEntity that = (ChukuxqEntity) o;
        return chukuxqId == that.chukuxqId &&
                Objects.equals(chukuxqTime, that.chukuxqTime);
    }

    @Override
    public int hashCode() {

        return Objects.hash(chukuxqId, chukuxqTime);
    }

    @ManyToOne
    @JoinColumn(name = "chuku_id", referencedColumnName = "chuku_id")
    public ChukuEntity getChukuByChukuId() {
        return chukuByChukuId;
    }

    public void setChukuByChukuId(ChukuEntity chukuByChukuId) {
        this.chukuByChukuId = chukuByChukuId;
    }

    @ManyToOne
    @JoinColumn(name = "outpatient_id", referencedColumnName = "outpatient_id")
    public OutpatientEntity getOutpatientByOutpatientId() {
        return outpatientByOutpatientId;
    }

    public void setOutpatientByOutpatientId(OutpatientEntity outpatientByOutpatientId) {
        this.outpatientByOutpatientId = outpatientByOutpatientId;
    }
}
