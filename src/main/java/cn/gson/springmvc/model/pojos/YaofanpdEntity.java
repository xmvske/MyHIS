package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "yaofanpd_", schema = "his", catalog = "")
public class YaofanpdEntity {
    private int yaofanpdId;
    private Timestamp yaofanpdData;
    private String yaofanpdFzr;
    private BigDecimal yaofanpdZje;
    private Integer yaofanpdZsl;
    private Collection<RepertoryEntity> repertoriesByYaofanpdId;

    @Id
    @Column(name = "yaofanpd_id", nullable = false)
    public int getYaofanpdId() {
        return yaofanpdId;
    }

    public void setYaofanpdId(int yaofanpdId) {
        this.yaofanpdId = yaofanpdId;
    }

    @Basic
    @Column(name = "yaofanpd_data", nullable = true)
    public Timestamp getYaofanpdData() {
        return yaofanpdData;
    }

    public void setYaofanpdData(Timestamp yaofanpdData) {
        this.yaofanpdData = yaofanpdData;
    }

    @Basic
    @Column(name = "yaofanpd_fzr", nullable = true, length = 20)
    public String getYaofanpdFzr() {
        return yaofanpdFzr;
    }

    public void setYaofanpdFzr(String yaofanpdFzr) {
        this.yaofanpdFzr = yaofanpdFzr;
    }

    @Basic
    @Column(name = "yaofanpd_zje", nullable = true, precision = 2)
    public BigDecimal getYaofanpdZje() {
        return yaofanpdZje;
    }

    public void setYaofanpdZje(BigDecimal yaofanpdZje) {
        this.yaofanpdZje = yaofanpdZje;
    }

    @Basic
    @Column(name = "yaofanpd_zsl", nullable = true)
    public Integer getYaofanpdZsl() {
        return yaofanpdZsl;
    }

    public void setYaofanpdZsl(Integer yaofanpdZsl) {
        this.yaofanpdZsl = yaofanpdZsl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        YaofanpdEntity that = (YaofanpdEntity) o;
        return yaofanpdId == that.yaofanpdId &&
                Objects.equals(yaofanpdData, that.yaofanpdData) &&
                Objects.equals(yaofanpdFzr, that.yaofanpdFzr) &&
                Objects.equals(yaofanpdZje, that.yaofanpdZje) &&
                Objects.equals(yaofanpdZsl, that.yaofanpdZsl);
    }

    @Override
    public int hashCode() {

        return Objects.hash(yaofanpdId, yaofanpdData, yaofanpdFzr, yaofanpdZje, yaofanpdZsl);
    }

    @OneToMany(mappedBy = "yaofanpdByYaofanpdId")
    public Collection<RepertoryEntity> getRepertoriesByYaofanpdId() {
        return repertoriesByYaofanpdId;
    }

    public void setRepertoriesByYaofanpdId(Collection<RepertoryEntity> repertoriesByYaofanpdId) {
        this.repertoriesByYaofanpdId = repertoriesByYaofanpdId;
    }
}
