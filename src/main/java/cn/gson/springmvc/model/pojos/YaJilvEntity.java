package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "ya_jilv", schema = "his", catalog = "")
public class YaJilvEntity {
    private Long yaId;
    private Integer yaTotal;
    private Integer yaYu;
    private Integer yaXu;
    private Date yaDate;
    private HospEntity hospByHospId;

    @Id
    @Column(name = "ya_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getYaId() {
        return yaId;
    }

    public void setYaId(Long yaId) {
        this.yaId = yaId;
    }

    @Basic
    @Column(name = "ya_total", nullable = true, precision = 0)
    public Integer getYaTotal() {
        return yaTotal;
    }

    public void setYaTotal(Integer yaTotal) {
        this.yaTotal = yaTotal;
    }

    @Basic
    @Column(name = "ya_yu", nullable = true, precision = 0)
    public Integer getYaYu() {
        return yaYu;
    }

    public void setYaYu(Integer yaYu) {
        this.yaYu = yaYu;
    }

    @Basic
    @Column(name = "ya_xu", nullable = true, precision = 0)
    public Integer getYaXu() {
        return yaXu;
    }

    public void setYaXu(Integer yaXu) {
        this.yaXu = yaXu;
    }

    @Basic
    @Column(name = "ya_date", nullable = true)
    public Date getYaDate() {
        return yaDate;
    }

    public void setYaDate(Date yaDate) {
        this.yaDate = yaDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        YaJilvEntity that = (YaJilvEntity) o;
        return yaId == that.yaId &&
                Objects.equals(yaTotal, that.yaTotal) &&
                Objects.equals(yaYu, that.yaYu) &&
                Objects.equals(yaXu, that.yaXu) &&
                Objects.equals(yaDate, that.yaDate);
    }

    @Override
    public int hashCode() {

        return Objects.hash(yaId, yaTotal, yaYu, yaXu, yaDate);
    }

    @ManyToOne
    @JoinColumn(name = "hosp_id", referencedColumnName = "hosp_id")
    public HospEntity getHospByHospId() {
        return hospByHospId;
    }

    public void setHospByHospId(HospEntity hospByHospId) {
        this.hospByHospId = hospByHospId;
    }
}
