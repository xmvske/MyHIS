package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "fayaoxq_", schema = "his", catalog = "")
public class FayaoxqEntity {
    private int fayaoxqId;
    private Timestamp fayaoxqData;
    private Integer fayaoxqSl;
    private FayaoEntity fayaoByFayaoId;
    private YizhuDetailEntity yizhuDetailByYizhuDetailId;

    @Id
    @Column(name = "fayaoxq_id", nullable = false)
    public int getFayaoxqId() {
        return fayaoxqId;
    }

    public void setFayaoxqId(int fayaoxqId) {
        this.fayaoxqId = fayaoxqId;
    }

    @Basic
    @Column(name = "fayaoxq_data", nullable = true)
    public Timestamp getFayaoxqData() {
        return fayaoxqData;
    }

    public void setFayaoxqData(Timestamp fayaoxqData) {
        this.fayaoxqData = fayaoxqData;
    }

    @Basic
    @Column(name = "fayaoxq_sl", nullable = true)
    public Integer getFayaoxqSl() {
        return fayaoxqSl;
    }

    public void setFayaoxqSl(Integer fayaoxqSl) {
        this.fayaoxqSl = fayaoxqSl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FayaoxqEntity that = (FayaoxqEntity) o;
        return fayaoxqId == that.fayaoxqId &&
                Objects.equals(fayaoxqData, that.fayaoxqData) &&
                Objects.equals(fayaoxqSl, that.fayaoxqSl);
    }

    @Override
    public int hashCode() {

        return Objects.hash(fayaoxqId, fayaoxqData, fayaoxqSl);
    }

    @ManyToOne
    @JoinColumn(name = "fayao_id", referencedColumnName = "fayao_id")
    public FayaoEntity getFayaoByFayaoId() {
        return fayaoByFayaoId;
    }

    public void setFayaoByFayaoId(FayaoEntity fayaoByFayaoId) {
        this.fayaoByFayaoId = fayaoByFayaoId;
    }

    @ManyToOne
    @JoinColumn(name = "yizhu_detail_id", referencedColumnName = "yizhu_detail_id")
    public YizhuDetailEntity getYizhuDetailByYizhuDetailId() {
        return yizhuDetailByYizhuDetailId;
    }

    public void setYizhuDetailByYizhuDetailId(YizhuDetailEntity yizhuDetailByYizhuDetailId) {
        this.yizhuDetailByYizhuDetailId = yizhuDetailByYizhuDetailId;
    }
}
