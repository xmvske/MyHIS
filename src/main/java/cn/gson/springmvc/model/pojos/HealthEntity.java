package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.sql.Date;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "health_", schema = "his", catalog = "")
public class HealthEntity {
    private int healthId;
    private Date healthDate;
    private Integer healthMoneyState;
    private Collection<DiagnoseEntity> diagnosesByHealthId;
    private MedicalCardEntity medicalCardByMedicalId;
    private Collection<HealthDetailEntity> healthDetailsByHealthId;
    private Collection<ChargeEntity> chargesByHealthId;

    @Id
    @Column(name = "health_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getHealthId() {
        return healthId;
    }

    public void setHealthId(int healthId) {
        this.healthId = healthId;
    }

    @Basic
    @Column(name = "health_date", nullable = true)
    public Date getHealthDate() {
        return healthDate;
    }

    public void setHealthDate(Date healthDate) {
        this.healthDate = healthDate;
    }

    @Basic
    @Column(name = "health_money_state", nullable = true)
    public Integer getHealthMoneyState() {
        return healthMoneyState;
    }

    public void setHealthMoneyState(Integer healthMoneyState) {
        this.healthMoneyState = healthMoneyState;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HealthEntity that = (HealthEntity) o;
        return healthId == that.healthId &&
                Objects.equals(healthDate, that.healthDate) &&
                Objects.equals(healthMoneyState, that.healthMoneyState);
    }

    @Override
    public int hashCode() {

        return Objects.hash(healthId, healthDate, healthMoneyState);
    }

    @OneToMany(mappedBy = "healthByHealthId")
    public Collection<DiagnoseEntity> getDiagnosesByHealthId() {
        return diagnosesByHealthId;
    }

    public void setDiagnosesByHealthId(Collection<DiagnoseEntity> diagnosesByHealthId) {
        this.diagnosesByHealthId = diagnosesByHealthId;
    }

    @ManyToOne
    @JoinColumn(name = "medical_id", referencedColumnName = "medical_id")
    public MedicalCardEntity getMedicalCardByMedicalId() {
        return medicalCardByMedicalId;
    }

    public void setMedicalCardByMedicalId(MedicalCardEntity medicalCardByMedicalId) {
        this.medicalCardByMedicalId = medicalCardByMedicalId;
    }

    @OneToMany(mappedBy = "healthByHealthId")
    public Collection<HealthDetailEntity> getHealthDetailsByHealthId() {
        return healthDetailsByHealthId;
    }

    public void setHealthDetailsByHealthId(Collection<HealthDetailEntity> healthDetailsByHealthId) {
        this.healthDetailsByHealthId = healthDetailsByHealthId;
    }

    @OneToMany(mappedBy = "healthByHealthId")
    public Collection<ChargeEntity> getChargesByHealthId() {
        return chargesByHealthId;
    }

    public void setChargesByHealthId(Collection<ChargeEntity> chargesByHealthId) {
        this.chargesByHealthId = chargesByHealthId;
    }
}
