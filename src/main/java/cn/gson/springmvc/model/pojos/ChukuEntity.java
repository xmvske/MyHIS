package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "chuku_", schema = "his", catalog = "")
public class ChukuEntity {
    private int chukuId;
    private Timestamp chukuSj;
    private Integer chukuSl;
    private Collection<ChukuxqEntity> chukuxqsByChukuId;

    @Id
    @Column(name = "chuku_id", nullable = false)
    public int getChukuId() {
        return chukuId;
    }

    public void setChukuId(int chukuId) {
        this.chukuId = chukuId;
    }

    @Basic
    @Column(name = "chuku_sj", nullable = true)
    public Timestamp getChukuSj() {
        return chukuSj;
    }

    public void setChukuSj(Timestamp chukuSj) {
        this.chukuSj = chukuSj;
    }

    @Basic
    @Column(name = "chuku_sl", nullable = true)
    public Integer getChukuSl() {
        return chukuSl;
    }

    public void setChukuSl(Integer chukuSl) {
        this.chukuSl = chukuSl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChukuEntity that = (ChukuEntity) o;
        return chukuId == that.chukuId &&
                Objects.equals(chukuSj, that.chukuSj) &&
                Objects.equals(chukuSl, that.chukuSl);
    }

    @Override
    public int hashCode() {

        return Objects.hash(chukuId, chukuSj, chukuSl);
    }

    @OneToMany(mappedBy = "chukuByChukuId")
    public Collection<ChukuxqEntity> getChukuxqsByChukuId() {
        return chukuxqsByChukuId;
    }

    public void setChukuxqsByChukuId(Collection<ChukuxqEntity> chukuxqsByChukuId) {
        this.chukuxqsByChukuId = chukuxqsByChukuId;
    }
}
