package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "user_detail_", schema = "his", catalog = "")
public class UserDetailEntity {
    private int userDetailId;
    private String userDetailSex;
    private String userDetailCard;
    private String userDetailAdd;
    private String userDetailPhone;
    private UserEntity userByUserId;

    @Id
    @Column(name = "user_detail_id", nullable = false)
    public int getUserDetailId() {
        return userDetailId;
    }

    public void setUserDetailId(int userDetailId) {
        this.userDetailId = userDetailId;
    }

    @Basic
    @Column(name = "user_detail_sex", nullable = true, length = 3)
    public String getUserDetailSex() {
        return userDetailSex;
    }

    public void setUserDetailSex(String userDetailSex) {
        this.userDetailSex = userDetailSex;
    }

    @Basic
    @Column(name = "user_detail_card", nullable = true, length = 20)
    public String getUserDetailCard() {
        return userDetailCard;
    }

    public void setUserDetailCard(String userDetailCard) {
        this.userDetailCard = userDetailCard;
    }

    @Basic
    @Column(name = "user_detail_add", nullable = true, length = 250)
    public String getUserDetailAdd() {
        return userDetailAdd;
    }

    public void setUserDetailAdd(String userDetailAdd) {
        this.userDetailAdd = userDetailAdd;
    }

    @Basic
    @Column(name = "user_detail_phone", nullable = true, length = 20)
    public String getUserDetailPhone() {
        return userDetailPhone;
    }

    public void setUserDetailPhone(String userDetailPhone) {
        this.userDetailPhone = userDetailPhone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserDetailEntity that = (UserDetailEntity) o;
        return userDetailId == that.userDetailId &&
                Objects.equals(userDetailSex, that.userDetailSex) &&
                Objects.equals(userDetailCard, that.userDetailCard) &&
                Objects.equals(userDetailAdd, that.userDetailAdd) &&
                Objects.equals(userDetailPhone, that.userDetailPhone);
    }

    @Override
    public int hashCode() {

        return Objects.hash(userDetailId, userDetailSex, userDetailCard, userDetailAdd, userDetailPhone);
    }

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    public UserEntity getUserByUserId() {
        return userByUserId;
    }

    public void setUserByUserId(UserEntity userByUserId) {
        this.userByUserId = userByUserId;
    }
}
