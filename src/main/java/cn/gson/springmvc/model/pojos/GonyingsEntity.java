package cn.gson.springmvc.model.pojos;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "gonyings_", schema = "his", catalog = "")
public class GonyingsEntity {
    private Long gonyingsId;
    private String gonyingsName;
    private String gonyingsDz;
    private Timestamp gonyingsTime;
    private String gonyingsLxr;
    private Integer gonyingsLxdh;
    private TuihuoEntity tuihuoByTuihuoId;
    private Collection<PurchasingEntity> purchasingByPurchasingId;
    private Collection<YaopinEntity> yaopinsByGonyingsId;

    @Id
    @Column(name = "gonyings_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getGonyingsId() {
        return gonyingsId;
    }

    public void setGonyingsId(Long gonyingsId) {
        this.gonyingsId = gonyingsId;
    }

    @Basic
    @Column(name = "gonyings_name", nullable = true, length = 20)
    public String getGonyingsName() {
        return gonyingsName;
    }

    public void setGonyingsName(String gonyingsName) {
        this.gonyingsName = gonyingsName;
    }

    @Basic
    @Column(name = "gonyings_dz", nullable = true, length = 100)
    public String getGonyingsDz() {
        return gonyingsDz;
    }

    public void setGonyingsDz(String gonyingsDz) {
        this.gonyingsDz = gonyingsDz;
    }

    @Basic
    @Column(name = "gonyings_time", nullable = true)
    public Timestamp getGonyingsTime() {
        return gonyingsTime;
    }

    public void setGonyingsTime(Timestamp gonyingsTime) {
        this.gonyingsTime = gonyingsTime;
    }

    @Basic
    @Column(name = "gonyings_lxr", nullable = true, length = 20)
    public String getGonyingsLxr() {
        return gonyingsLxr;
    }

    public void setGonyingsLxr(String gonyingsLxr) {
        this.gonyingsLxr = gonyingsLxr;
    }

    @Basic
    @Column(name = "gonyings_lxdh", nullable = true)
    public Integer getGonyingsLxdh() {
        return gonyingsLxdh;
    }

    public void setGonyingsLxdh(Integer gonyingsLxdh) {
        this.gonyingsLxdh = gonyingsLxdh;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GonyingsEntity that = (GonyingsEntity) o;
        return gonyingsId == that.gonyingsId &&
                Objects.equals(gonyingsName, that.gonyingsName) &&
                Objects.equals(gonyingsDz, that.gonyingsDz) &&
                Objects.equals(gonyingsTime, that.gonyingsTime) &&
                Objects.equals(gonyingsLxr, that.gonyingsLxr) &&
                Objects.equals(gonyingsLxdh, that.gonyingsLxdh);
    }

    @Override
    public int hashCode() {

        return Objects.hash(gonyingsId, gonyingsName, gonyingsDz, gonyingsTime, gonyingsLxr, gonyingsLxdh);
    }

    @ManyToOne
    @JoinColumn(name = "tuihuo_id", referencedColumnName = "tuihuo_id")
    public TuihuoEntity getTuihuoByTuihuoId() {
        return tuihuoByTuihuoId;
    }

    public void setTuihuoByTuihuoId(TuihuoEntity tuihuoByTuihuoId) {
        this.tuihuoByTuihuoId = tuihuoByTuihuoId;
    }

   @OneToMany(mappedBy = "gonyingsByPurchasingId")
    public Collection<PurchasingEntity> getPurchasingByPurchasingId() {
        return purchasingByPurchasingId;
    }

    public void setPurchasingByPurchasingId(Collection<PurchasingEntity> purchasingByPurchasingId) {
        this.purchasingByPurchasingId = purchasingByPurchasingId;
    }

    @OneToMany(mappedBy = "gonyingsByGonyingsId")
    public Collection<YaopinEntity> getYaopinsByGonyingsId() {
        return yaopinsByGonyingsId;
    }

    public void setYaopinsByGonyingsId(Collection<YaopinEntity> yaopinsByGonyingsId) {
        this.yaopinsByGonyingsId = yaopinsByGonyingsId;
    }
}
