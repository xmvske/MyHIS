package cn.gson.springmvc.model.dao;


import cn.gson.springmvc.model.pojos.MedicalCardRecordEntity;
import org.springframework.data.repository.CrudRepository;

public interface MedicalCardRecordDao extends CrudRepository<MedicalCardRecordEntity,Long> {

}
