package cn.gson.springmvc.model.dao;

import cn.gson.springmvc.model.pojos.InventoryDetailsEntity;
import org.springframework.data.repository.CrudRepository;

public interface InventoryDetailsDaoZx extends CrudRepository<InventoryDetailsEntity,Long> {
}
