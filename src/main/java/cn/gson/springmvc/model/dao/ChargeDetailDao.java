package cn.gson.springmvc.model.dao;

import cn.gson.springmvc.model.pojos.ChargeDetailEntity;
import org.springframework.data.repository.CrudRepository;

public interface ChargeDetailDao extends CrudRepository<ChargeDetailEntity,Long> {
}
