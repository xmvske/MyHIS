package cn.gson.springmvc.model.dao;

import cn.gson.springmvc.model.pojos.UserEntity;
import org.springframework.data.repository.CrudRepository;

public interface UserDao extends CrudRepository<UserEntity,Long> {
}
