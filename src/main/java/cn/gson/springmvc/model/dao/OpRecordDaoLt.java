package cn.gson.springmvc.model.dao;


import cn.gson.springmvc.model.pojos.OpRecordEntity;
import org.springframework.data.repository.CrudRepository;

public interface OpRecordDaoLt extends CrudRepository<OpRecordEntity,Long> {
}
