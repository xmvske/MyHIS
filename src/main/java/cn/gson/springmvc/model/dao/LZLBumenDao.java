package cn.gson.springmvc.model.dao;

import cn.gson.springmvc.model.pojos.BumenEntity;
import org.springframework.data.repository.CrudRepository;


public interface LZLBumenDao extends CrudRepository<BumenEntity,Long> {

}
