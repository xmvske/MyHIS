package cn.gson.springmvc.model.dao;

import cn.gson.springmvc.model.pojos.ZhuyuanApplyEntity;
import org.springframework.data.repository.CrudRepository;

public interface ZhuyuanApplyDaoLt extends CrudRepository<ZhuyuanApplyEntity,Long> {
}
