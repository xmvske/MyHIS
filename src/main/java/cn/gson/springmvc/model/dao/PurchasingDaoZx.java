package cn.gson.springmvc.model.dao;

import cn.gson.springmvc.model.pojos.PurchasingEntity;
import org.springframework.data.repository.CrudRepository;

public interface PurchasingDaoZx extends CrudRepository<PurchasingEntity,Long> {
}
