package cn.gson.springmvc.model.dao;

import cn.gson.springmvc.model.pojos.HealthMealEntity;
import org.springframework.data.repository.CrudRepository;

public interface HealthMealDao extends CrudRepository<HealthMealEntity,Long> {
}
