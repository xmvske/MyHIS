package cn.gson.springmvc.model.dao;

import cn.gson.springmvc.model.pojos.RoleEntity;
import org.springframework.data.repository.CrudRepository;


public interface LZLRoleDao extends CrudRepository<RoleEntity,Long> {

}
