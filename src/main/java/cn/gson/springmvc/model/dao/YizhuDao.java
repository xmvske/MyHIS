package cn.gson.springmvc.model.dao;

import cn.gson.springmvc.model.pojos.YizhuEntity;
import org.springframework.data.repository.CrudRepository;

public interface YizhuDao extends CrudRepository<YizhuEntity,Long> {
}
