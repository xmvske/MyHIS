package cn.gson.springmvc.model.dao;


import cn.gson.springmvc.model.pojos.OpSqEntity;
import org.springframework.data.repository.CrudRepository;

public interface OpSqDaoLt extends CrudRepository<OpSqEntity,Long> {
}
