package cn.gson.springmvc.model.dao;

import cn.gson.springmvc.model.pojos.MedicalCardEntity;
import org.springframework.data.repository.CrudRepository;

public interface MedicalCardDao extends CrudRepository<MedicalCardEntity,Long> {

}
