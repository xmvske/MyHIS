package cn.gson.springmvc.model.dao;

import cn.gson.springmvc.model.pojos.DrugapplyTableEntity;
import org.springframework.data.repository.CrudRepository;

public interface DrugapplyTableDaoZx extends CrudRepository<DrugapplyTableEntity,Long> {

}
