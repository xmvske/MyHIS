package cn.gson.springmvc.model.dao;


import cn.gson.springmvc.model.pojos.OpSqEntity;
import cn.gson.springmvc.model.pojos.YaJilvEntity;
import org.springframework.data.repository.CrudRepository;

public interface YaJilvDaoLt extends CrudRepository<YaJilvEntity,Long> {
}
