package cn.gson.springmvc.model.dao;

import cn.gson.springmvc.model.pojos.OutpatientEntity;
import org.springframework.data.repository.CrudRepository;

public interface OutpatientDaoZx extends CrudRepository<OutpatientEntity,Long> {
}
