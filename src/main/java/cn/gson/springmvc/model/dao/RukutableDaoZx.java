package cn.gson.springmvc.model.dao;

import cn.gson.springmvc.model.pojos.RukutableEntity;
import org.springframework.data.repository.CrudRepository;

public interface RukutableDaoZx extends CrudRepository<RukutableEntity,Long> {
}
