package cn.gson.springmvc.model.dao;

import cn.gson.springmvc.model.pojos.ChargeEntity;
import org.springframework.data.repository.CrudRepository;

public interface ChargeDao extends CrudRepository<ChargeEntity,Long> {

}
