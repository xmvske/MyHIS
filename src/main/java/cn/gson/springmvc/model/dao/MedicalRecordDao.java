package cn.gson.springmvc.model.dao;

import cn.gson.springmvc.model.pojos.MedicalRecordEntity;
import org.springframework.data.repository.CrudRepository;

public interface MedicalRecordDao extends CrudRepository<MedicalRecordEntity,Long> {
}
