package cn.gson.springmvc.model.dao;

import cn.gson.springmvc.model.pojos.YizhuDetailEntity;
import org.springframework.data.repository.CrudRepository;

public interface YizhuDetailDao extends CrudRepository<YizhuDetailEntity,Long> {
}
