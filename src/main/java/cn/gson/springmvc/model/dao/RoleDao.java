package cn.gson.springmvc.model.dao;

import cn.gson.springmvc.model.pojos.RoleEntity;
import org.springframework.data.repository.CrudRepository;

public interface RoleDao extends CrudRepository<RoleEntity,Long> {

}
