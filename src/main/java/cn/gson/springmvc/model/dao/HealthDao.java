package cn.gson.springmvc.model.dao;

import cn.gson.springmvc.model.pojos.HealthEntity;
import org.springframework.data.repository.CrudRepository;

public interface HealthDao extends CrudRepository<HealthEntity,Long> {

}
