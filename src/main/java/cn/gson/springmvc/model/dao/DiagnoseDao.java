package cn.gson.springmvc.model.dao;

import cn.gson.springmvc.model.pojos.DiagnoseEntity;
import org.springframework.data.repository.CrudRepository;


public interface DiagnoseDao extends CrudRepository<DiagnoseEntity,Long> {
}
