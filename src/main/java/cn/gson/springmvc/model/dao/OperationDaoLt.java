package cn.gson.springmvc.model.dao;


import cn.gson.springmvc.model.pojos.OperationEntity;
import org.springframework.data.repository.CrudRepository;

public interface OperationDaoLt extends CrudRepository<OperationEntity,Long> {
}
