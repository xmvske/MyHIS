package cn.gson.springmvc.model.dao;

import cn.gson.springmvc.model.pojos.XiangqingEntity;
import org.springframework.data.repository.CrudRepository;

public interface XiangqingDaoZx extends CrudRepository<XiangqingEntity,Long> {

}
