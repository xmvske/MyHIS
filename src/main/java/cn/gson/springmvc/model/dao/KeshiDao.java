package cn.gson.springmvc.model.dao;

import cn.gson.springmvc.model.pojos.KeshiEntity;
import org.springframework.data.repository.CrudRepository;

public interface KeshiDao extends CrudRepository<KeshiEntity,Integer> {

}
