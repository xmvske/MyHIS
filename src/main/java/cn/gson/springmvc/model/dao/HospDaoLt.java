package cn.gson.springmvc.model.dao;


import cn.gson.springmvc.model.pojos.HospEntity;
import org.springframework.data.repository.CrudRepository;


public interface HospDaoLt extends CrudRepository<HospEntity,Long> {
}
