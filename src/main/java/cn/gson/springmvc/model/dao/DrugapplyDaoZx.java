package cn.gson.springmvc.model.dao;

import cn.gson.springmvc.model.pojos.DrugapplyEntity;
import org.springframework.data.repository.CrudRepository;

public interface DrugapplyDaoZx extends CrudRepository<DrugapplyEntity,Long> {
}
