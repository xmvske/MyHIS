package cn.gson.springmvc.model.dao;

import cn.gson.springmvc.model.pojos.UserDetailEntity;
import org.springframework.data.repository.CrudRepository;


public interface LZLUserDetailDao extends CrudRepository<UserDetailEntity,Long> {

}
