package cn.gson.springmvc.model.dao;


import cn.gson.springmvc.model.pojos.RegistrationEntity;
import org.springframework.data.repository.CrudRepository;

public interface RegistrationDao extends CrudRepository<RegistrationEntity,Integer> {

}
