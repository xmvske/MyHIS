package cn.gson.springmvc.model.dao;

import cn.gson.springmvc.model.pojos.YaopinEntity;
import org.springframework.data.repository.CrudRepository;

public interface YaoPinDaoZx extends CrudRepository<YaopinEntity,Long> {
}
