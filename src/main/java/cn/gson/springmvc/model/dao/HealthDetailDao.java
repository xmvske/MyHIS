package cn.gson.springmvc.model.dao;

import cn.gson.springmvc.model.pojos.HealthDetailEntity;
import org.springframework.data.repository.CrudRepository;

public interface HealthDetailDao extends CrudRepository<HealthDetailEntity,Long> {
}
