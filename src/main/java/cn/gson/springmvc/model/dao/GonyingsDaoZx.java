package cn.gson.springmvc.model.dao;

import cn.gson.springmvc.model.pojos.GonyingsEntity;
import org.springframework.data.repository.CrudRepository;

public interface GonyingsDaoZx extends CrudRepository<GonyingsEntity,Long> {
}
