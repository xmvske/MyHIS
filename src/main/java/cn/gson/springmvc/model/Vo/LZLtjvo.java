package cn.gson.springmvc.model.Vo;

import java.util.Date;

public class LZLtjvo {
    private String userDetailSex;
    private String userDetailCard;
    private String userDetailAdd;
    private String userDetailPhone;
    private String juese;
    private String userName;
    private Date userDate;
    private Integer userFhl;
    private String bumenName;
    private String keshiName;

    public String getUserDetailSex() {
        return userDetailSex;
    }

    public void setUserDetailSex(String userDetailSex) {
        this.userDetailSex = userDetailSex;
    }

    public String getUserDetailCard() {
        return userDetailCard;
    }

    public void setUserDetailCard(String userDetailCard) {
        this.userDetailCard = userDetailCard;
    }

    public String getUserDetailAdd() {
        return userDetailAdd;
    }

    public void setUserDetailAdd(String userDetailAdd) {
        this.userDetailAdd = userDetailAdd;
    }

    public String getUserDetailPhone() {
        return userDetailPhone;
    }

    public void setUserDetailPhone(String userDetailPhone) {
        this.userDetailPhone = userDetailPhone;
    }

    public String getJuese() {
        return juese;
    }

    public void setJuese(String juese) {
        this.juese = juese;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getUserDate() {
        return userDate;
    }

    public void setUserDate(Date userDate) {
        this.userDate = userDate;
    }

    public Integer getUserFhl() {
        return userFhl;
    }

    public void setUserFhl(Integer userFhl) {
        this.userFhl = userFhl;
    }

    public String getBumenName() {
        return bumenName;
    }

    public void setBumenName(String bumenName) {
        this.bumenName = bumenName;
    }

    public String getKeshiName() {
        return keshiName;
    }

    public void setKeshiName(String keshiName) {
        this.keshiName = keshiName;
    }

    public LZLtjvo() {
        super();
    }

    @Override
    public String toString() {
        return "LZLtjvo{" +
                "userDetailSex='" + userDetailSex + '\'' +
                ", userDetailCard='" + userDetailCard + '\'' +
                ", userDetailAdd='" + userDetailAdd + '\'' +
                ", userDetailPhone='" + userDetailPhone + '\'' +
                ", juese='" + juese + '\'' +
                ", userName='" + userName + '\'' +
                ", userDate=" + userDate +
                ", userFhl=" + userFhl +
                ", bumenName='" + bumenName + '\'' +
                ", keshiName='" + keshiName + '\'' +
                '}';
    }

    public LZLtjvo(String userDetailSex, String userDetailCard, String userDetailAdd, String userDetailPhone, String juese, String userName, Date userDate, Integer userFhl, String bumenName, String keshiName) {
        this.userDetailSex = userDetailSex;
        this.userDetailCard = userDetailCard;
        this.userDetailAdd = userDetailAdd;
        this.userDetailPhone = userDetailPhone;
        this.juese = juese;
        this.userName = userName;
        this.userDate = userDate;
        this.userFhl = userFhl;
        this.bumenName = bumenName;
        this.keshiName = keshiName;
    }
}
