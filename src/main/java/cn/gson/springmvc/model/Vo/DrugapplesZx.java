package cn.gson.springmvc.model.Vo;

import java.math.BigDecimal;

public class DrugapplesZx {
    private Integer yaopinId;
    private String yaopinName;
    private BigDecimal yaopinDj;
    private Integer buyNumber;

    @Override
    public String toString() {
        return "DrugapplesZx{" +
                "yaopinId=" + yaopinId +
                ", yaopinName='" + yaopinName + '\'' +
                ", yaopinDj=" + yaopinDj +
                ", buyNumber=" + buyNumber +
                '}';
    }

    public Integer getYaopinId() {
        return yaopinId;
    }

    public void setYaopinId(Integer yaopinId) {
        this.yaopinId = yaopinId;
    }

    public String getYaopinName() {
        return yaopinName;
    }

    public void setYaopinName(String yaopinName) {
        this.yaopinName = yaopinName;
    }

    public BigDecimal getYaopinDj() {
        return yaopinDj;
    }

    public void setYaopinDj(BigDecimal yaopinDj) {
        this.yaopinDj = yaopinDj;
    }

    public Integer getBuyNumber() {
        return buyNumber;
    }

    public void setBuyNumber(Integer buyNumber) {
        this.buyNumber = buyNumber;
    }

    public DrugapplesZx(Integer yaopinId, String yaopinName, BigDecimal yaopinDj, Integer buyNumber) {

        this.yaopinId = yaopinId;
        this.yaopinName = yaopinName;
        this.yaopinDj = yaopinDj;
        this.buyNumber = buyNumber;
    }
}
