package cn.gson.springmvc.model.Vo;

public class YaoPinsZx {
    private Integer yaopinId;
    private String  yaopinName;
    private String yaopinGuige;
    private int xiangqingSum;

    @Override
    public String toString() {
        return "YaoPins{" +
                "yaopinId=" + yaopinId +
                ", yaopinName='" + yaopinName + '\'' +
                ", yaopinGuige='" + yaopinGuige + '\'' +
                ", xiangqingSum=" + xiangqingSum +
                '}';
    }

    public Integer getYaopinId() {
        return yaopinId;
    }

    public void setYaopinId(Integer yaopinId) {
        this.yaopinId = yaopinId;
    }

    public String getYaopinName() {
        return yaopinName;
    }

    public void setYaopinName(String yaopinName) {
        this.yaopinName = yaopinName;
    }

    public String getYaopinGuige() {
        return yaopinGuige;
    }

    public void setYaopinGuige(String yaopinGuige) {
        this.yaopinGuige = yaopinGuige;
    }

    public int getXiangqingSum() {
        return xiangqingSum;
    }

    public void setXiangqingSum(int xiangqingSum) {
        this.xiangqingSum = xiangqingSum;
    }

    public YaoPinsZx() {

    }

    public YaoPinsZx(Integer yaopinId, String yaopinName, String yaopinGuige, int xiangqingSum) {

        this.yaopinId = yaopinId;
        this.yaopinName = yaopinName;
        this.yaopinGuige = yaopinGuige;
        this.xiangqingSum = xiangqingSum;
    }
}
