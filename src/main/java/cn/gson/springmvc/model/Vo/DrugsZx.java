package cn.gson.springmvc.model.Vo;

public class DrugsZx {
    private Integer yaopinId;
    private String yaopinName;
    private Double yaopinDj;
    private int buyNumber;

    @Override
    public String toString() {
        return "Drugs{" +
                "yaopinId=" + yaopinId +
                ", yaopinName='" + yaopinName + '\'' +
                ", yaopinDj=" + yaopinDj +
                ", buyNumber=" + buyNumber +
                '}';
    }

    public Integer getYaopinId() {
        return yaopinId;
    }

    public void setYaopinId(Integer yaopinId) {
        this.yaopinId = yaopinId;
    }

    public String getYaopinName() {
        return yaopinName;
    }

    public void setYaopinName(String yaopinName) {
        this.yaopinName = yaopinName;
    }

    public Double getYaopinDj() {
        return yaopinDj;
    }

    public void setYaopinDj(Double yaopinDj) {
        this.yaopinDj = yaopinDj;
    }

    public int getBuyNumber() {
        return buyNumber;
    }

    public void setBuyNumber(int buyNumber) {
        this.buyNumber = buyNumber;
    }

    public DrugsZx(Integer yaopinId, String yaopinName, Double yaopinDj, int buyNumber) {

        this.yaopinId = yaopinId;
        this.yaopinName = yaopinName;
        this.yaopinDj = yaopinDj;
        this.buyNumber = buyNumber;
    }

    public DrugsZx() {

    }
}
