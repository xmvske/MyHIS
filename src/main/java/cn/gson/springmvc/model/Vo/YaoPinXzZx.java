package cn.gson.springmvc.model.Vo;

import java.util.Date;

public class YaoPinXzZx {
    private String namen;
    private Integer region;
    private String datee;
    private String resource;
    private Date types;
    private String desc;

    public String getNamen() {
        return namen;
    }

    public YaoPinXzZx() {
    }

    @Override
    public String toString() {
        return "YaoPinXzZx{" +
                "namen='" + namen + '\'' +
                ", region=" + region +
                ", datee='" + datee + '\'' +
                ", resource='" + resource + '\'' +
                ", types=" + types +
                ", desc='" + desc + '\'' +
                '}';
    }

    public void setNamen(String namen) {
        this.namen = namen;
    }

    public Integer getRegion() {
        return region;
    }

    public void setRegion(Integer region) {
        this.region = region;
    }

    public String getDatee() {
        return datee;
    }

    public void setDatee(String datee) {
        this.datee = datee;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public Date getTypes() {
        return types;
    }

    public void setTypes(Date types) {
        this.types = types;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public YaoPinXzZx(String namen, Integer region, String datee, String resource, Date types, String desc) {
        this.namen = namen;
        this.region = region;
        this.datee = datee;
        this.resource = resource;
        this.types = types;
        this.desc = desc;
    }
}
