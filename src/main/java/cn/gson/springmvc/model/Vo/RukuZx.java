package cn.gson.springmvc.model.Vo;

import java.util.List;

public class RukuZx {
    private String user;
    private String rukur;
    private int amount;

    private List<YaoPinsZx> rkyaop;


    public RukuZx(String user, String rukur, int amount, List<YaoPinsZx> rkyaop) {
        this.user = user;
        this.rukur = rukur;
        this.amount = amount;
        this.rkyaop = rkyaop;
    }

    @Override
    public String toString() {
        return "Ruku{" +
                "user='" + user + '\'' +
                ", rukur='" + rukur + '\'' +
                ", amount=" + amount +
                ", rkyaop=" + rkyaop +
                '}';
    }

    public String getRukur() {

        return rukur;
    }

    public void setRukur(String rukur) {
        this.rukur = rukur;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public List<YaoPinsZx> getRkyaop() {
        return rkyaop;
    }

    public void setRkyaop(List<YaoPinsZx> rkyaop) {
        this.rkyaop = rkyaop;
    }

    public RukuZx() {

    }
}
