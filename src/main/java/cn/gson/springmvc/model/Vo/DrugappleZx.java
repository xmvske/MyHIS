package cn.gson.springmvc.model.Vo;

import java.util.List;

public class DrugappleZx {
    private String user;

    private Integer amount;


    List<DrugapplesZx> druge;

    @Override
    public String toString() {
        return "DrugappleZx{" +
                "user='" + user + '\'' +
                ", amount=" + amount +
                ", druge=" + druge +
                '}';
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public List<DrugapplesZx> getDruge() {
        return druge;
    }

    public void setDruge(List<DrugapplesZx> druge) {
        this.druge = druge;
    }

    public DrugappleZx(String user, Integer amount, List<DrugapplesZx> druge) {

        this.user = user;
        this.amount = amount;
        this.druge = druge;
    }
}
