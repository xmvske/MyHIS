package cn.gson.springmvc.model.Vo;

import java.util.Date;
import java.util.List;

public class PurchaseInfoZx {
    private String user;
    private Date values;
    private Double amount;
    private Integer region;

    private List<DrugsZx> drugs;

    @Override
    public String toString() {
        return "PurchaseInfo{" +
                "user='" + user + '\'' +
                ", values=" + values +
                ", amount=" + amount +
                ", region=" + region +
                ", drugs=" + drugs +
                '}';
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Date getValues() {
        return values;
    }

    public void setValues(Date values) {
        this.values = values;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getRegion() {
        return region;
    }

    public void setRegion(Integer region) {
        this.region = region;
    }

    public List<DrugsZx> getDrugs() {
        return drugs;
    }

    public void setDrugs(List<DrugsZx> drugs) {
        this.drugs = drugs;
    }

    public PurchaseInfoZx() {

    }

    public PurchaseInfoZx(String user, Date values, Double amount, Integer region, List<DrugsZx> drugs) {

        this.user = user;
        this.values = values;
        this.amount = amount;
        this.region = region;
        this.drugs = drugs;
    }
}

