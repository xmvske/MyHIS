package cn.gson.springmvc.model.mapper;

import cn.gson.springmvc.model.pojos.UserEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserMapper {
    List<UserEntity> findByKeshi(@Param("keshi") Long keshi,
                                 @Param("role") String role);

//    JSONArray findByKeshi2(@Param("keshi") Long keshi,
//                          @Param("role") String role,
//                          @Param("top") Long top);
}
