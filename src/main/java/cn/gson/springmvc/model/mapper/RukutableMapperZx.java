package cn.gson.springmvc.model.mapper;

import com.alibaba.fastjson.JSONArray;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RukutableMapperZx{
    //查询
    public JSONArray findAll();
    //根据采购单查询详情
    public JSONArray findAlln(Integer purchasingId);
}
