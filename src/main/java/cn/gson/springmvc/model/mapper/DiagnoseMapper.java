package cn.gson.springmvc.model.mapper;

import cn.gson.springmvc.model.pojos.DiagnoseEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DiagnoseMapper {
    List<DiagnoseEntity> FindAllByMid(@Param("mid") Long mid);
}
