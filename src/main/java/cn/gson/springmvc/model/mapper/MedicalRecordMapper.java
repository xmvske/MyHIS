package cn.gson.springmvc.model.mapper;

import cn.gson.springmvc.model.pojos.MedicalRecordEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface MedicalRecordMapper {

    List<MedicalRecordEntity> findAllByMid(@Param("mid") Long mid);
}
