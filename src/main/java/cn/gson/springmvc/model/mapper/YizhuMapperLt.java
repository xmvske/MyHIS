package cn.gson.springmvc.model.mapper;


import com.alibaba.fastjson.JSONArray;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


@Mapper
public interface YizhuMapperLt {
    public JSONArray hospyizhustate();

    //护士工作站，查询医嘱
    public  JSONArray yizhufindAll();

    //护士工作站，查询医嘱详情
    public  JSONArray yizhuXQfindAll(@Param("YizhuIdXQ") Long YizhuIdXQ);
}
