package cn.gson.springmvc.model.mapper;

import cn.gson.springmvc.model.pojos.RegistrationEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

@Mapper
public interface RegistrationMapper {
    List<RegistrationEntity> findAllReg();

    List<RegistrationEntity> findRegByDate(@Param("date") Date date,
                                           @Param("uid")Long uid,
                                           @Param("state")Long state);

    List<RegistrationEntity> findRegByDate2(@Param("date") Date date,
                                           @Param("uid")Long uid);

    List<RegistrationEntity> findAllReg(@Param("date") Date date,
                                        @Param("medid")Long medid);

    List<RegistrationEntity> findByNow(@Param("date")Date date);

    RegistrationEntity findByIdM(@Param("rid") Long rid);
}
