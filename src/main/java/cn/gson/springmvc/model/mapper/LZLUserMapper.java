package cn.gson.springmvc.model.mapper;

import cn.gson.springmvc.model.Vo.LZLtjvo;
import com.alibaba.fastjson.JSONArray;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

@Mapper
public interface LZLUserMapper {
    public JSONArray findAll();
    public void xgmm(Integer userid);
    public void yglizhi(Integer userid);
    public JSONArray denlu(@Param("userName") String userName, @Param("userPass") String userPass);
    public void xinzengjs(@Param("userId") Integer userId,@Param("roleId") Integer roleId);
    public void xinzengyg(@Param("bumenId") Integer bumenId,@Param("keshiId") Integer keshiId,@Param("userName") String userName,@Param("userDate") Date userDate);
    public void xinzengxq(@Param("userId") Integer userId,@Param("userDetailSex") String userDetailSex,@Param("userDetailCard") String userDetailCard,@Param("userDetailAdd") String userDetailAdd,@Param("userDetailPhone") String userDetailPhone);
    public Integer userid(String username);
    public Integer bumenid(String bumenName);
    public Integer keshid(String keshiName);
    public Integer roleid(String juse);
}
