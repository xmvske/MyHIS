package cn.gson.springmvc.model.mapper;

import cn.gson.springmvc.model.pojos.ChargeEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ChargeMapper  {
    List<ChargeEntity> findByRid(@Param("rid") Long rid);
}
