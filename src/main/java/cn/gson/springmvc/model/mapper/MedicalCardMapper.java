package cn.gson.springmvc.model.mapper;

import com.alibaba.fastjson.JSONArray;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface MedicalCardMapper {
    JSONArray FindMeadicalCarByState(@Param("state") Integer state);
    JSONArray FindMeadicalInit(@Param("state") Integer state);
    JSONArray FindMeadicalCar();
}
