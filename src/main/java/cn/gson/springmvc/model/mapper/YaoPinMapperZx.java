package cn.gson.springmvc.model.mapper;

import com.alibaba.fastjson.JSONArray;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface YaoPinMapperZx {

    //供应商和药品查询
    public JSONArray findAlln(Integer gonyingsId);

    public JSONArray findAll();
}
