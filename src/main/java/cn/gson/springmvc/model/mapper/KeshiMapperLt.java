package cn.gson.springmvc.model.mapper;

import com.alibaba.fastjson.JSONArray;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface KeshiMapperLt {
    public JSONArray keshifinall();

    public JSONArray keshibed(@Param("ksid") Integer ksid);

    public JSONArray keshiroom(@Param("keidromm") Integer ksid);
}
