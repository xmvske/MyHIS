package cn.gson.springmvc.model.services;

import cn.gson.springmvc.model.dao.OpRecordDaoLt;
import cn.gson.springmvc.model.dao.OperationDaoLt;
import cn.gson.springmvc.model.mapper.OpRecordMapperLt;
import cn.gson.springmvc.model.pojos.OpRecordEntity;
import cn.gson.springmvc.model.pojos.OperationEntity;
import com.alibaba.fastjson.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional(rollbackFor = Exception.class)
public class OpRecordServiceLt {

    @Autowired
    OpRecordMapperLt opRecordMapperLt;

    @Autowired
    OpRecordDaoLt opRecordDaoLt;

    @Autowired
    OperationDaoLt operationDaoLt;

    /*
     * 查询所有完成手术记录
     * @return
     */
    public JSONArray oprecordnAll(){
        return   opRecordMapperLt.oprecordnAll();
    }


    /**
     * 新增记录表，修改安排表状态
     */
    @Transactional(value ="transactionManager" ,rollbackFor = Exception.class)
    public  void opjladd(OpRecordEntity opRecordEntity){
        opRecordDaoLt.save(opRecordEntity);
        System.out.println(opRecordEntity.getOperationByOperationId().getOperationId());
        //修改安排表状态findById
       OperationEntity uu=operationDaoLt.findById(new Long(opRecordEntity.getOperationByOperationId().getOperationId())).get();

        System.out.println(opRecordEntity.getOperationByOperationId().getOperationId());
        uu.setOperationState(2);
        operationDaoLt.save(uu);

    }

}
