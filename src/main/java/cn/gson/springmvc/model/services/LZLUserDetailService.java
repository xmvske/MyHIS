package cn.gson.springmvc.model.services;

import cn.gson.springmvc.model.Vo.LZLtjvo;
import cn.gson.springmvc.model.dao.LZLUserDao;
import cn.gson.springmvc.model.dao.LZLUserDetailDao;
import cn.gson.springmvc.model.mapper.LZLUserMapper;
import cn.gson.springmvc.model.pojos.UserDetailEntity;
import cn.gson.springmvc.model.pojos.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
public class LZLUserDetailService {
    @Autowired
    LZLUserDetailDao lzlUserDetailDao;

    @Autowired
    LZLUserDao lzlUserDao;

    @Autowired
    LZLUserMapper lzlUserMapper;

    @Transactional(value ="transactionManager" ,rollbackFor = Exception.class)
    public void savePurchase(LZLtjvo aa){
        System.out.println(aa);
        System.out.println(1);
        Integer bumenid=lzlUserMapper.bumenid(aa.getBumenName());
        Integer keshid=lzlUserMapper.keshid(aa.getKeshiName());
        Integer roleid=lzlUserMapper.roleid(aa.getJuese());
        System.out.println(bumenid+"\t"+keshid+"\t");
        lzlUserMapper.xinzengyg(bumenid,keshid,aa.getUserName(),aa.getUserDate());
        Integer userid=lzlUserMapper.userid(aa.getUserName());
        lzlUserMapper.xinzengxq(userid,aa.getUserDetailSex(),aa.getUserDetailCard(),aa.getUserDetailAdd(),aa.getUserDetailPhone());
        lzlUserMapper.xinzengjs(userid,roleid);
        }
}