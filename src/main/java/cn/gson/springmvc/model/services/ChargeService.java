package cn.gson.springmvc.model.services;


import cn.gson.springmvc.model.dao.ChargeDao;
import cn.gson.springmvc.model.dao.ChargeDetailDao;
import cn.gson.springmvc.model.dao.RegistrationDao;
import cn.gson.springmvc.model.mapper.ChargeMapper;
import cn.gson.springmvc.model.mapper.RegistrationMapper;
import cn.gson.springmvc.model.pojos.ChargeDetailEntity;
import cn.gson.springmvc.model.pojos.ChargeEntity;
import cn.gson.springmvc.model.pojos.RegistrationEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
public class ChargeService  {

    @Autowired
    ChargeDao chad;

    @Autowired
    ChargeMapper cham;

    @Autowired
    ChargeDetailDao cdd;

    @Autowired
    RegistrationDao regd;

    @Autowired
    RegistrationMapper regm;

    public List<ChargeEntity> findByRid(Long rid){
        return cham.findByRid(rid);
    }

    public int savaCha(ChargeEntity ce){
        chad.save(ce);

        for (ChargeDetailEntity cde:ce.getChargeDetailsByChargeId()){
            cdd.save(cde);
        }
        return 1;
    }

    @Transactional(value = "transactionManager", rollbackFor = Exception.class)
    public int stateCha(ChargeEntity ce){
        RegistrationEntity registrationByRegistrationId = regm.
                findByIdM(new Long(ce.getRegistrationByRegistrationId().getRegistrationId()));
        chad.save(ce);
//        registrationByRegistrationId.setRegistrationState(3);
//        regd.save(registrationByRegistrationId);
        return 1;
    }
}
