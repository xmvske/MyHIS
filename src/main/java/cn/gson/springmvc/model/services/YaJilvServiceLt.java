package cn.gson.springmvc.model.services;


import cn.gson.springmvc.model.dao.HospDaoLt;
import cn.gson.springmvc.model.dao.YaJilvDaoLt;
import cn.gson.springmvc.model.mapper.YaJilvMapperLt;
import cn.gson.springmvc.model.pojos.HospEntity;
import cn.gson.springmvc.model.pojos.YaJilvEntity;
import com.alibaba.fastjson.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
public class YaJilvServiceLt {

    @Autowired
    YaJilvDaoLt yaJilvDaoLt;

    @Autowired
    YaJilvMapperLt yaJilvMapperLt;

    @Autowired
    HospDaoLt hospDaoLt;

    @Transactional(value ="transactionManager" ,rollbackFor = Exception.class)
    public void addyaji(YaJilvEntity yaJilvEntity){
        yaJilvDaoLt.save(yaJilvEntity);
        HospEntity ho = hospDaoLt.findById(new Long(yaJilvEntity.getHospByHospId().getHospId())).get();
        //修改住院登记money
        ho.setHospCashMoney(yaJilvEntity.getYaYu());
        hospDaoLt.save(ho);
    }


    public JSONArray allyjjl(Long hospid) {
       return yaJilvMapperLt.allyjjl(hospid);
    }


}
