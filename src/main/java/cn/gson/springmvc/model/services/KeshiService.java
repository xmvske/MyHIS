package cn.gson.springmvc.model.services;

import cn.gson.springmvc.model.mapper.KeshiMapper;
import com.alibaba.fastjson.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
public class KeshiService {
    @Autowired
    KeshiMapper kesm;


    @Transactional(value ="transactionManager_mybatis" ,rollbackFor = Exception.class)
    public JSONArray findAllmapper(){
        return kesm.findAll();
    }
}
