package cn.gson.springmvc.model.services;


import cn.gson.springmvc.model.dao.YizhuDaoLt;
import cn.gson.springmvc.model.dao.YizhuDataDaoLt;
import cn.gson.springmvc.model.mapper.RepertoryMapperLt;
import cn.gson.springmvc.model.mapper.YizhuMapperLt;
import cn.gson.springmvc.model.pojos.YizhuDetailEntity;
import cn.gson.springmvc.model.pojos.YizhuEntity;
import com.alibaba.fastjson.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
public class YizhuServiceLt {

    @Autowired
    YizhuDaoLt yizhuDaoLt;

    @Autowired
    YizhuMapperLt yizhuMapperLt;

    @Autowired
    RepertoryMapperLt repertoryMapperLt;//药房

    @Autowired
    YizhuDataDaoLt yizhuDataDaoLt;//医嘱详情

    //查询未开医嘱人
    public JSONArray hospyizhustate(){
        return  yizhuMapperLt.hospyizhustate();
    }

    //药房
    public JSONArray ReYao(){
        return  repertoryMapperLt.Reyaofan();
    }


    /*医嘱加详情*/
    @Transactional(value = "transactionManager",rollbackFor = Exception.class)
    public void YiZhuJiaXianQin(YizhuEntity yz){


        //医嘱
        System.out.println(yz.toString());
        YizhuEntity save = yizhuDaoLt.save(yz);
        System.out.println("哈哈"+yz.getYizhuDetailsByYizhuId().toString());
        for (YizhuDetailEntity yizhuDetailEntity : yz.getYizhuDetailsByYizhuId()) {
            yizhuDetailEntity.setYizhuByYizhuId(save);
            yizhuDataDaoLt.save(yizhuDetailEntity);
        }
    }

    /**
     *  查询所有未执行医嘱+详情+药品
     * @return
     */
    @Transactional(value = "transactionManager",rollbackFor = Exception.class)
    public JSONArray yizhufindAll(){
        return  yizhuMapperLt.yizhufindAll();
    }

    /**
     * 根据id查询：医嘱详情+药品
     * @param YizhuIdXQ
     * @return
     */
    @Transactional(value = "transactionManager",rollbackFor = Exception.class)
    public JSONArray yizhuXQfindAll(Long YizhuIdXQ){
        return  yizhuMapperLt.yizhuXQfindAll(YizhuIdXQ);
    }


}
