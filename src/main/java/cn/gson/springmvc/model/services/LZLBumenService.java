package cn.gson.springmvc.model.services;

import cn.gson.springmvc.model.dao.LZLBumenDao;
import cn.gson.springmvc.model.mapper.LZLBumenMapper;
import com.alibaba.fastjson.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
public class LZLBumenService {
    @Autowired
    LZLBumenMapper lzlBumenMapper;
    @Autowired
    LZLBumenDao lzlBumenDao;

    public JSONArray findAll(){return lzlBumenMapper.findAll();}
}