package cn.gson.springmvc.model.services;


import cn.gson.springmvc.model.mapper.ProjectMapperXzy;
import cn.gson.springmvc.model.mapper.YuyueMapperXzy;
import com.alibaba.fastjson.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
public class ProjectServiceXzy {

    @Autowired
    ProjectMapperXzy ProjectMapper;

    public JSONArray finaAll(){
        return ProjectMapper.findAll();
    }

}



