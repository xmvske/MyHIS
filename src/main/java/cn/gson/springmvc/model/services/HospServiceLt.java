package cn.gson.springmvc.model.services;

import cn.gson.springmvc.model.dao.HospDaoLt;
import cn.gson.springmvc.model.dao.YaJilvDaoLt;
import cn.gson.springmvc.model.dao.ZhuyuanApplyDaoLt;
import cn.gson.springmvc.model.mapper.HospMapperLt;
import cn.gson.springmvc.model.pojos.HospEntity;
import cn.gson.springmvc.model.pojos.ZhuyuanApplyEntity;
import com.alibaba.fastjson.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional(rollbackFor = Exception.class)
public class HospServiceLt {

    @Autowired
    HospMapperLt hospMapper;

    @Autowired
    HospDaoLt hospDao;

    @Autowired
    ZhuyuanApplyDaoLt zhuyuanApplyDaoLt;


    @Autowired
    YaJilvDaoLt yaJilvDaoLt;//押金记录

    /**
     * 查询登记记录
     * @return
     */
    public JSONArray HospFindAll(){
        return  hospMapper.findAll();
    }

    /**
     * 新增记录表
     * @param hospEntity
     */
    @Transactional(value ="transactionManager" ,rollbackFor = Exception.class)
   public void  addHosp(HospEntity hospEntity){
       hospDao.save(hospEntity);
       ZhuyuanApplyEntity zhuyuanApplyEntity = zhuyuanApplyDaoLt.findById(new Long(hospEntity.getZhuyuanApplyByZhuyuanApplyId().getZhuyuanApplyId())).get();
       zhuyuanApplyEntity.setApplyState(2);
       zhuyuanApplyDaoLt.save(zhuyuanApplyEntity);

//       YaJilvEntity jilv=new YaJilvEntity();
//       jilv.setHospByHospId(hospEntity);
//       jilv.setYaTotal(hospEntity.getHospCashMoney());
//       jilv.setYaXu(0);
//       jilv.setYaYu(hospEntity.getHospCashMoney());
//       jilv.setYaDate(new Date());
//       System.out.println("");
//       yaJilvDaoLt.save(jilv);
   }


    /**
     * 查询所有
     */
    public JSONArray DJ(){
        return hospMapper.hospfindAll();
    }

}
