package cn.gson.springmvc.model.services;

import cn.gson.springmvc.model.mapper.RegistrationMapper;
import cn.gson.springmvc.model.mapper.UserMapper;
import cn.gson.springmvc.model.pojos.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional(rollbackFor = Exception.class)
public class UserService {
    @Autowired
    UserMapper userm;

    @Autowired
    RegistrationMapper regm;

//    查询该科室医生,统计叫号人数
    @Transactional(value ="transactionManager_mybatis" ,rollbackFor = Exception.class)
    public List<Map<String,Object>> findUserByKeshi(Long keshi,String role){
        List<UserEntity> byKeshi = userm.findByKeshi(keshi, role);
        List<UserEntity> byKx =new ArrayList<>();
        List<UserEntity> bySy =new ArrayList<>();
//        List<Map<Integer,Object>> byCout=new ArrayList<>();
        List<Integer> byCout=new ArrayList<>();
        List<Integer> byCout2=new ArrayList<>();
//        Map<Integer,Object> m=new HashMap<>();

        for (UserEntity u:byKeshi) {
            int count = regm.findRegByDate2(new Date(), new Long(u.getUserId())).size();
            if(count==0){
                byKx.add(u);
                byCout.add(count);
            }else{
                bySy.add(u);
                byCout2.add(count);
            }
        }

        List<Map<String,Object>> objects = new ArrayList<>();
        Map<String,Object> map =new HashMap<>();
        map.put("label","空闲医生");
        map.put("options",byKx);
        map.put("count",byCout);

        Map<String,Object> map2 =new HashMap<>();
        map2.put("label","其余医生");
        map2.put("options",bySy);
        map2.put("count",byCout2);
        objects.add(map);
        objects.add(map2);


        return objects;
    }
}
