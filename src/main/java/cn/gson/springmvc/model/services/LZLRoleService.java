package cn.gson.springmvc.model.services;

import cn.gson.springmvc.model.dao.LZLRoleDao;
import cn.gson.springmvc.model.mapper.LZLRoleMapper;
import com.alibaba.fastjson.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
public class LZLRoleService {
    @Autowired
    LZLRoleMapper lzlRoleMapper;
    @Autowired
    LZLRoleDao lzlRoleDao;

    public JSONArray findAll(){return lzlRoleMapper.findAll();}
}