package cn.gson.springmvc.model.services;

import cn.gson.springmvc.model.dao.MedicalRecordDao;
import cn.gson.springmvc.model.mapper.MedicalRecordMapper;
import cn.gson.springmvc.model.pojos.MedicalRecordEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
public class MedicalRecordService {

    @Autowired
    MedicalRecordDao mrdd;

    @Autowired
    MedicalRecordMapper mrdm;

    @Transactional(value ="transactionManager" ,rollbackFor = Exception.class)
    public int SaveMrd(MedicalRecordEntity mre){
        return mrdd.save(mre)==null?0:1;
    }


    public List<MedicalRecordEntity> findAllByMid(Long mid){
        return mrdm.findAllByMid(mid);
    }
}
