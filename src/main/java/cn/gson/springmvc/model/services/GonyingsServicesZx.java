package cn.gson.springmvc.model.services;

import cn.gson.springmvc.model.dao.GonyingsDaoZx;
import cn.gson.springmvc.model.mapper.GonyingsMapperZx;
import cn.gson.springmvc.model.pojos.GonyingsEntity;
import com.alibaba.fastjson.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
public class GonyingsServicesZx {

    @Autowired
    GonyingsMapperZx gonyingsMapperZx;

    @Autowired
    GonyingsDaoZx gonyingsDaoZx;


    public JSONArray findAlll(){
        return gonyingsMapperZx.findAlll();
    }

    //新增
    @Transactional(value ="transactionManager" ,rollbackFor = Exception.class)
    public void insertGonyings(GonyingsEntity xz){
        gonyingsDaoZx.save(xz);
    }

}
