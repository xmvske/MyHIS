package cn.gson.springmvc.model.services;


import cn.gson.springmvc.model.dao.OpSqDaoLt;
import cn.gson.springmvc.model.dao.OperationDaoLt;
import cn.gson.springmvc.model.mapper.OperationMapperLt;
import cn.gson.springmvc.model.pojos.OpSqEntity;
import cn.gson.springmvc.model.pojos.OperationEntity;
import com.alibaba.fastjson.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional(rollbackFor = Exception.class)
public class OperationServiceLt {

    @Autowired
    OperationMapperLt operationMapperLt;

    @Autowired
    OperationDaoLt operationDaoLt;

    @Autowired
    OpSqDaoLt opSqDaoLt;

    /**
     * 查询所有未执行手术
     * @return
     */
    public JSONArray opAll(){
        return   operationMapperLt.operationAll();
    }

    /**
     * 新增未执行手术
     */
    public  void  opadd(OperationEntity operationEntity){
        operationDaoLt.save(operationEntity);
        //改手术申请表
        OpSqEntity opSqEntity = opSqDaoLt.findById(operationEntity.getOpSqBySqRshenqingId2().getSqRshenqingId2()).get();
        opSqEntity.setSqState(2);
        opSqDaoLt.save(opSqEntity);
    }


}
