package cn.gson.springmvc.model.services;

import cn.gson.springmvc.model.dao.*;
import cn.gson.springmvc.model.mapper.DiagnoseMapper;
import cn.gson.springmvc.model.pojos.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
public class DiagnoseService  {

    @Autowired
    DiagnoseDao diad;

    @Autowired
    MedicalRecordDao mrd;

    @Autowired
    YizhuDao yizd;

    @Autowired
    YizhuDetailDao ydd;

    @Autowired
    RegistrationDao regd;

    @Autowired
    ChargeDao chad;

    @Autowired
    HealthDao head;

    @Autowired
    HealthDetailDao hdd;

    @Autowired
    DiagnoseMapper diam;
    @Transactional(value = "transactionManager", rollbackFor = Exception.class)
    public int SaveDia(DiagnoseEntity de){
        diad.save(de);
        return 1;
    }

    /**
     *
     * 新增,看诊模块,看诊记录,电子病历,医嘱等
     */
    @Transactional(value = "transactionManager", rollbackFor = Exception.class)
    public int SaveKanZhen( DiagnoseEntity de){
        //医嘱
        YizhuEntity yizhuEntity = yizd.save(de.getYizhuByYizhuId());
        //医嘱详情
        for(YizhuDetailEntity yde:de.getYizhuByYizhuId().getYizhuDetailsByYizhuId()){
            yde.setYizhuByYizhuId(yizhuEntity);
            ydd.save(yde);
        }



        //体检
        HealthEntity healthEntity = head.save(de.getHealthByHealthId());
        for(HealthDetailEntity hde:de.getHealthByHealthId().getHealthDetailsByHealthId()){
            hde.setHealthByHealthId(healthEntity);
            hdd.save(hde);
        }


        //电子病历
        MedicalRecordEntity mre = mrd.save(de.getMedicalRecordByMedicalRecordId());

        //挂号状态
        RegistrationEntity registrationEntity = regd.findById(de.getRegistrationByRegistrationId().getRegistrationId()).get();
        registrationEntity.setRegistrationState(2);
        regd.save(registrationEntity);

        //收费表
        ChargeEntity ce = de.getChargeByChargeId();
        ce.setRegistrationByRegistrationId(de.getRegistrationByRegistrationId());
        ce.setMedicalCardByMedicalId(de.getMedicalCardByMedicalId());
        ce.setHealthByHealthId(de.getHealthByHealthId());
        ce.setYizhuByYizhuId(de.getYizhuByYizhuId());
        ChargeEntity chargeEntity = chad.save(ce);

        de.setChargeByChargeId(chargeEntity);
        diad.save(de);

        return 1;
    }

    public List<DiagnoseEntity> FindAllByMid(Long mid){
        return diam.FindAllByMid(mid);
    }
}
