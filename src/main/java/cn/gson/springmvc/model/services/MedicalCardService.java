package cn.gson.springmvc.model.services;

import cn.gson.springmvc.model.dao.MedicalCardDao;
import cn.gson.springmvc.model.mapper.MedicalCardMapper;
import cn.gson.springmvc.model.pojos.MedicalCardEntity;
import com.alibaba.fastjson.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
public class MedicalCardService {

    @Autowired
    MedicalCardDao mead;

    @Autowired
    MedicalCardMapper medm;
    @Transactional(value ="transactionManager_mybatis" ,rollbackFor = Exception.class)
    public JSONArray FindMeadicalCar(Integer s){
        return  s==null?medm.FindMeadicalCar()
                        :medm.FindMeadicalCarByState(s);
    }

    @Transactional(value ="transactionManager_mybatis" ,rollbackFor = Exception.class)
    public JSONArray FindMeadicalCarInit(Integer s){
        return  medm.FindMeadicalInit(s);
    }
    @Transactional(value ="transactionManager" ,rollbackFor = Exception.class)
    public int SaveMeadicalCar(MedicalCardEntity me){
        MedicalCardEntity save = mead.save(me);
        return save.getMedicalState()==2?1:0;
    }
}
