package cn.gson.springmvc.model.services;


import cn.gson.springmvc.model.dao.RegistrationDao;
import cn.gson.springmvc.model.mapper.RegistrationMapper;
import cn.gson.springmvc.model.pojos.RegistrationEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
public class RegistrationSeivice {
    @Autowired
    RegistrationDao regd;

    @Autowired
    RegistrationMapper regm;

    @Transactional(value ="transactionManager" ,rollbackFor = Exception.class)
    public int saveReg(RegistrationEntity re){
        List<RegistrationEntity> byMedical = findByMedical(re.getRegistrationDate(), new Long(re.getMedicalCardByMedicalId().getMedicalId()));
        System.out.println(byMedical.size());
        if(byMedical==null||byMedical.size()==0){
            regd.save(re);
            return 1;
        }else{
            if(re.getRegistrationId()!=0){
                regd.save(re);
                return 1;
            }else{
                return 0;
            }
        }

    }

    @Transactional(value ="transactionManager" ,rollbackFor = Exception.class)
    public List<RegistrationEntity> findAll(){
        return regm.findAllReg();
    }

    @Transactional(value ="transactionManager" ,rollbackFor = Exception.class)
    public List<RegistrationEntity> findByMedical(Date date,Long medid){
        return regm.findAllReg(date,medid);
    }

    @Transactional(value ="transactionManager" ,rollbackFor = Exception.class)
    public List<RegistrationEntity> findRegByDate(Date date,Long uid){
        System.out.println(date);
        return regm.findRegByDate(date,uid,11L);
    }

//    @Transactional(value ="transactionManager" ,rollbackFor = Exception.class)
    public List<RegistrationEntity> findRegByDateF(Date date,Long uid){
        System.out.println(date);
        return regm.findRegByDate(date,uid,2L);
    }

    //查询当天挂号记录
    public List<RegistrationEntity> findByNow(){
        return regm.findByNow(new java.util.Date());
    }

    //修改状态
    @Transactional(value ="transactionManager" ,rollbackFor = Exception.class)
    public int changeState(RegistrationEntity re,Integer state){
        Integer old=re.getRegistrationState();
        re.setRegistrationState(state);
        RegistrationEntity save = regd.save(re);
        //前后相同则修改失败
        return save.getRegistrationState()!=old?1:0;
    }
}
