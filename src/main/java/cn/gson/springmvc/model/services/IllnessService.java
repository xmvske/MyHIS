package cn.gson.springmvc.model.services;

import cn.gson.springmvc.model.mapper.IllnessMapper;
import com.alibaba.fastjson.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
public class IllnessService {

    @Autowired
    IllnessMapper illm;

    public JSONArray MfindAll(){
        return illm.MfindAll();
    }
}
