package cn.gson.springmvc.model.services;

import cn.gson.springmvc.model.Vo.DrugappleZx;
import cn.gson.springmvc.model.Vo.DrugapplesZx;
import cn.gson.springmvc.model.dao.DrugapplyDaoZx;
import cn.gson.springmvc.model.dao.DrugapplyTableDaoZx;
import cn.gson.springmvc.model.dao.YaoPinDaoZx;
import cn.gson.springmvc.model.pojos.DrugapplyEntity;
import cn.gson.springmvc.model.pojos.DrugapplyTableEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
public class DrugapplyServicesZx {

    @Autowired
    YaoPinDaoZx yaoPinDaoZx;

    @Autowired
    DrugapplyDaoZx drugapplyDaoZx;

    @Autowired
    DrugapplyTableDaoZx drugapplyTableDaoZx;

    @Transactional(value="transactionManager",rollbackFor = Exception.class)
    public void savePurchase(DrugappleZx drugappleZx){
        //新增请领单
        DrugapplyEntity druga=new DrugapplyEntity();
        druga.setDrugapplySl(drugappleZx.getAmount());
        druga.setDrugapplyFzr(drugappleZx.getUser());
        druga.setDrugapplyClzt("未处理");

        drugapplyDaoZx.save(druga);

        List<DrugapplesZx> list=drugappleZx.getDruge();
        for(DrugapplesZx l:list){
            DrugapplyTableEntity drugapplyTableEntity=new DrugapplyTableEntity();
            drugapplyTableEntity.setDrugapplyByDrugapplytableId(druga);//请领表id
            drugapplyTableEntity.setYaopinByYaopinId(yaoPinDaoZx.findById(new Long(l.getYaopinId())).get());//药品表id
            drugapplyTableEntity.setDrugapplyTableYaolian(l.getBuyNumber());
            drugapplyTableEntity.setDrugapplyTableDj(l.getYaopinDj());

            drugapplyTableDaoZx.save(drugapplyTableEntity);
        }

    }
}
