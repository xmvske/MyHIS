package cn.gson.springmvc.model.services;


import cn.gson.springmvc.model.mapper.HealthMapperXzy;
import cn.gson.springmvc.model.mapper.ReportMapperXzy;
import com.alibaba.fastjson.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
public class ReportServiceXzy {

    @Autowired
    ReportMapperXzy ReportMapper;

    public JSONArray finaAll(){
        return ReportMapper.findAll();
    }

}



