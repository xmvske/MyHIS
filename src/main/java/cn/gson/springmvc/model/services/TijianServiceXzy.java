package cn.gson.springmvc.model.services;

import cn.gson.springmvc.model.dao.TijianDaoXzy;
import cn.gson.springmvc.model.mapper.TijianMapperXzy;
import com.alibaba.fastjson.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
public class TijianServiceXzy {

    @Autowired
    TijianMapperXzy tijianMapper;
    @Autowired
    TijianDaoXzy dd;


    public JSONArray finaAll(){
        return tijianMapper.findAll();
    }

}
