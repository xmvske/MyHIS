package cn.gson.springmvc.model.services;

import cn.gson.springmvc.model.dao.LZLUserDao;
import cn.gson.springmvc.model.mapper.LZLUserMapper;
import cn.gson.springmvc.model.pojos.UserEntity;
import com.alibaba.fastjson.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
public class LZLUserService {
    @Autowired
    LZLUserMapper um;
    @Autowired
    LZLUserDao lzlUserDao;

    public JSONArray findAll(){return um.findAll();}

    public void xgmm(Integer userid){um.xgmm(userid);}

    public void yglizhi(Integer userid){um.yglizhi(userid);}

    public void xinzeng(UserEntity userEntity){lzlUserDao.save(userEntity);}

    public JSONArray denlu(String userName,String userPass){return um.denlu(userName,userPass);}

}