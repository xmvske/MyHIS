package cn.gson.springmvc.model.services;

import cn.gson.springmvc.model.mapper.KeshiMapperLt;
import com.alibaba.fastjson.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional(rollbackFor = Exception.class)
public class KeshiServiceLt {

    @Autowired
    KeshiMapperLt keshiMapper;


    public JSONArray Userkysfin(){
        return  keshiMapper.keshifinall();
    }


    public JSONArray keshibed(Integer ksid){
        return  keshiMapper.keshibed(ksid);
    }

    public JSONArray keshiroom(Integer ksidroom){
        return  keshiMapper.keshiroom(ksidroom);
    }
}
