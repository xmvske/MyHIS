package cn.gson.springmvc.model.services;

import cn.gson.springmvc.model.Vo.YaoPinXzZx;
import cn.gson.springmvc.model.dao.GonyingsDaoZx;
import cn.gson.springmvc.model.dao.YaoPinDaoZx;
import cn.gson.springmvc.model.mapper.YaoPinMapperZx;
import cn.gson.springmvc.model.pojos.GonyingsEntity;
import cn.gson.springmvc.model.pojos.YaopinEntity;
import com.alibaba.fastjson.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Date;

@Service
@Transactional(rollbackFor = Exception.class)
public class YaoPinServicesZx {
//    @Autowired
//    YaoPinDao yaoPinDao;
    @Autowired
    GonyingsDaoZx gonyingsDaoZx;
    @Autowired
    YaoPinMapperZx yaoPinMapperZx;
    @Autowired
    YaoPinDaoZx yaoPinDaoZx;

    public JSONArray finAlln(Integer gonyingsId){
        return yaoPinMapperZx.findAlln(gonyingsId);
    }


    public JSONArray findAll(){
        return yaoPinMapperZx.findAll();
    }

    //新增药品
    @Transactional(value="transactionManager",rollbackFor = Exception.class)
    public void findXz(YaoPinXzZx y){
        YaopinEntity yaopin=new YaopinEntity();
        yaopin.setYaopinName(y.getNamen());
        //供应商
        GonyingsEntity gonyingsEntity = gonyingsDaoZx.findById(new Long(y.getRegion())).get();
        yaopin.setGonyingsByGonyingsId(gonyingsEntity);
        yaopin.setYaopinGuige(y.getDatee());
        yaopin.setYaopinLeixin(y.getResource());
        yaopin.setYaopinScrq(new Timestamp(new Date().getTime()));
        yaopin.setYaopingYxq(new Timestamp(y.getTypes().getTime()));

        yaoPinDaoZx.save(yaopin);

    }
}
