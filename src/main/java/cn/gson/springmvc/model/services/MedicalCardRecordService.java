package cn.gson.springmvc.model.services;

import cn.gson.springmvc.model.dao.MedicalCardRecordDao;
import cn.gson.springmvc.model.pojos.MedicalCardRecordEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
public class MedicalCardRecordService {

    @Autowired
    MedicalCardRecordDao mcrd;

    @Transactional(value ="transactionManager" ,rollbackFor = Exception.class)
    public int AddMcrd(MedicalCardRecordEntity mcre){
        return mcrd.save(mcre).getRecordId()!=0?1:0;
    }
}
