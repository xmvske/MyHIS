package cn.gson.springmvc.model.services;

import cn.gson.springmvc.model.dao.ZhuyuanApplyDaoLt;
import cn.gson.springmvc.model.mapper.ZhuyuanApplyMapperLt;
import com.alibaba.fastjson.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional(rollbackFor = Exception.class)
public class ZhuyuanApplyServiceLt {

    @Autowired
    ZhuyuanApplyMapperLt zhuyuanApplyMapper;

    @Autowired
    ZhuyuanApplyDaoLt zhuyuanApplyDao;


    public JSONArray zytongzhiFindAll(){
        return  zhuyuanApplyMapper.zhuyuantongzhi();
    }


}
