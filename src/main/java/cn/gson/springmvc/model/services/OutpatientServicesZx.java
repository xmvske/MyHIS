package cn.gson.springmvc.model.services;

import cn.gson.springmvc.model.mapper.OutpatientMapperZx;
import com.alibaba.fastjson.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
public class OutpatientServicesZx {
    @Autowired
    OutpatientMapperZx outpatientMapperZx;

    public JSONArray findAll(){
        return outpatientMapperZx.findAll();
    }
}
