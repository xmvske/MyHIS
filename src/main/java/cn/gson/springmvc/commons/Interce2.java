package cn.gson.springmvc.commons;


import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Interce2 extends HandlerInterceptorAdapter {
    private long start;
    private long end;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        start = System.currentTimeMillis();
        System.out.println("预处理："+start);
        HttpSession session= request.getSession();
        System.out.println(session.getAttribute("user"));
        if(session.getAttribute("user")!=null){
            return true;
        }
        response.sendRedirect("/enter");
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        end = System.currentTimeMillis();
        System.out.println("后处理"+end);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        long offset = end - start;
        System.out.println("方法执行时间："+offset);
    }
}
