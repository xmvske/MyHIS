package cn.gson.springmvc.commons;

import org.springframework.core.convert.converter.Converter;

import java.util.Arrays;
import java.util.List;

public class ArrayToListConverter implements Converter<String,List> {
    @Override
    public List convert(String str) {
        return Arrays.asList(str.split(","));
    }
}
