package cn.gson.springmvc.commons;


import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Interce extends HandlerInterceptorAdapter {
    private long start;
    private long end;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        start = System.currentTimeMillis();
        System.out.println("预处理："+start);
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        end = System.currentTimeMillis();
        int r=(int)modelAndView.getModel().get("r");
        r=r%3==0?r*10:r;
        modelAndView.addObject("r",r);
        System.out.println("后处理"+end);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        long offset = end - start;
        System.out.println("方法执行时间："+offset);
    }
}
